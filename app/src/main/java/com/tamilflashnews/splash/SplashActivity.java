package com.tamilflashnews.splash;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.comscore.analytics.comScore;
import com.comscore.instrumentation.InstrumentedActivity;
import com.facebook.appevents.AppEventsLogger;
import com.tamilflashnews.R;
import com.tamilflashnews.dbhelpers.Repo;
import com.tamilflashnews.dbhelpers.RepoArticles;
import com.tamilflashnews.home.AppIntroduction;
import com.tamilflashnews.home.HomeActivity;
import com.tamilflashnews.model.Category;
import com.tamilflashnews.model.NewsAppConstants;
import com.tamilflashnews.model.UpdatedArticleTable;
import com.tamilflashnews.model.VolleyJsonObjectResponder;
import com.tamilflashnews.service.UpdatedArticlesSync;
import com.tamilflashnews.util.NewsAppUtil;
import com.tamilflashnews.util.gcm.RegistrationIntentService;
import com.tamilflashnews.util.network.VolleyRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SplashActivity extends InstrumentedActivity {

    private static int SPLASH_TIME_OUT = 10000;
    public SharedPreferences mode;
    public String date;
    public boolean navigatedToHome;
    boolean isCategoriesReceived, isArticlesUpdated;
    boolean isIntroScreenCompleted;
    SharedPreferences sharedpreferencesLanguage;
    SharedPreferences.Editor sharedpreferencesEditor;
    private boolean downloadedAllArticlesAppOpen = false;
    private String TAG = "SplashActivity";
    private SharedPreferences sharedpreferencesDownloadedAllArticles, sharedPreferencesUpdateArticlesAlarm, sharedPreferencesUpdatedArticleCount;
    private boolean UpdatedArtilcesAlarm = false;
    private SharedPreferences appIntroPreference;
    private String mArticleId;
    private String mShareArticleId;
    private boolean mShareClick;
    private String mShareArticleTitle;
    private int mCurrentVersion = 1;
    private boolean UpdatedArticlesAPIAlarm;
    private Runnable callHomeActivityRunnable;
    private android.os.Handler callHomeActivityHandler;
    private SharedPreferences sharedPreferencesPushNotifications;
    private boolean pushReceived = false;
    private SharedPreferences.Editor editorPushNotifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        sharedpreferencesDownloadedAllArticles = getSharedPreferences("downloadedAllArticles", MODE_PRIVATE);
        downloadedAllArticlesAppOpen = sharedpreferencesDownloadedAllArticles.getBoolean("downloadedAllArticlesAppOpen", false);

        sharedpreferencesLanguage = getSharedPreferences("language", MODE_PRIVATE);
        sharedpreferencesEditor = sharedpreferencesLanguage.edit();



      /*  sharedPreferencesPushNotifications = getSharedPreferences("PushNotifications", Context.MODE_PRIVATE);
        pushReceived = sharedPreferencesPushNotifications.getBoolean("PushReceived", false);*/

        Log.e("Time", "" + System.currentTimeMillis());
        comScore.setAppContext(this.getApplicationContext());
        comScore.setCustomerC2("20455672");
        comScore.setPublisherSecret("5795bbedd783374eab662d5da0bd8059");

        navigatedToHome = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);

        comScore.onUxActive();
        comScore.getVersion();
        comScore.onUserInteraction();
        comScore.enableAutoUpdate(120, true);

        Intent pushNotificationIntent = getIntent();
        Intent shareIntent = getIntent();

        mArticleId = pushNotificationIntent.getStringExtra("articleId");
        mShareArticleId = shareIntent.getStringExtra("shareArticleId");
        mShareClick = shareIntent.getBooleanExtra("shareClick", false);
        mShareArticleTitle = shareIntent.getStringExtra("shareArticleTitle");

        NewsAppUtil.logMessage("push message splash", "message received is: " + mArticleId + mShareArticleId);

        ImageView gyroView = (ImageView) findViewById(R.id.gyro);
        gyroView.setBackgroundResource(R.drawable.splash_screen_gif);
        AnimationDrawable gyroAnimation = (AnimationDrawable) gyroView.getBackground();
        gyroAnimation.start();

        mode = getSharedPreferences("date", MODE_PRIVATE);
        date = mode.getString("lastdate", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));

        appIntroPreference = getSharedPreferences("App Intro", Context.MODE_PRIVATE);
        isIntroScreenCompleted = appIntroPreference.getBoolean("App Intro Completed", false);

      /*  PushReceived pushBroadCastReceiver = new PushReceived();
        registerReceiver(pushBroadCastReceiver, new IntentFilter("PushReceived"));*/


        callHomeActivityHandler = new android.os.Handler();
        if (NewsAppUtil.getInstance(this).isNetworkConnected()) {

           /* //last fetched date == current date
            if (date == new SimpleDateFormat("dd-MM-yyyy").format(new Date())) {

            } else {

            }*/


            String pushStatus = NewsAppUtil.getInstance(getApplicationContext()).getSecurePrefs().getString("pushEnabled", "");
            if (pushStatus.equals("set") || pushStatus.equals("")) {
                registerForPushNotification();
            }

            getCategories();
            if (getArticleCount() != 0) {
                isArticlesUpdated = true;
            }
            if (!downloadedAllArticlesAppOpen) {
                getAllArticles();
            } else {
                sharedPreferencesUpdateArticlesAlarm = getSharedPreferences("UpdatedArticlesAlarm", MODE_PRIVATE);
                UpdatedArticlesAPIAlarm = sharedPreferencesUpdateArticlesAlarm.getBoolean("UpdatedArticlesAPIAlarm", false);

                if (!UpdatedArticlesAPIAlarm) {
                    Intent intent = new Intent(this, UpdatedArticlesSync.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 234324243, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, (60000), pendingIntent);
                    // alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), (60000), pendingIntent);

                    sharedPreferencesUpdateArticlesAlarm = getSharedPreferences("UpdatedArticlesAlarm", MODE_PRIVATE);
                    //  SharedPreferences.Editor editorUpdateArticles = sharedpreferencesDownloadedAllArticles.edit();
                    SharedPreferences.Editor editorUpdateArticles = sharedPreferencesUpdateArticlesAlarm.edit();
                    editorUpdateArticles.putBoolean("UpdatedArticlesAPIAlarm", true);
                    editorUpdateArticles.commit();

                }
               /* if (pushReceived) {
                    onResetStartWebService();
                } else {
                    if (!UpdatedArticlesAPIAlarm) {
                        Intent intent = new Intent(this, UpdatedArticlesSync.class);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 234324243, intent, 0);
                        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, (60000), pendingIntent);
                        // alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), (60000), pendingIntent);

                        sharedPreferencesUpdateArticlesAlarm = getSharedPreferences("UpdatedArticlesAlarm", MODE_PRIVATE);
                        //  SharedPreferences.Editor editorUpdateArticles = sharedpreferencesDownloadedAllArticles.edit();
                        SharedPreferences.Editor editorUpdateArticles = sharedPreferencesUpdateArticlesAlarm.edit();
                        editorUpdateArticles.putBoolean("UpdatedArticlesAPIAlarm", true);
                        editorUpdateArticles.commit();

                    }
                }*/

                sharedPreferencesUpdatedArticleCount = getSharedPreferences("updatedArticles", MODE_PRIVATE);
                //  SharedPreferences.Editor editorUpdateArticlesCount = sharedpreferencesDownloadedAllArticles.edit();
                SharedPreferences.Editor editorUpdateArticlesCount = sharedPreferencesUpdatedArticleCount.edit();
                boolean updatedArticleCount = sharedPreferencesUpdatedArticleCount.getBoolean("UpdatedArticleCount", false);
                if (updatedArticleCount) {
                    Repo.getInstance(SplashActivity.this).clearAllTables();
                    getAllArticles();
                    editorUpdateArticlesCount.putBoolean("UpdatedArticleCount", false);
                    editorUpdateArticlesCount.commit();
                }
            }

        } else {

            if (getCategoryCount() == 0 || getArticleCount() == 0) {
                Toast.makeText(this, getString(R.string.initial_data_connection_error_msg), Toast.LENGTH_LONG).show();
            } else {
                isCategoriesReceived = true;
                isArticlesUpdated = true;
                goToHome();
            }
        }
        Typeface tf = Typeface.createFromAsset(getAssets(),
                "splashfont.ttf");
        TextView tv = (TextView) findViewById(R.id.textView18);
        TextView tv1 = (TextView) findViewById(R.id.textView19);
        tv.setTypeface(tf);
        tv1.setTypeface(tf);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppEventsLogger.activateApp(this);
        // Notify comScore about lifecycle usage
        comScore.onEnterForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();

        AppEventsLogger.deactivateApp(this);
        // Notify comScore about lifecycle usage
        comScore.onExitForeground();
    }

    private void registerForPushNotification() {
        String token = NewsAppUtil.getInstance(this).getSecurePrefs().getString(NewsAppConstants.PUSH_TOKEN_KEY, "");

        SharedPreferences preferences = getSharedPreferences("PushNotification", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorPref = preferences.edit();
        editorPref.putBoolean("isActive", true);
        editorPref.commit();
        if (token.isEmpty() && NewsAppUtil.checkPlayServices(this)) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private void getCategories() {

        NewsAppUtil.logMessage("splash activity", "get categories");
        VolleyRequester.getInstance(this)
                .sendVolleyJsonObjectRequest(NewsAppConstants.CATEGORY_REQUEST_TAG,
                        NewsAppUtil.getInstance(getApplicationContext()).getReqUrlWithModAndView(NewsAppConstants.MODULE_ARTICLES, NewsAppConstants.VIEW_CATEGORY_LIST),
                        new VolleyJsonObjectResponder() {
                            @Override
                            public void onJsonObjectResponseSuccess(JSONObject response) {
                                try {
                                    JSONArray categoriesList = response.getJSONArray("clist");
                                    Repo.getInstance(SplashActivity.this).clearTable(Category.class);
                                    NewsAppUtil.logMessage("Categories table Count before categories insertion " + Repo.getInstance(SplashActivity.this).repoCategories.getCount());
                                    for (int index = 0; index < categoriesList.length(); index++) {
                                        Category category = new Category().fromJSON(categoriesList.getJSONObject(index));
                                        Repo.getInstance(SplashActivity.this).repoCategories.insert(category);
                                    }

                                    isCategoriesReceived = true;
                                    NewsAppUtil.logMessage("Categories table Count after categories insertion " + Repo.getInstance(SplashActivity.this).repoCategories.getCount());
                                    goToHome();

                                } catch (JSONException je) {
                                    je.printStackTrace();
                                }
                            }

                            @Override
                            public void onJsonObjectResponseFailure(VolleyError error) {
                                NewsAppUtil.logError("Cannot retrieve categories " + error);
                                if (getArticleCount() != 0 && getCategoryCount() != 0) {
                                    goToHome();
                                }
                            }
                        });
    }

    private Integer getLastUpdatedArticleId() {

        List<UpdatedArticleTable> updatedArticleTable_list;
        updatedArticleTable_list = Repo.getInstance(SplashActivity.this).repoArticles.getAllArticlesById();
        int articleSize = 0;
        int articleId = 0;

        articleSize = updatedArticleTable_list.size();
        if (articleSize != 0) {

            UpdatedArticleTable lastUpdatedArticleTable = updatedArticleTable_list.get(updatedArticleTable_list.size() - 1);
            articleId = lastUpdatedArticleTable.getArticleId();
        }
        return articleId;
    }

    private UpdatedArticleTable getFirstOldArticle() {
        UpdatedArticleTable lastUpdatedArticleTable = null;
        List<UpdatedArticleTable> updatedArticleTable_list;
        updatedArticleTable_list = Repo.getInstance(SplashActivity.this).repoArticles.getAllArticlesDescenOrderById();
        int articleSize = 0;
        int articleId = 0;

        articleSize = updatedArticleTable_list.size();
        if (articleSize != 0) {

            lastUpdatedArticleTable = updatedArticleTable_list.get(updatedArticleTable_list.size() - 1);
            //articleId = lastUpdatedArticleTable.getArticleId();
        }
        return lastUpdatedArticleTable;
    }


    private void getAllArticles() {
        VolleyRequester.getInstance(this)
                .sendVolleyJsonObjectRequest(NewsAppConstants.ALL_ARTICLES_REQUEST_TAG,
                        NewsAppUtil.getInstance(getApplicationContext()).getReqUrlWithMod(NewsAppConstants.MODULE_ARTICLES), new VolleyJsonObjectResponder() {

                            @Override
                            public void onJsonObjectResponseSuccess(JSONObject response) {
                                try {
                                    int newArticlesCount = 0;

                                    if (!response.isNull("bpath")) {
                                        String appendImagePath = response.getString("bpath");
                                        Log.e("URL", "" + appendImagePath);
                                        SharedPreferences sharedpreferencesImagePath = getSharedPreferences("downloadedAllArticles", MODE_PRIVATE);
                                        SharedPreferences.Editor imagePath = sharedpreferencesImagePath.edit();
                                        imagePath.putString("bpath", appendImagePath);
                                        imagePath.commit();
                                        //String appendImageUrl= imageJsonObject.get("bpath");

                                    }
                                    if (!response.isNull("a_list")) {
                                        JSONArray newArticleArray = response.getJSONArray("a_list");
                                        Log.d(TAG, "Response" + response);
                                        RepoArticles repoArticles = Repo.getInstance(SplashActivity.this).repoArticles;
                                        NewsAppUtil.logMessage("UpdatedArticleTable table Count before articles insertion " + repoArticles.getCount());
                                        NewsAppUtil.logMessage(TAG, "url:" + repoArticles);
                                        NewsAppUtil.logMessage(TAG, "url:" + repoArticles.getCount());

                                        for (int i = 0; i < newArticleArray.length(); ++i) {

                                            NewsAppUtil.logMessage(TAG, "coming in");
                                            JSONObject categoryObj = newArticleArray.getJSONObject(i);
                                            NewsAppUtil.logMessage(TAG, "category obj is: " + categoryObj);
                                            String categoryID = categoryObj.getString("cid");
                                            NewsAppUtil.logMessage(TAG, "category id is: " + categoryID);
                                            newArticlesCount++;
                                            UpdatedArticleTable updatedArticleTable = new UpdatedArticleTable().fromJSON(newArticleArray.getJSONObject(i));
                                            updatedArticleTable.setCategoryId(categoryID);

                                            Repo.getInstance(SplashActivity.this).repoArticles.insert(updatedArticleTable);
                                        }

                                        SharedPreferences sharedpreferencesDownloadedAllArticles = getSharedPreferences("downloadedAllArticles", MODE_PRIVATE);
                                        SharedPreferences.Editor editorDownloadedAllArticles = sharedpreferencesDownloadedAllArticles.edit();
                                        editorDownloadedAllArticles.putBoolean("downloadedAllArticlesAppOpen", true);
                                        editorDownloadedAllArticles.commit();
                                    }
                                    if (!response.isNull("language")) {
                                        JSONArray languageArray = response.getJSONArray("language");
                                        for (int i = 0; i < languageArray.length(); i++) {
                                            JSONObject languageObj = languageArray.getJSONObject(i);
                                            JSONArray languagesDataArray = languageObj.getJSONArray("data");

                                            ArrayList<String> languageArrayList = new ArrayList<String>();

                                            if (languagesDataArray != null) {
                                                for (int j = 0; j < languagesDataArray.length(); j++) {
                                                    languageArrayList.add(languagesDataArray.get(j).toString());
                                                }
                                            }
                                            sharedpreferencesEditor.putString("LanguageArrayList", TextUtils.join(",", languageArrayList).toString());
                                            sharedpreferencesEditor.commit();
                                            String enable = languageObj.getString("enable");
                                            if (enable != null) {
                                                if (enable.equals("1")) {
                                                    sharedpreferencesEditor.putBoolean("enable", true);
                                                    sharedpreferencesEditor.commit();
                                                }
                                            }
                                        }
                                    }

                                    sharedpreferencesEditor.putBoolean("enable", true);
                                    sharedpreferencesEditor.commit();
                                    NewsAppUtil.logMessage("mycheck", "new articels count is: " + newArticlesCount);

                                    if (!isArticlesUpdated) {
                                        isArticlesUpdated = true;
                                        if (isIntroScreenCompleted) {
                                            goToHome();
                                        }
                                    } else {
                                        NewsAppUtil.logMessage("mycheck", "articles updated already.not pushing to home");
                                    }

                                    SharedPreferences sharedpreferences = getSharedPreferences("updatedArticles", MODE_PRIVATE);
                                    // int oldArticleCount = sharedpreferences.getInt("updatedCount", 0);

                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString("updatedCount", newArticlesCount + "");
                                    editor.commit();
                                    NewsAppUtil.logMessage("UpdatedArticleTable table Count after articles insertion " + Repo.getInstance(SplashActivity.this).repoArticles.getCount());
                                } catch (JSONException je) {
                                    je.printStackTrace();
                                }
                            }

                            @Override
                            public void onJsonObjectResponseFailure(VolleyError error) {
                                NewsAppUtil.logError("Cannot retrieve articles " + error);
                                if (getArticleCount() != 0 && getCategoryCount() != 0) {
                                    goToHome();
                                }
                            }
                        });
    }

    private long getCategoryCount() {

        long categoriesCount = Repo.getInstance(SplashActivity.this).repoCategories.getCount();
        return categoriesCount;

    }

    private long getArticleCount() {

        long articlesCount = Repo.getInstance(SplashActivity.this).repoArticles.getCount();
        return articlesCount;
    }


    private void goToHome() {

        callHomeActivityHandler = new android.os.Handler();
 /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
        callHomeActivityRunnable = new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                NewsAppUtil.logMessage("home" + "category: " + String.valueOf(isCategoriesReceived) + "article_update" + String.valueOf(isArticlesUpdated));
                navigatedToHome = true;

                if (isIntroScreenCompleted) {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    if (mShareClick) {
                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.cancel(Integer.parseInt(mShareArticleId));
                        intent.putExtra("shareArticleId", mShareArticleId);
                        intent.putExtra("shareClick", mShareClick);
                        intent.putExtra("shareArticleTitle", mShareArticleTitle);
                        NewsAppUtil.logMessage("push message shareclick", "message received is: " + mArticleId + mShareArticleId);

                    } else {
                        intent.putExtra("articleId", mArticleId);
                        NewsAppUtil.logMessage("push message normal", "message received is: " + mArticleId + mShareArticleId);
                    }
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, AppIntroduction.class);
                    intent.putExtra("articleId", mArticleId);
                    intent.putExtra("multiCategoryIntro", true);
                    startActivity(intent);
                    finish();
                }
            }
        };

        callHomeActivityHandler.postDelayed(callHomeActivityRunnable, 7000);

        /*pushReceived = sharedPreferencesPushNotifications.getBoolean("PushReceived", false);
        if (pushReceived)
            callHomeActivityHandler.postDelayed(callHomeActivityRunnable, 160000);
        else
            callHomeActivityHandler.postDelayed(callHomeActivityRunnable, 3000);
*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

   /*     try {
            mCurrentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if(mCurrentVersion>=6){
            NewsAppUtil.getInstance(getApplicationContext()).getSecurePrefs().edit()
                    .putString("PushStatus","AppVersionAPI").commit();

        }*/
    }

    //Reset Updating Article Web Service and Start it again to show exact article
    public void onResetStartWebService() {
        SharedPreferences sharedPreferencesUpdateArticlesAlarm;
        boolean UpdatedArticlesStartAPIAlarm, UpdatedArticlesStopAPIAlarm;

        //Reset Alarm
        sharedPreferencesUpdateArticlesAlarm = getSharedPreferences("UpdatedArticlesAlarm", MODE_PRIVATE);
        UpdatedArticlesStopAPIAlarm = sharedPreferencesUpdateArticlesAlarm.getBoolean("UpdatedArticlesAPIAlarm", false);
        if (UpdatedArticlesStopAPIAlarm) {
            Intent cancelUpdateArticleIntent = new Intent(this, UpdatedArticlesSync.class);
            PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 234324243, cancelUpdateArticleIntent, 0);
            AlarmManager cancelAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            cancelAlarmManager.cancel(cancelPendingIntent);

            SharedPreferences.Editor editorUpdateArticles = sharedPreferencesUpdateArticlesAlarm.edit();
            editorUpdateArticles.putBoolean("UpdatedArticlesAPIAlarm", false);
            editorUpdateArticles.commit();
        }

        //Start Alarm
        UpdatedArticlesStartAPIAlarm = sharedPreferencesUpdateArticlesAlarm.getBoolean("UpdatedArticlesAPIAlarm", false);
        if (!UpdatedArticlesStartAPIAlarm) {
            Intent intent = new Intent(this, UpdatedArticlesSync.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 234324243, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            // alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), (60000), pendingIntent);
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, (60000), pendingIntent);

            SharedPreferences.Editor editorUpdateArticles = sharedPreferencesUpdateArticlesAlarm.edit();
            editorUpdateArticles.putBoolean("UpdatedArticlesAPIAlarm", true);
            editorUpdateArticles.commit();
        }
    }

   /* public class PushReceived extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            editorPushNotifications = sharedPreferencesPushNotifications.edit();
            editorPushNotifications.putBoolean("PushReceived", false);
            editorPushNotifications.commit();
            //callHomeActivityHandler.removeCallbacks(callHomeActivityRunnable);
            //callHomeActivityHandler.removeCallbacksAndMessages(null);
            callHomeActivityHandler.post(callHomeActivityRunnable);

        }
    }*/
}