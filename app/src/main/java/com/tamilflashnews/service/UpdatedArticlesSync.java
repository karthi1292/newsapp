package com.tamilflashnews.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tamilflashnews.dbhelpers.Repo;
import com.tamilflashnews.dbhelpers.RepoArticles;
import com.tamilflashnews.model.NewsAppConstants;
import com.tamilflashnews.model.UpdatedArticleTable;
import com.tamilflashnews.model.VolleyJsonObjectResponder;
import com.tamilflashnews.model.VolleyStringResponder;
import com.tamilflashnews.util.NewsAppUtil;
import com.tamilflashnews.util.network.VolleyRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dspriyan on 26/08/15.
 */
public class UpdatedArticlesSync extends BroadcastReceiver {

    private static final String PREFERENCE_NAME = "updatedArticles";
    public static String Url = "http://text2plate.com/api/mobile/addUser.json";
    public Context mContext;
    SharedPreferences.Editor editorPushNotifications;
    private String TAG = "UpdatedArticles";
    private SharedPreferences sharedPreferencesPushNotifications;
    private boolean pushReceived = false;
    private String language="Tamil";
    SharedPreferences sharedpreferencesLanguage;
    SharedPreferences.Editor sharedpreferencesEditor;

    public void onReceive(Context context, Intent intent) {

      //  Toast.makeText(context, "Varudha????", Toast.LENGTH_LONG).show();
        ConnectivityManager connect = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        mContext = context;
        if (connect != null && (
                (connect.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) ||
                        (connect.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED))) {

            updateArticles(context);
        }
    }

    private Integer getLastUpdatedArticleId(Context ctx) {

        List<UpdatedArticleTable> updatedArticleTable_list;
        updatedArticleTable_list = Repo.getInstance(ctx).repoArticles.getAllArticlesById();
        int articleSize = 0;
        int articleId = 0;

        articleSize = updatedArticleTable_list.size();
        if (articleSize != 0) {

            UpdatedArticleTable lastUpdatedArticleTable = updatedArticleTable_list.get(updatedArticleTable_list.size() - 1);
            articleId = lastUpdatedArticleTable.getArticleId();
        }
        return articleId;
    }

    private UpdatedArticleTable getFirstOldArticle(Context context) {
        UpdatedArticleTable lastUpdatedArticleTable = null;
        List<UpdatedArticleTable> updatedArticleTable_list;
        updatedArticleTable_list = Repo.getInstance(context).repoArticles.getAllArticlesDescenOrderById();
        int articleSize = 0;
        int articleId = 0;

        articleSize = updatedArticleTable_list.size();
        if (articleSize != 0) {

            lastUpdatedArticleTable = updatedArticleTable_list.get(updatedArticleTable_list.size() - 1);
            //articleId = Article.getArticleId();
        }
        return lastUpdatedArticleTable;
    }

    private void updateArticles(Context ctx) {
        final Context contextIs = ctx;
        SharedPreferences sharedpreferences = contextIs.getSharedPreferences("updatedArticles", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();
        sharedpreferencesLanguage =contextIs.getSharedPreferences("language", Context.MODE_PRIVATE);
        sharedpreferencesEditor = sharedpreferencesLanguage.edit();



        String lastUpdatedDateTime = getLastArticleUpdatedDateTime(ctx);

        Log.d("lastUpdatedDateTime", "" + lastUpdatedDateTime);

        if (lastUpdatedDateTime != null) {
            try {
                lastUpdatedDateTime = URLEncoder.encode(lastUpdatedDateTime, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            VolleyRequester.getInstance(ctx)
                    .sendVolleyJsonObjectRequest(NewsAppConstants.ALL_ARTICLES_REQUEST_TAG,
                            NewsAppUtil.getInstance(mContext).getReqUrlWithNewsModuleCategoryAndUpdatedDateTime(lastUpdatedDateTime), new VolleyJsonObjectResponder() {

                                @Override
                                public void onJsonObjectResponseSuccess(JSONObject response) {
                                    Log.d(TAG, "Response" + response);
                                    try {
                                        //long allArticles=1001;
                                        //long allArticles = repoArticles.getCount();
                                        /*if (allArticles > 1000) {
                                            UpdatedArticleTable firstInsertedArticle = getFirstArticleInserted(contextIs);
                                            Repo.getInstance(contextIs).repoArticles.delete(firstInsertedArticle);
                                        }*/
                                        NewsAppUtil.logMessage("UpdatedArticleTable table Count after articles deletion " + Repo.getInstance(contextIs).repoArticles.getCount());

                                        int newArticlesCount = 0;
                                        if (!response.isNull("bpath")) {
                                            String appendImagePath = response.getString("bpath");
                                            Log.e("URL", "" + appendImagePath);

                                            SharedPreferences sharedpreferencesImagePath = mContext.getSharedPreferences("downloadedAllArticles", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor imagePath = sharedpreferencesImagePath.edit();
                                            imagePath.putString("bpath", appendImagePath);
                                            imagePath.commit();
                                        }
                                        if (!response.isNull("a_list")) {
                                            JSONArray newArticleArray = response.getJSONArray("a_list");

                                            if (newArticleArray.length() >= 200) {
                                                editor.putBoolean("UpdatedArticleCount", true);
                                                editor.commit();
                                                return;
                                            }
                                            RepoArticles repoArticles = Repo.getInstance(contextIs).repoArticles;
                                            NewsAppUtil.logMessage("UpdatedArticleTable table Count before articles insertion " + repoArticles.getCount());
                                            NewsAppUtil.logMessage("splash activity", "url:" + repoArticles);
                                            NewsAppUtil.logMessage("splash activity", "url:" + repoArticles.getCount());

                                            for (int i = 0; i < newArticleArray.length(); ++i) {

                                                NewsAppUtil.logMessage(TAG, "coming in");
                                                JSONObject categoryObj = newArticleArray.getJSONObject(i);
                                                NewsAppUtil.logMessage(TAG, "category obj is: " + categoryObj);
                                                String categoryID = categoryObj.getString("cid");
                                                NewsAppUtil.logMessage(TAG, "category id is: " + categoryID);
                                                newArticlesCount++;
                                                UpdatedArticleTable updatedArticleTable = new UpdatedArticleTable().fromJSON(newArticleArray.getJSONObject(i));
                                                updatedArticleTable.setCategoryId(categoryID);

                                                //long allArticles=1001;
                                                long allArticles = repoArticles.getCount();
                                                if (allArticles > 1000) {
                                                    UpdatedArticleTable firstInsertedUpdatedArticleTable = getFirstArticleInserted(contextIs);
                                                    Repo.getInstance(contextIs).repoArticles.delete(firstInsertedUpdatedArticleTable);
                                                }
                                                Repo.getInstance(contextIs).repoArticles.insert(updatedArticleTable);
                                            }
                                        }
                                      /*  JSONArray deleteArticleArray = new JSONArray();
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.putOpt("aid", "3570");
                                        jsonObject.putOpt("adate", "1 seconds Ago");
                                        jsonObject.putOpt("cdate", "2015-10-07 17:38:57");
                                        jsonObject.putOpt("udate", "2015-10-07 19:10:59");
                                        jsonObject.putOpt("cid", "10");
                                        jsonObject.putOpt("atitle", "விருதை திருப்பியளித்த மேலும் ஒரு கவிஞர்ு");
                                        jsonObject.putOpt("atag", "அமித்ஷா, லாலு மீது");
                                        jsonObject.putOpt("adesc", "பீஹாரில் காட்டுத்தனமான ஆட்சியை லாலு நடத்தினார்");
                                        jsonObject.putOpt("aimg", "upload/2015/10/07/images/400X400/3551_thumb.jpg");
                                        jsonObject.putOpt("aurl", "");
                                        jsonObject.putOpt("atype", "1");
                                        jsonObject.putOpt("data", "");
                                        jsonObject.putOpt("ref_source", "");
                                        jsonObject.putOpt("ref_url", "");
                                        jsonObject.putOpt("like_count", "0");
                                        deleteArticleArray.put(jsonObject);*/


                                        if (!response.isNull("language")) {
                                            JSONArray languageArray = response.getJSONArray("language");
                                            for (int i = 0; i < languageArray.length(); i++) {
                                                JSONObject languageObj = languageArray.getJSONObject(i);
                                                JSONArray languagesDataArray = languageObj.getJSONArray("data");

                                                ArrayList<String> languageArrayList = new ArrayList<String>();

                                                if (languagesDataArray != null) {
                                                    for (int j = 0; j < languagesDataArray.length(); j++) {
                                                        languageArrayList.add(languagesDataArray.get(j).toString());
                                                    }
                                                }
                                                sharedpreferencesEditor.putString("LanguageArrayList", TextUtils.join(",", languageArrayList).toString());
                                                sharedpreferencesEditor.commit();
                                                String enable = languageObj.getString("enable");
                                                if (enable != null) {
                                                    if (enable.equals("1")) {
                                                        sharedpreferencesEditor.putBoolean("enable", true);
                                                        sharedpreferencesEditor.commit();
                                                    }
                                                }
                                            }
                                        }
                                        if (!response.isNull("u_list")) {
                                            JSONArray updateArticleArray = response.getJSONArray("u_list");
                                            Log.d("UpdatedArticleArray", "" + updateArticleArray.length());
                                            //  JSONArray updateArticleArray = jsonArray.getJSONArray();

                                            if (updateArticleArray.length() != 0) {
                                                for (int index = 0; index < updateArticleArray.length(); ++index) {

                                                    NewsAppUtil.logMessage("splash activity update", "coming in");
                                                    JSONObject categoryObjectForUpdation = updateArticleArray.getJSONObject(index);
                                                    NewsAppUtil.logMessage("splash activity update", "category obj is: " + categoryObjectForUpdation);
                                                    String categoryID = categoryObjectForUpdation.getString("cid");
                                                    NewsAppUtil.logMessage("splash activity update", "category id is: " + categoryID);

                                                    // newArticlesCount++;
                                                    UpdatedArticleTable updatedArticleTable = new UpdatedArticleTable().fromJSON(updateArticleArray.getJSONObject(index));
                                                    updatedArticleTable.setCategoryId(categoryID);

                                                    Repo.getInstance(contextIs).repoArticles.update(updatedArticleTable);
                                                }
                                            }
                                        }


                                     /*   JSONArray deleteArticleArray1 = new JSONArray();
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.putOpt("aid", "6239");
                                        deleteArticleArray1.put(jsonObject);*/

                                       /* d_list: [1]
                                        0:  {
                                            aid: "6221"
                                        }*/
                                         if (!response.isNull("d_list")) {
                                            JSONArray deleteArticleArray = response.getJSONArray("d_list");

                                            if (deleteArticleArray.length() != 0) {
                                                for (int index = 0; index < deleteArticleArray.length(); ++index) {

                                                    // JSONObject categoryObjectForDeletion = deleteArticleArray.getJSONObject(index);
                                                    //String categoryID = categoryObjectForDeletion.getString("cid");

                                                    UpdatedArticleTable updatedArticleTable = new UpdatedArticleTable().fromJSON(deleteArticleArray.getJSONObject(index));
                                                    //updatedArticleTable.setCategoryId(categoryID);

                                                    //int deletedArticleId = Integer.parseInt(updatedArticleTable.getArticleId() + "");

                                                    Repo.getInstance(contextIs).repoArticles.delete(updatedArticleTable);
                                                }
                                            }
                                        }

                                        NewsAppUtil.logMessage("mycheck", "new articels count is: " + newArticlesCount);

                                   /*     SharedPreferences sharedpreferences = contextIs.getSharedPreferences("updatedArticles", contextIs.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedpreferences.edit();
                                        editor.putString("updatedCount", newArticlesCount + "");
                                        editor.commit();
                                        String updatedCount = sharedpreferences.getString("updatedCount", "0");
                                        Log.d("Checking Count", "" + updatedCount);*/

                                        SharedPreferences sharedpreferences = contextIs.getSharedPreferences("updatedArticles", contextIs.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedpreferences.edit();
                                        editor.putString("updatedCount", newArticlesCount + "");
                                        editor.commit();

                                        NewsAppUtil.logMessage("UpdatedArticleTable table Count after articles insertion " + Repo.getInstance(contextIs).repoArticles.getCount());
                                    } catch (JSONException je) {
                                        je.printStackTrace();
                                    }

                                  /*  sharedPreferencesPushNotifications = mContext.getSharedPreferences("PushNotifications", Context.MODE_PRIVATE);
                                    pushReceived = sharedPreferencesPushNotifications.getBoolean("PushReceived", false);
                                    if (pushReceived) {
                                        editorPushNotifications = sharedPreferencesPushNotifications.edit();
                                        editorPushNotifications.putBoolean("PushReceived", false);
                                        //editorPushNotifications.commit();
                                        Intent i = new Intent("PushReceived");
                                        mContext.sendBroadcast(i);
                                    }*/
                                }
                                @Override
                                public void onJsonObjectResponseFailure(VolleyError error) {
                                    NewsAppUtil.logError("Cannot retrieve articles " + error);
                                }
                            }
                    );
        }

    }

    private String getLastArticleUpdatedDateTime(Context context) {

        List<UpdatedArticleTable> updatedArticleTable_list;
        String lastArticleUpdatedDateTime = null;
        int articleSize = 0;

        RepoArticles repoArticles = Repo.getInstance(context).repoArticles;
        updatedArticleTable_list = repoArticles.getAllArticlesByDateTime();

        articleSize = updatedArticleTable_list.size();
        if (articleSize != 0) {

            // UpdatedArticleTable article=updatedArticleTable_list.get(updatedArticleTable_list.size()-1);
            lastArticleUpdatedDateTime = updatedArticleTable_list.get(updatedArticleTable_list.size() - 1).getArticleUpdatedDate();
        }
        return lastArticleUpdatedDateTime;
    }

    private UpdatedArticleTable getFirstArticleInserted(Context context) {

        List<UpdatedArticleTable> updatedArticleTable_list;
        int articleSize = 0;
        UpdatedArticleTable updatedArticleTable = null;
        RepoArticles repoArticles = Repo.getInstance(context).repoArticles;
        updatedArticleTable_list = repoArticles.getAllArticlesByDescenDateTime(language);

        articleSize = updatedArticleTable_list.size();
        if (articleSize != 0) {
            updatedArticleTable = updatedArticleTable_list.get(updatedArticleTable_list.size() - 1);
            // firstArticleUpdatedDateTime = updatedArticleTable_list.get(updatedArticleTable_list.size() - 1).getArticleUpdatedDate();
        }
        return updatedArticleTable;
    }
}
