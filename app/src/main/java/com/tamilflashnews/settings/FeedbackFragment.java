package com.tamilflashnews.settings;

import android.app.Dialog;
import android.content.ContentValues;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tamilflashnews.R;
import com.tamilflashnews.util.NewsAppUtil;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by scidevelop05 on 30/6/15.
 */
public class FeedbackFragment extends DialogFragment {


    public static FeedbackFragment newInstance() {
        FeedbackFragment f = new FeedbackFragment();
        return f;
    }

    @Override
    public void onCreate(Bundle s)
    {
        super.onCreate(s);
        NewsAppUtil.logMessage("Infor", "Oncreate");
    }

    @Override
    public void onStart() {
        super.onStart();
        NewsAppUtil.logMessage("Log", "On Start");
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            d.getWindow().setLayout(width, height);
        }

    }
    @Override
    public void onResume()
    {
        super.onResume();
    }

    View view;
    EditText name,emailid,comment;
    String namestr,emailstr,commentstr;
    Button send;

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        view = inflater.inflate(R.layout.feedback_layout, container, false);
        name=(EditText)view.findViewById(R.id.nameEdit);
        emailid=(EditText)view.findViewById(R.id.emailEdit);
        comment=(EditText)view.findViewById(R.id.commentEdit);
        send=(Button)view.findViewById(R.id.sendfeedback);
        namestr=emailstr=commentstr="";

        view.findViewById(R.id.close_setting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendfeedback();
            }
        });

        return view;
    }
    public void sendfeedback()
    {
        SendFeedback feed=new SendFeedback();
        namestr=name.getText().toString();
        emailstr=emailid.getText().toString();
        commentstr=comment.getText().toString();
        if(namestr.equals("")||emailstr.equals("")||commentstr.equals("")||namestr.equals("Name")||emailstr.equals("Email")||commentstr.equals("Comment"))
        {
            Toast.makeText(getActivity().getApplicationContext(),"Please fill all the fields",Toast.LENGTH_LONG).show();
        }
        else
        {
            ContentValues temp=new ContentValues();
            temp.put("name",namestr);
            temp.put("email",emailstr);
            temp.put("comment",commentstr);
            feed.execute(temp);
        }
    }

    public class SendFeedback extends AsyncTask<ContentValues,Void,String>
    {
        public SendFeedback()
        {

        }

        @Override
        protected String doInBackground(ContentValues... a) {
            try {

                ContentValues feed = a[0];
                String names, emails, comments;
                names = feed.get("name").toString();
                emails = feed.get("email").toString();
                comments = feed.get("comment").toString();
                comments=  URLEncoder.encode(comments, "UTF-8");
             //   String urlstr = "http://www.tamilflashnews.com/api/index.php?module=feedback";
                String urlstr = NewsAppUtil.getInstance(getActivity()).getBaseUrl()+"&module=feedback";
                urlstr += "&name=" + names + "&email=" + emails + "&comments=" + comments;
                URL url = new URL(urlstr);

                //send Get and receive response
                String response=new String();

                HttpURLConnection urlconn = (HttpURLConnection) url.openConnection();
                //urlconn.setRequestMethod();
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlconn.getInputStream()));

                int temporarychar;
                while ((temporarychar = reader.read()) != -1) {
                    response += (char) temporarychar;
                }
                NewsAppUtil.logMessage("ReceivedResponse", response);
                return response;
            }
            catch(MalformedURLException e)
            {
                Toast.makeText(getActivity().getApplicationContext(),"Error while submitting feedback",Toast.LENGTH_LONG).show();
                return "Error";
            }
            catch(Exception e)
            {
                Toast.makeText(getActivity().getApplicationContext(),"Error while submitting feedback",Toast.LENGTH_LONG).show();
                return "Error";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject responsejson = new JSONObject(result);
                String res = responsejson.getString("rstatus");
                NewsAppUtil.logMessage("ReceivedResponse", "rstatus  =" + res);
                if (res.equals("Success") || res.equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Your feedback has been submitted successfully", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getActivity().getApplicationContext(), "Error while submitting feedback", Toast.LENGTH_LONG).show();
                }
                dismiss();
            }
            catch (Exception e){
                Toast.makeText(getActivity().getApplicationContext(), "Error while submitting feedback", Toast.LENGTH_LONG).show();
            }
        }
    }
}
