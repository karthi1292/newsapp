package com.tamilflashnews.settings;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.LikeView;
import com.facebook.share.widget.ShareButton;
import com.securepreferences.SecurePreferences;
import com.tamilflashnews.R;
import com.tamilflashnews.dbhelpers.Repo;
import com.tamilflashnews.home.HomeActivity;
import com.tamilflashnews.home.LanguageInterface;
import com.tamilflashnews.home.UserContentActivity;
import com.tamilflashnews.model.Bookmark;
import com.tamilflashnews.model.NewsAppConstants;
import com.tamilflashnews.model.UpdatedArticleTable;
import com.tamilflashnews.model.UpdatedLikeTable;
import com.tamilflashnews.splash.SplashActivity;
import com.tamilflashnews.util.NewsAppUtil;
import com.tamilflashnews.util.adapters.NewsAdapter;
import com.tamilflashnews.util.gcm.RegistrationIntentService;
import com.tamilflashnews.util.ui.ViewClip;

import java.util.Arrays;
import java.util.List;


public class SettingsFragment extends DialogFragment {


    public static Typeface roboto_font;
    public Context context;
    public Boolean toggle;
    public Intent intent;
    public Integer currentPage;
    public ViewPager vpPager;
    public NewsAdapter adapterViewPager;
    public TextView categoryName;
    public String category_id;
    public ImageView bookmark_icon, like_icon;
    public String articlePosition;
    View view;
    LinearLayout contentBackground, headlineBackground;
    LikeView likeView;
    ShareButton share_view;
    Button dialogBtnOK;
    ImageView imageviewLeftArrow, imageViewRightArrow;
    TextView textviewLanguage;
    ShareLinkContent content;
    TextView headline, desc;
    CallbackManager callbackManager;
    ImageView day, night;
    TextView close, refer, bookmarks, rateus, helpTextView, likedarty, shareContent;
    ViewClip clip;
    ToggleButton t_all, t_business, t_astrology, t_motor, t_cinema, t_pugai_pagai, t_vasagar_pakkam, t_vaanga_eluthalaam, t_ithara;
    SwitchCompat day_night, push_switch;
    SharedPreferences sharedpreferencesLanguage;
    String languageDataSerialiazed, language = "Tamil";
    List<String> languageArrayList;
    List<String> selectedList;
    SharedPreferences.Editor sharedpreferencesEditor;
    int languagePosition = 0;
    private LinearLayout root_linear_layout;
    private TextView languageTextView;
    private boolean languageEnable = false;
    private SharedPreferences sharedpreferencesMultiCategory;
    private LanguageInterface languageInterface;
    public static SettingsFragment newInstance(String category_id) {

        /*Bundle args=new Bundle();
        args.putString("categoryid", category_id);
        SettingsFragment f = new SettingsFragment();*/

        SettingsFragment f = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString("categoryid", category_id);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        currentPage = getArguments().getInt("currentPage");
        category_id = getArguments().getString("categoryid");


        sharedpreferencesLanguage = getActivity().getSharedPreferences("language", Context.MODE_PRIVATE);
        sharedpreferencesEditor = sharedpreferencesLanguage.edit();
        languageEnable = sharedpreferencesLanguage.getBoolean("enable", false);

        languageDataSerialiazed = sharedpreferencesLanguage.getString("LanguageArrayList", null);
        if (languageDataSerialiazed != null)
            languageArrayList = Arrays.asList(TextUtils.split(languageDataSerialiazed, ","));

        sharedpreferencesMultiCategory = getActivity().getSharedPreferences("MultiCategoryArticles", Context.MODE_PRIVATE);
        String selectedCategorySerialized = sharedpreferencesMultiCategory.getString("MultiCategory", null);

        if (selectedCategorySerialized != null)
            selectedList = Arrays.asList(TextUtils.split(selectedCategorySerialized, ","));


        //  languageEnable=sharedpreferencesLanguage.getBoolean("enable",false);
        // sharedpreferencesEditor = sharedpreferencesLanguage.edit();
//        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
//
//        callbackManager = CallbackManager.Factory.create();
        //share_view.setShareContent(content);

//        // Set the object for which you want to get likes from your users (Photo, Link or even your FB Fan page)
        //likeView.setObjectIdAndType("http://www.vikatan.com/", LikeView.ObjectType.PAGE);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    ;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        NewsAppUtil.logMessage("settings123", "oncreateview");
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));


        // Get LikeView button
        view = inflater.inflate(R.layout.settings_layout, container, false);


        day_night = (SwitchCompat) view.findViewById(R.id.nighton);

        SharedPreferences preferences = getActivity().getSharedPreferences("PushNotification", Context.MODE_PRIVATE);
        boolean isSwitchChecked = preferences.getBoolean("isActive", false);

        String pushStatus = NewsAppUtil.getInstance(getActivity()).getSecurePrefs().getString("pushEnabled", "notSet");


        push_switch = (SwitchCompat) view.findViewById(R.id.push_manage);
        if (isSwitchChecked) {
            push_switch.setChecked(true);
        } else {
            push_switch.setChecked(false);
        }

        toggle = NewsAppUtil.getInstance(getActivity()).getSecurePrefs().getBoolean("toggle", false);
        day_night.setChecked(toggle);

        view.findViewById(R.id.close_setting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (intent == null)
//                    dismiss();
//                else
//                    startActivity(intent);
                getDialog().dismiss();
            }
        });


        refer = (TextView) view.findViewById(R.id.refer_text);
        refer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String market, link, play_app_id;

                play_app_id = getResources().getString(R.string.play_store_id);
                market = getResources().getString(R.string.market);
                market += play_app_id;
                link = getResources().getString(R.string.playlink);
                link += play_app_id;


                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Check out the app at: " + NewsAppConstants.APP_LINK);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        helpTextView = (TextView) view.findViewById(R.id.help_text);
        helpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), HelpActivity.class));
                getDialog().dismiss();
                /*Intent intent = new Intent(getActivity(), AppIntroduction.class);
                startActivity(intent);*/
                dismiss();
            }
        });


        day_night.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                SecurePreferences.Editor editor = NewsAppUtil.getInstance(getActivity()).getSecurePrefs().edit();
                editor.putBoolean("toggle", isChecked);
                HomeActivity.themeRes = !isChecked;
                HomeActivity homeActivity = (HomeActivity) getActivity();
                editor.commit();
                homeActivity.currentPage = currentPage;
                homeActivity.setHomeAdapter();

            }

        });

        push_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // this preferecece is for Push Notification turn off and on
                SharedPreferences preferences = getActivity().getSharedPreferences("PushNotification", Context.MODE_PRIVATE);
                SharedPreferences.Editor editorPref = preferences.edit();
                editorPref.putBoolean("isActive", isChecked);
                editorPref.commit();

                SecurePreferences.Editor editor = NewsAppUtil.getInstance(getActivity()).getSecurePrefs().edit();
                String pushStatus = (isChecked) ? "set" : "unset";
                editor.putString("pushEnabled", pushStatus);
                editor.commit();

                String pushStatusString = NewsAppUtil.getInstance(getActivity()).getSecurePrefs().getString("pushEnabled", "notSet");

                if (pushStatusString.equals("set")) {
                    registerForPushNotification();
                } else {
                    unRegisterForPushNotification();
                }
            }

        });

        root_linear_layout = (LinearLayout) view.findViewById(R.id.root_linear_layout);

        /*if (true) {
            *//*ViewGroup.LayoutParams lparams = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
*//*
            languageTextView = new TextView(getActivity());
            languageTextView.setText("Language");
            languageTextView.setTextColor(Color.parseColor("#ffffff"));
            languageTextView
            root_linear_layout.addView(languageTextView);

        }*/
        view.findViewById(R.id.feedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedbackFragment ff = new FeedbackFragment();
                ff.show(getFragmentManager(), "Feed");
                dismiss();
            }
        });

        bookmarks = (TextView) view.findViewById(R.id.bookmark);
        bookmarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedpreferences = getActivity().getSharedPreferences("CategoryClicked", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("NewsCategory", "Bookmarks");
                editor.commit();
                fetchBookmarkedArticles();
            }
        });

        likedarty = (TextView) view.findViewById(R.id.likedarticals);
        likedarty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedpreferences = getActivity().getSharedPreferences("CategoryClicked", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("NewsCategory", "LikedArticles");
                editor.commit();
                fetchLikedArticles();
            }
        });

        rateus = (TextView) view.findViewById(R.id.rateus);
        rateus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {                //Rate this app

                Intent intent = new Intent(Intent.ACTION_VIEW);
                String market, link, play_app_id;

                play_app_id = getResources().getString(R.string.play_store_id);
                market = getResources().getString(R.string.market);
                market += play_app_id;
                link = getResources().getString(R.string.playlink);
                link += play_app_id;
                //Try Google play
                intent.setData(Uri.parse(market));
                if (!MyStartActivity(intent)) {
                    //Market (Google play) app seems not installed, let's try to open a webbrowser
                    intent.setData(Uri.parse(link));
                    if (!MyStartActivity(intent)) {
                        //Well if this also fails, we have run out of options, inform the user.                     Toast.makeText(getActivity().getApplicationContext(), "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        shareContent = (TextView) view.findViewById(R.id.shareContent_text);
        shareContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent content = new Intent(getActivity(), UserContentActivity.class);
                startActivity(content);
                dismiss();
            }
        });

        languageTextView = (TextView) view.findViewById(R.id.language_change);
        if (languageEnable)
            languageTextView.setVisibility(View.VISIBLE);
        languageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getDialog().dismiss();
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.language_dialog);

                imageviewLeftArrow = (ImageView) dialog.findViewById(R.id.imageview_left_arrow);
                imageViewRightArrow = (ImageView) dialog.findViewById(R.id.imageview_right_arrow);
                textviewLanguage = (TextView) dialog.findViewById(R.id.textview_language);
                dialogBtnOK = (Button) dialog.findViewById(R.id.button_language_ok);
                languagePosition = sharedpreferencesLanguage.getInt("SelectedLanguagePosition", 0);
                textviewLanguage.setText(languageArrayList.get(languagePosition));
                imageViewRightArrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        languagePosition = sharedpreferencesLanguage.getInt("LanguagePosition", 0);
                        languagePosition++;
                        if (languageArrayList.size() > languagePosition) {
                            textviewLanguage.setText(languageArrayList.get(languagePosition));
                            sharedpreferencesEditor.putInt("LanguagePosition", languagePosition);
                            sharedpreferencesEditor.commit();
                        }
                    }
                });
                imageviewLeftArrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        languagePosition = sharedpreferencesLanguage.getInt("LanguagePosition", 0);
                        languagePosition--;
                        if (languagePosition >= 0) {
                            if (languagePosition < languageArrayList.size()) {
                                textviewLanguage.setText(languageArrayList.get(languagePosition));
                                sharedpreferencesEditor.putInt("LanguagePosition", languagePosition);
                                sharedpreferencesEditor.commit();
                            }
                        }
                    }
                });
                dialogBtnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sharedpreferencesEditor.putInt("SelectedLanguagePosition", languagePosition);
                        sharedpreferencesEditor.putInt("LanguagePosition", languagePosition);

                        sharedpreferencesEditor.putString("LanguageSelected", textviewLanguage.getText().toString());
                        sharedpreferencesEditor.commit();
                      //  Toast.makeText(context,"LangaugeSelected",Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                        languageInterface=(LanguageInterface)context;
                        languageInterface.languageChanged();
                    }
                });
                dialog.show();
            }
        });
        return view;
    }

    private void unRegisterForPushNotification() {
        String token = NewsAppUtil.getInstance(getActivity()).getSecurePrefs().getString(NewsAppConstants.PUSH_TOKEN_KEY, "");
        if (!token.isEmpty() && NewsAppUtil.checkPlayServices(getActivity())) {
            Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
            getActivity().startService(intent);
        }
    }

    private void registerForPushNotification() {
        String token = NewsAppUtil.getInstance(getActivity()).getSecurePrefs().getString(NewsAppConstants.PUSH_TOKEN_KEY, "");
        if (token.isEmpty() && NewsAppUtil.checkPlayServices(getActivity())) {
            Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
            getActivity().startService(intent);
        }
    }


    private boolean MyStartActivity(Intent aIntent) {
        try {
            startActivity(aIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }


    public void fetchBookmarkedArticles() {

        List<Bookmark> bookmarkList = Repo.getInstance(context).repoBookmarks.getAll(language);


        if (bookmarkList.size() == 0) {
            Toast.makeText(context, "No bookmarks to display", Toast.LENGTH_SHORT).show();
            return;
        }


        vpPager = HomeActivity.vpPager;

        vpPager.removeAllViews();


        if (HomeActivity.themeRes == null)
            HomeActivity.adapterViewPager = new NewsAdapter(context, getActivity().getSupportFragmentManager(), true, false, null, selectedList);
        else
            HomeActivity.adapterViewPager = new NewsAdapter(context, getActivity().getSupportFragmentManager(), HomeActivity.themeRes, false, null, selectedList);

        HomeActivity.currentPosition = 0;
        vpPager.setAdapter(HomeActivity.adapterViewPager);
        vpPager.getAdapter().notifyDataSetChanged();
        categoryName = (TextView) getActivity().findViewById(R.id.categoryName);
        categoryName.setText("Bookmarks");
        bookmark_icon = (ImageView) getActivity().findViewById(R.id.bookmark_icon);
        setBookmarkIcon(0);
        getDialog().dismiss();
        // dismiss();

    }

    public Boolean checkBookmark(String cid, String aid) {

        List<Bookmark> bookmarkList = Repo.getInstance(context).repoBookmarks.getArticleBycIDandaID(cid, aid);
        if (bookmarkList == null || bookmarkList.size() == 0)
            return false;
        else
            return true;


    }

    public void setBookmarkIcon(int position) {

        if (HomeActivity.adapterViewPager.updatedArticleTableList != null && HomeActivity.adapterViewPager.updatedArticleTableList.size() != 0) {

            UpdatedArticleTable updatedArticleTable = HomeActivity.adapterViewPager.updatedArticleTableList.get(position);
            String article_id = updatedArticleTable.getArticleId() + "";
            String category_id = updatedArticleTable.getCategoryId();
            articlePosition = article_id;

            if (checkBookmark(category_id, article_id)) {
                NewsAppUtil.logMessage("sailesh", "check for on article already in db, set on" + HomeActivity.present_category_id + ": " + article_id);
                bookmark_icon.setImageResource(R.drawable.bookmark_icon_on_new);
            } else {
                bookmark_icon.setImageResource(R.drawable.bookmark_icon);
                NewsAppUtil.logMessage("sailesh", "check for on updatedArticleTable not in db, set off" + HomeActivity.present_category_id + ": " + article_id);
            }
        }
    }

    public Boolean checkLikedArticals(String cid, String aid) {

        List<UpdatedLikeTable> likedList = Repo.getInstance(context).repoLike.getArticleBycIDandaID(cid, aid);
        if (likedList == null || likedList.size() == 0)
            return false;
        else
            return true;
    }

    public void fetchLikedArticles() {

        List<UpdatedLikeTable> updatedLikeTableList = Repo.getInstance(context).repoLike.getAll(language);

        if (updatedLikeTableList.size() == 0) {
            Toast.makeText(context, "No Liked Articles to display", Toast.LENGTH_SHORT).show();
            return;
        }

         /*   HomeActivity.adapterViewPager
                    .changeDataSet(HomeActivity.adapterViewPager.LikeToArticles(likeList));
            categoryName = (TextView) getActivity().findViewById(R.id.categoryName);
            categoryName.setText("Liked Articles");
*/
        vpPager = HomeActivity.vpPager;

        vpPager.removeAllViews();


        if (HomeActivity.themeRes == null)
            HomeActivity.adapterViewPager = new NewsAdapter(context, getActivity().getSupportFragmentManager(), true, false, null, selectedList);
        else
            HomeActivity.adapterViewPager = new NewsAdapter(context, getActivity().getSupportFragmentManager(), HomeActivity.themeRes, false, null, selectedList);


        HomeActivity.currentPosition = 0;
        vpPager.setAdapter(HomeActivity.adapterViewPager);
        vpPager.getAdapter().notifyDataSetChanged();
        categoryName = (TextView) getActivity().findViewById(R.id.categoryName);
        categoryName.setText("Liked Articles");
       /* like_icon = (ImageView) getActivity().findViewById(R.id.like_icon);
        setLikeIcon(0);*/
        getDialog().dismiss();
    }

   /* public void setLikeIcon(int position) {

        if (HomeActivity.adapterViewPager.articleList != null && HomeActivity.adapterViewPager.articleList.size() != 0) {
        if (HomeContentFragment.adapterViewPager.updatedArticleTableList != null && HomeContentFragment.adapterViewPager.updatedArticleTableList.size() != 0) {

            Article article = HomeActivity.adapterViewPager.articleList.get(position);
            String article_id = article.getArticleId() + "";
            String category_id = article.getCategoryId();
            if (checkLikedArticals(category_id, article_id)) {
                Toast.makeText(getActivity(), "Article Id : " + article_id, Toast.LENGTH_SHORT).show();
                NewsAppUtil.logMessage("sailesh", "check for on article already in db, set on" + HomeActivity.present_category_id + ": " + article_id);
                //like_icon.setImageResource(R.drawable.heart);
            } else {
                // like_icon.setImageResource(R.drawable.heart_e);
                NewsAppUtil.logMessage("sailesh", "check for on article not in db, set off" + HomeActivity.present_category_id + ": " + article_id);
            }
        }
    }*/


}
