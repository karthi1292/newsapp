package com.tamilflashnews.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.tamilflashnews.R;
import com.tamilflashnews.home.HomeActivity;
import com.tamilflashnews.util.CircleIndicator;
import com.tamilflashnews.util.adapters.HelpScreenAdapter;

public class HelpActivity extends AppCompatActivity
        implements HelpScreenAdapter.HelpAdapterListener {

    // Circle indicator
    private CircleIndicator mCircleIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        ViewPager viewPager = (ViewPager) findViewById(R.id.helpScreenViewPager);
        mCircleIndicator = (CircleIndicator) findViewById(R.id.indicator_help_activity);

        HelpScreenAdapter helpScreenAdapter = new HelpScreenAdapter(getLayoutInflater(),
                this);
        helpScreenAdapter.setOnAdapterLister(this);
        helpScreenAdapter.setOnAdapterLister(this);
        viewPager.setAdapter(helpScreenAdapter);
        mCircleIndicator.setViewPager(viewPager);
        //viewPager.setPageTransformer(true, new DepthPageTransformer());
    }

    @Override
    public void onClick() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
