package com.tamilflashnews.dbhelpers;

import com.j256.ormlite.dao.Dao;
import com.tamilflashnews.model.Category;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RepoCategories {

	Dao<Category, Integer> categoryDao;

	public RepoCategories(DatabaseHelper db)
	{
		try {
			categoryDao = db.getDao(Category.class);
		} catch (Exception e) {
			// TODO: Exception Handling
			e.printStackTrace();
		}
	}
	
	public int insert(Category category)
	{
		try {
            return categoryDao.create(category);
		} catch (SQLException e) {
			// TODO: Exception Handling
			e.printStackTrace();
		}
		return 0;
	}

    public int update(Category category)
    {
        try {
            return categoryDao.update(category);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Category> getAll()
    {
        try {
            return categoryDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public long getCount()
    {
        long count = 0l;
        try {
            count = categoryDao.countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    //TODO Sam[le method to get list of rows using where condition
    public List<Category> getCategoryByID()
    {
        List<Category> categoryList = null;
        try {
            categoryList = categoryDao.queryBuilder()// true for ascending, false for descending
                          .where()
                          .eq("blah", "blah")
                          .and().eq("blah", "blah")
                          .query();
            return categoryList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categoryList;
    }
    public ArrayList<Category> getCategoryByName(String filterName){
        ArrayList<Category> sh = null;
        try{
            sh = (ArrayList) categoryDao.queryBuilder()
                    .where().like("cname", filterName.toLowerCase()+"%").query();
            return sh;
        }catch (Exception e){
            return sh;
        }
    }

    //TODO Sam[le method to get list of rows using where condition
    public String getCategoryByID(String id)
    {
        List<Category> articleList = null;
        try {
            articleList = categoryDao.queryBuilder()// true for ascending, false for descending
                    .where()
                    .eq("categoryId", id)
                    .query();
            return articleList.get(0).getCategoryName();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public int delete(Category category)
    {
        try {
            return categoryDao.delete(category);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


}
