package com.tamilflashnews.dbhelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.android.AndroidDatabaseConnection;
import com.j256.ormlite.android.DatabaseTableConfigUtil;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.table.DatabaseTableConfig;
import com.j256.ormlite.table.TableUtils;
import com.tamilflashnews.model.Article;
import com.tamilflashnews.model.Bookmark;
import com.tamilflashnews.model.Category;
import com.tamilflashnews.model.Like;
import com.tamilflashnews.model.NewsAppConstants;
import com.tamilflashnews.model.UpdatedArticleTable;
import com.tamilflashnews.model.UpdatedLikeTable;
import com.tamilflashnews.util.NewsAppUtil;

import java.sql.SQLException;
import java.util.List;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    // name of the database file for your application -- change to something appropriate for your app
    private static final String DATABASE_NAME = NewsAppConstants.DB_NAME;
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = NewsAppConstants.DB_VERSION;
    protected AndroidConnectionSource connectionSource = new AndroidConnectionSource(this);

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to insert
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        DatabaseConnection conn = connectionSource.getSpecialConnection();
        boolean clearSpecial = false;
        if (conn == null) {
            conn = new AndroidDatabaseConnection(db, true);
            try {
                connectionSource.saveSpecialConnection(conn);
                clearSpecial = true;
            } catch (SQLException e) {
                throw new IllegalStateException("Could not save special connection", e);
            }
        }
        try {
            onCreate();
        } finally {
            if (clearSpecial) {
                connectionSource.clearSpecialConnection(conn);
            }
        }
    }

    @Override
    public final void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        DatabaseConnection conn = connectionSource.getSpecialConnection();
        boolean clearSpecial = false;
        if (conn == null) {
            conn = new AndroidDatabaseConnection(db, true);
            try {
                connectionSource.saveSpecialConnection(conn);
                clearSpecial = true;
            } catch (SQLException e) {
                throw new IllegalStateException("Could not save special connection", e);
            }
        }
        try {
            onUpgrade(oldVersion, newVersion);
        } finally {
            if (clearSpecial) {
                connectionSource.clearSpecialConnection(conn);
            }
        }
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
    }

    private void onCreate() {
        try {
            TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, UpdatedArticleTable.class);
            TableUtils.createTable(connectionSource, Bookmark.class);
            TableUtils.createTable(connectionSource, UpdatedLikeTable.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    private void onUpgrade(int oldVersion, int newVersion) {
        try {

            if (oldVersion == 1 || oldVersion == 2) {

                Dao<Bookmark, Integer> bookmarkDao = getDao(Bookmark.class);
                Dao<Like, Integer> likeDao = getDao(Like.class);
                Dao<Article, Integer> articleDao = getDao(Article.class);

                articleDao.executeRaw("ALTER TABLE 'article' ADD COLUMN image_album VARCHAR DEFAULT 'Tamil';");
                articleDao.executeRaw("ALTER TABLE 'article' ADD COLUMN  scloud VARCHAR;");
                articleDao.executeRaw("ALTER TABLE 'article' ADD COLUMN  language VARCHAR;");
                bookmarkDao.executeRaw("ALTER TABLE 'bookmark' ADD COLUMN scloud VARCHAR;");
                bookmarkDao.executeRaw("ALTER TABLE 'bookmark' ADD COLUMN image_album VARCHAR;");
                bookmarkDao.executeRaw("ALTER TABLE 'bookmark' ADD COLUMN language VARCHAR;");
                likeDao.executeRaw("ALTER TABLE 'like' ADD COLUMN scloud VARCHAR;");
                likeDao.executeRaw("ALTER TABLE 'like' ADD COLUMN image_album VARCHAR;");
                likeDao.executeRaw("ALTER TABLE 'like' ADD COLUMN language VARCHAR;");

                TableUtils.createTable(connectionSource, UpdatedArticleTable.class);
                TableUtils.createTable(connectionSource, UpdatedLikeTable.class);

                Dao<UpdatedArticleTable, Integer> articlesDao = getDao(UpdatedArticleTable.class);
                Dao<UpdatedLikeTable, Integer> updatedLikeDao = getDao(UpdatedLikeTable.class);

                for (Article article : getAllArticles(articleDao)) {
                    insert(articlesDao, new UpdatedArticleTable().fromArticle(article));
                }

                TableUtils.dropTable(connectionSource, Article.class, true);

                likeDao.executeRaw("ALTER TABLE 'like' ADD COLUMN likeId INTEGER;");
                for (Like like : getAll(likeDao)) {
                    insertLike(updatedLikeDao, new UpdatedLikeTable().fromLike(like));
                }

                TableUtils.dropTable(connectionSource, Like.class, true);
                addLanguageColumn();
            } else if (oldVersion == 3) {
                addLanguageColumn();
            }

        /*	NewsAppUtil.logMessage(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Category.class, true);
			TableUtils.dropTable(connectionSource, UpdatedArticleTable.class, true);
			TableUtils.dropTable(connectionSource, Bookmark.class, true);
			TableUtils.dropTable(connectionSource, Like.class, true);*/
            // after we drop the old databases, we insert the new ones
            //  onCreate();
        } catch (SQLException e) {
            NewsAppUtil.logMessage(DatabaseHelper.class.getName(), "Can't drop databases " + e);
            throw new RuntimeException(e);
        }
    }

    public <D extends Dao<T, ?>, T> D getDao(Class<T> clazz) throws SQLException {
        // lookup the dao, possibly invoking the cached database config
        Dao<T, ?> dao = DaoManager.lookupDao(connectionSource, clazz);
        if (dao == null) {
            // try to use our new reflection magic
            DatabaseTableConfig<T> tableConfig = DatabaseTableConfigUtil.fromClass(connectionSource, clazz);
            if (tableConfig == null) {
                /**
                 * TODO: we have to do this to get to see if they are using the deprecated annotations like
                 * {@link DatabaseFieldSimple}.
                 */
                dao = DaoManager.createDao(connectionSource, clazz);
            } else {
                dao = DaoManager.createDao(connectionSource, tableConfig);
            }
        }

        @SuppressWarnings("unchecked")
        D castDao = (D) dao;
        return castDao;
    }

    public <T> void clearTable(Class<T> clazz) {
        try {
            TableUtils.clearTable(connectionSource, clazz);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //Add Language column in UpdatedLikeTable,UpdatedArticleTable,BookmarkTable
    public void addLanguageColumn(){
        try{
            Dao<Bookmark, Integer> bookmarkDao = getDao(Bookmark.class);
            Dao<UpdatedLikeTable, Integer> updatedLikeDao = getDao(UpdatedLikeTable.class);
            Dao<UpdatedArticleTable, Integer> updatedArticleDao = getDao(UpdatedArticleTable.class);

            updatedArticleDao.executeRaw("ALTER TABLE 'updatedArticle' ADD COLUMN language VARCHAR;");
            bookmarkDao.executeRaw("ALTER TABLE 'bookmark' ADD COLUMN language VARCHAR;");
            updatedLikeDao.executeRaw("ALTER TABLE 'updatedLike' ADD COLUMN language VARCHAR;");

            for (UpdatedLikeTable updatedLikeOldArticle : getAllLikeOldArticles(updatedLikeDao)) {
                updatedLikeOldArticle.setLanguage("tamil");
                updateLike(updatedLikeDao, updatedLikeOldArticle);
            }
            for (UpdatedArticleTable updatedOldArticle : getAllOldArticles(updatedArticleDao)) {
                updatedOldArticle.setLanguage("tamil");
                update(updatedArticleDao, updatedOldArticle);
            }
            for (Bookmark bookmarkOldArticle : getAllBookmarkOldArticles(bookmarkDao)) {
                bookmarkOldArticle.setLanguage("tamil");
                updateBookmark(bookmarkDao,bookmarkOldArticle);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
    public List<Article> getAllArticles(Dao<Article, Integer> articleDao) {

        List<Article> articlesList = null;
        try {
            articlesList = articleDao.queryBuilder()
                    .orderBy("articleId", false)// true for ascending, false for descending
                    .query();
            return articlesList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UpdatedArticleTable> getAllOldArticles(Dao<UpdatedArticleTable, Integer> updatedArticleDao) {

        List<UpdatedArticleTable> articlesList = null;
        try {
            articlesList = updatedArticleDao.queryBuilder()
                    .orderBy("articleId", false)// true for ascending, false for descending
                    .query();
            return articlesList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UpdatedLikeTable> getAllLikeOldArticles(Dao<UpdatedLikeTable, Integer> updatedLikeDao) {

        List<UpdatedLikeTable> likeArticlesList = null;
        try {
            likeArticlesList = updatedLikeDao.queryBuilder()
                    .orderBy("articleId", false)// true for ascending, false for descending
                    .query();
            return likeArticlesList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<Bookmark> getAllBookmarkOldArticles(Dao<Bookmark, Integer> bookmarkDao) {

        List<Bookmark> bookmarkArticlesList = null;
        try {
            bookmarkArticlesList = bookmarkDao.queryBuilder()
                    .orderBy("articleId", false)// true for ascending, false for descending
                    .query();
            return bookmarkArticlesList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<Like> getAll(Dao<Like, Integer> likeDao) {
        List<Like> likedList = null;
        try {
            likedList = likeDao.queryBuilder()
                    .orderBy("likeId", false)
                    .query();// true for ascending, false for descending

            return likedList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int insert(Dao<UpdatedArticleTable, Integer> articleDao, UpdatedArticleTable updatedArticleTable) {
        try {
            return articleDao.create(updatedArticleTable);
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }

    public int insertLike(Dao<UpdatedLikeTable, Integer> likeDao, UpdatedLikeTable updatedLikeTable) {
        try {
            return likeDao.create(updatedLikeTable);
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }

    public int update(Dao<UpdatedArticleTable, Integer> updatedArticleDao,UpdatedArticleTable updatedArticleTable) {
        Integer id = null;
        try {
            id = updatedArticleDao.extractId(updatedArticleTable);
            if (id == null || !updatedArticleDao.idExists(id)) {
                int numRows = updatedArticleDao.create(updatedArticleTable);
                new Dao.CreateOrUpdateStatus(true, false, numRows);
                return 0;
            } else {
                int numRows = updatedArticleDao.update(updatedArticleTable);
                new Dao.CreateOrUpdateStatus(false, true, numRows);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int updateLike(Dao<UpdatedLikeTable, Integer> updatedLikeDao,UpdatedLikeTable updatedLikeTable) {
        Integer id = null;
        try {
            id = updatedLikeDao.extractId(updatedLikeTable);
            if (id == null || !updatedLikeDao.idExists(id)) {
                int numRows = updatedLikeDao.create(updatedLikeTable);
                new Dao.CreateOrUpdateStatus(true, false, numRows);
                return 0;
            } else {
                int numRows = updatedLikeDao.update(updatedLikeTable);
                new Dao.CreateOrUpdateStatus(false, true, numRows);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int updateBookmark(Dao<Bookmark, Integer> bookmarkDao,Bookmark bookmarkTable) {
        Integer id = null;
        try {
            id = bookmarkDao.extractId(bookmarkTable);
            if (id == null || !bookmarkDao.idExists(id)) {
                int numRows = bookmarkDao.create(bookmarkTable);
                new Dao.CreateOrUpdateStatus(true, false, numRows);
                return 0;
            } else {
                int numRows = bookmarkDao.update(bookmarkTable);
                new Dao.CreateOrUpdateStatus(false, true, numRows);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


}