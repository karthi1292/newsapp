package com.tamilflashnews.dbhelpers;


import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.Where;
import com.tamilflashnews.model.UpdatedArticleTable;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RepoArticles {

    Dao<UpdatedArticleTable, Integer> articleDao;

    public RepoArticles(DatabaseHelper db) {
        try {
            articleDao = db.getDao(UpdatedArticleTable.class);
        } catch (Exception e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
    }

    public int insert(UpdatedArticleTable updatedArticleTable) {
        try {
            return articleDao.create(updatedArticleTable);
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }

    /* public int update(UpdatedArticleTable updatedArticleTable) {
         try {
             return articleDao.update(updatedArticleTable);
         } catch (SQLException e) {
             e.printStackTrace();
         }
         return 0;
     }
 */
    public int update(UpdatedArticleTable updatedArticleTable) {
        Integer id = null;
        try {
            id = articleDao.extractId(updatedArticleTable);
            if (id == null || !articleDao.idExists(id)) {
                int numRows = articleDao.create(updatedArticleTable);
                new Dao.CreateOrUpdateStatus(true, false, numRows);
                return 0;
            } else {
                int numRows = articleDao.update(updatedArticleTable);
                new Dao.CreateOrUpdateStatus(false, true, numRows);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public List<UpdatedArticleTable> getAll() {
        try {
            return articleDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UpdatedArticleTable> getAllArticlesById() {

        List<UpdatedArticleTable> updatedArticleTableList = null;
        try {
            updatedArticleTableList = articleDao.queryBuilder()
                    .orderBy("articleId", true)// true for ascending, false for descending
                    .query();
            return updatedArticleTableList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UpdatedArticleTable> getAllArticlesDescenOrderById() {

        List<UpdatedArticleTable> updatedArticleTableList = null;
        try {
            updatedArticleTableList = articleDao.queryBuilder()
                    .orderBy("articleId", false)// true for ascending, false for descending
                    .query();
            return updatedArticleTableList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UpdatedArticleTable> getAllArticlesByDateTime() {

        List<UpdatedArticleTable> updatedArticleTableList = null;
        try {
            updatedArticleTableList = articleDao.queryBuilder()
                    .orderBy("cdate", true)// true for ascending, false for descending
                    .query();
            Log.d("ArtcileListByDate", "" + updatedArticleTableList);


            return updatedArticleTableList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UpdatedArticleTable> getAllArticlesByDescenDateTime(String language) {

        List<UpdatedArticleTable> updatedArticleTableList = null;
        try {
            updatedArticleTableList = articleDao.queryBuilder()
                    .orderBy("cdate", false).where().eq("language",language)// true for ascending, false for descending
                    .query();
            Log.d("ArtcileListByDate", "" + updatedArticleTableList);
            for (int i = 0; i < updatedArticleTableList.size(); i++) {
                String lastArticleUpdatedDesc = updatedArticleTableList.get(i).getArticleDesc();
                Log.d("lastArticleUpdatedDesc", "" + lastArticleUpdatedDesc);
            }
            return updatedArticleTableList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UpdatedArticleTable> getAllArticles() {

        List<UpdatedArticleTable> updatedArticleTableList = null;
        try {
            updatedArticleTableList = articleDao.queryBuilder()
                    .orderBy("articleId", false)// true for ascending, false for descending
                    .query();
            return updatedArticleTableList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public long getCount() {
        long count = 0l;
        try {
            count = articleDao.countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    //TODO Sam[le method to get list of rows using where condition
    public List<UpdatedArticleTable> getArticleByID(String id,String language) {
        List<UpdatedArticleTable> updatedArticleTableList = null;
        try {
            updatedArticleTableList = articleDao.queryBuilder()// true for ascending, false for descending
                    .orderBy("cdate", false)
                    .where()
                    .eq("categoryId", id).and().eq("language",language)
                    .query();
            return updatedArticleTableList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updatedArticleTableList;
    }

    public List<UpdatedArticleTable> getSelectedCategoryArticleByID(List<String> ids,String language) {
        List<UpdatedArticleTable> updatedArticleTableList = null;
        Where<UpdatedArticleTable, Integer> where = articleDao.queryBuilder().orderBy("cdate", false).where();
        try {
            where=selectedCatgeoryWhereQuery(ids,where,language);
           // where.or(where.eq("categoryId", ids.get(1)), where.eq("categoryId", ids.get(2)), where.eq("categoryId", ids.get(3)));
            updatedArticleTableList = where.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updatedArticleTableList;
    }


    public Where<UpdatedArticleTable, Integer> selectedCatgeoryWhereQuery(List<String> ids, Where<UpdatedArticleTable, Integer> where,String language) {
        try {
            switch (ids.size()) {
                case 1:
                    where.eq("categoryId", ids.get(0)).and().eq("language", language);
                    break;
                case 2:
                    where.or(where.eq("categoryId", ids.get(0)), where.eq("categoryId", ids.get(1))).and().eq("language",language);
                    break;
                case 3:
                    where.or(where.eq("categoryId", ids.get(0)), where.eq("categoryId", ids.get(1)), where.eq("categoryId", ids.get(2))).and().eq("language", language);
                    break;
                case 4:
                    where.or(where.eq("categoryId", ids.get(0)), where.eq("categoryId", ids.get(1)), where.eq("categoryId", ids.get(2)), where.eq("categoryId", ids.get(3))).and().eq("language", language);
                    break;
                case 5:
                    where.or(where.eq("categoryId", ids.get(0)), where.eq("categoryId", ids.get(1)), where.eq("categoryId", ids.get(2)), where.eq("categoryId", ids.get(3)), where.eq("categoryId", ids.get(4))).and().eq("language", language);
                    break;
                case 6:
                    where.or(where.eq("categoryId", ids.get(0)), where.eq("categoryId", ids.get(1)), where.eq("categoryId", ids.get(2)), where.eq("categoryId", ids.get(3)), where.eq("categoryId", ids.get(4)), where.eq("categoryId", ids.get(5))).and().eq("language", language);
                    break;
                case 7:
                    where.or(where.eq("categoryId", ids.get(0)), where.eq("categoryId", ids.get(1)), where.eq("categoryId", ids.get(2)), where.eq("categoryId", ids.get(3)), where.eq("categoryId", ids.get(4)), where.eq("categoryId", ids.get(5)), where.eq("categoryId", ids.get(6))).and().eq("language", language);
                    break;
                case 8:
                    where.or(where.eq("categoryId", ids.get(0)), where.eq("categoryId", ids.get(1)), where.eq("categoryId", ids.get(2)), where.eq("categoryId", ids.get(3)), where.eq("categoryId", ids.get(4)), where.eq("categoryId", ids.get(5)), where.eq("categoryId", ids.get(6)), where.eq("categoryId", ids.get(7))).and().eq("language", language);
                    break;
                case 9:
                    where.or(where.eq("categoryId", ids.get(0)), where.eq("categoryId", ids.get(1)), where.eq("categoryId", ids.get(2)), where.eq("categoryId", ids.get(3)), where.eq("categoryId", ids.get(4)), where.eq("categoryId", ids.get(5)), where.eq("categoryId", ids.get(6)), where.eq("categoryId", ids.get(7)), where.eq("categoryId", ids.get(8))).and().eq("language", language);
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return where;
    }

    public ArrayList<UpdatedArticleTable> getArticleByName(String filterName) {
        ArrayList<UpdatedArticleTable> sh = null;
        try {
            sh = (ArrayList) articleDao.queryBuilder()
                    .where().like("atitle", filterName.toLowerCase() + "%").query();
            return sh;
        } catch (Exception e) {
            return sh;
        }
    }

    public int delete(UpdatedArticleTable updatedArticleTable) {
        try {
            return articleDao.delete(updatedArticleTable);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


}

