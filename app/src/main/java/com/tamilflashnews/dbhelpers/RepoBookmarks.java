package com.tamilflashnews.dbhelpers;


import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.tamilflashnews.model.Bookmark;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class RepoBookmarks {

    Dao<Bookmark, Integer> bookmarkDao;

    public RepoBookmarks(DatabaseHelper db)
    {
        try {
            bookmarkDao = db.getDao(Bookmark.class);
        } catch (Exception e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
    }

    public int insert(Bookmark bookmark)
    {
        try {
            return bookmarkDao.create(bookmark);
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }

    public int update(Bookmark bookmark)
    {
        try {
            return bookmarkDao.update(bookmark);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Bookmark> getAll(String language)
    {
        List<Bookmark> bookmarkList = null;
        try{
            bookmarkList = bookmarkDao.queryBuilder()
                    .orderBy("bookmarkId", false).where().eq("language",language)// true for ascending, false for descending
                    .query();
            return bookmarkList;
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return  null;
    }

    public long getCount()
    {
        long count = 0l;
        try {
            count = bookmarkDao.countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    //TODO Sam[le method to get list of rows using where condition
    public List<Bookmark> getArticleByID(String id)
    {
        List<Bookmark> bookmarkList = null;
        try {
            bookmarkList = bookmarkDao.queryBuilder()// true for ascending, false for descending
                    .where()
                    .eq("articleId", id)
                    .query();
            return bookmarkList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookmarkList;
    }

    public List<Bookmark> getArticleBycIDandaID(String cid, String aid)
    {
        List<Bookmark> bookmarkList = null;
        try {
            bookmarkList = bookmarkDao.queryBuilder()// true for ascending, false for descending
                    .where()
                    .eq("articleId", aid).and().eq("categoryId",cid)
                    .query();
            return bookmarkList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookmarkList;
    }

    public ArrayList<Bookmark> getArticleByName(String filterName){
        ArrayList<Bookmark> sh = null;
        try{
            sh = (ArrayList) bookmarkDao.queryBuilder()
                    .where().like("atitle", filterName.toLowerCase()+"%").query();
            return sh;
        }catch (Exception e){
            return sh;
        }
    }

    public int delete(Bookmark bookmark)
    {
        try {
            return bookmarkDao.delete(bookmark);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int deleteById(String id)
    {
        DeleteBuilder<Bookmark, Integer> deleteBuilder = bookmarkDao.deleteBuilder();
        try {
            deleteBuilder.where().eq("articleId", id);
            deleteBuilder.delete();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


}

