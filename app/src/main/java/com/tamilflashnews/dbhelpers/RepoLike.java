package com.tamilflashnews.dbhelpers;


import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.tamilflashnews.model.UpdatedLikeTable;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class RepoLike {

    Dao<UpdatedLikeTable, Integer> likeDao;

    public RepoLike(DatabaseHelper db) {
        try {
            likeDao = db.getDao(UpdatedLikeTable.class);
        } catch (Exception e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
    }

    public int insert(UpdatedLikeTable updatedLikeTable) {
        try {
            return likeDao.create(updatedLikeTable);
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }

    public int update(UpdatedLikeTable updatedLikeTable) {
        try {
            return likeDao.update(updatedLikeTable);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<UpdatedLikeTable> getAll(String language) {
        List<UpdatedLikeTable> likedList = null;
        try {
            likedList = likeDao.queryBuilder()
                    .orderBy("likeId", false).where().eq("language",language)
                    .query();// true for ascending, false for descending

            return likedList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public long getCount() {
        long count = 0l;
        try {
            count = likeDao.countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    //TODO Sam[le method to get list of rows using where condition
    public List<UpdatedLikeTable> getArticleByID(String id) {
        List<UpdatedLikeTable> updatedLikeTableList = null;
        try {
            updatedLikeTableList = likeDao.queryBuilder()// true for ascending, false for descending
                    .where()
                    .eq("articleId", id)
                    .query();
            return updatedLikeTableList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updatedLikeTableList;
    }

    public List<UpdatedLikeTable> getArticleBycIDandaID(String cid, String aid) {

        List<UpdatedLikeTable> updatedLikeTableList = null;
        try {
            updatedLikeTableList = likeDao.queryBuilder()// true for ascending, false for descending
                    .where()
                    .eq("articleId", aid).and().eq("categoryId", cid)
                    .query();
            return updatedLikeTableList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updatedLikeTableList;
    }

    public ArrayList<UpdatedLikeTable> getArticleByName(String filterName) {
        ArrayList<UpdatedLikeTable> sh = null;
        try {
            sh = (ArrayList) likeDao.queryBuilder()
                    .where().like("atitle", filterName.toLowerCase() + "%").query();
            return sh;
        } catch (Exception e) {
            return sh;
        }
    }

    public int delete(UpdatedLikeTable updatedLikeTable) {
        try {
            return likeDao.delete(updatedLikeTable);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int deleteById(String id) {
        DeleteBuilder<UpdatedLikeTable, Integer> deleteBuilder = likeDao.deleteBuilder();
        try {
            deleteBuilder.where().eq("articleId", id);
            deleteBuilder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


}

