package com.tamilflashnews.dbhelpers;
import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.tamilflashnews.model.UpdatedArticleTable;
import com.tamilflashnews.model.Category;
import com.tamilflashnews.util.NewsAppUtil;

public class Repo {

    DatabaseHelper db;
    private static Repo instance = null;

    public RepoCategories repoCategories;
    public RepoArticles repoArticles;
    public RepoBookmarks repoBookmarks;
    public  RepoLike repoLike;
    private Context mContext;

	public Repo(Context context) {
        this.mContext = context;
	}

    public static Repo getInstance(Context context) {
        if(instance == null) instance = new Repo(context);
        return instance;
    }

    public void buildDao() {
        db = new DatabaseHelper(mContext);
        this.repoCategories = new RepoCategories(db);
        this.repoArticles = new RepoArticles(db);
        this.repoBookmarks = new RepoBookmarks(db);
        this.repoLike = new RepoLike(db);
    }

    public DatabaseHelper getDatabaseHelper(){
        db = (db==null)?new DatabaseHelper(mContext):db;
        return db;
    }

    public <T> void clearTable(Class<T> clazz) {
        NewsAppUtil.logMessage("updatemaster", "onmagazineupdate-cleartable");
        db.clearTable(clazz);
    }

    public <T> void clearAllTables() {
        db.clearTable(Category.class);
        db.clearTable(UpdatedArticleTable.class);
    }

    public void closeDb() {
        if (db == null) {
            OpenHelperManager.releaseHelper();
        }
        db = null;
    }

}
