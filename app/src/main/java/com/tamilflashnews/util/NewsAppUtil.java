package com.tamilflashnews.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.securepreferences.SecurePreferences;
import com.tamilflashnews.BuildConfig;
import com.tamilflashnews.model.NewsAppConstants;

/**
 * Created by System No 07 on 7/11/2015.
 */
public class NewsAppUtil {

    private static NewsAppUtil instance = null;
    private SecurePreferences mSecurePrefs;
    private static Context mContext;
    public static boolean logSwitch = false;
    private final static String TAG = NewsAppUtil.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private Typeface font, font1, roboto_font, roboto_regular, roboto_regular_italic;

    public NewsAppUtil(Context context) {
        this.mContext = context;
    }

    public static NewsAppUtil getInstance(Context context) {
        if (instance == null) instance = new NewsAppUtil(context);
        return instance;
    }

    public SecurePreferences getSecurePrefs() {
        mSecurePrefs = (null == mSecurePrefs) ? new SecurePreferences(mContext) : mSecurePrefs;
        return mSecurePrefs;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static boolean checkPlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        boolean isAvailable = resultCode == ConnectionResult.SUCCESS;
        if (!isAvailable) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
        }
        return isAvailable;
    }

    /**
     * Get Font Typefaces
     **/
    public Typeface getContentFontTypeFace() {
        return (null == font) ? Typeface.createFromAsset(mContext.getAssets(), NewsAppConstants.CONTENT_TYPEFACE) : font;
//        return font;
    }

    public Typeface getRobotoTypeFace() {
        return (null == roboto_font) ? Typeface.createFromAsset(mContext.getAssets(), NewsAppConstants.ROBOTO_TYPEFACE) : roboto_font;
    }

    public Typeface getRobotoRegularTypeFace() {
        return (null == roboto_font) ? Typeface.createFromAsset(mContext.getAssets(), NewsAppConstants.ROBOTO_REGULAR) : roboto_font;
    }

    public Typeface getRobotoRegularItalicTypeFace() {
        return (null == roboto_regular_italic) ? Typeface.createFromAsset(mContext.getAssets(), NewsAppConstants.ROBOTO_REGULAR_ITALIC) : roboto_regular_italic;
    }

    public Typeface getTitleFontFace() {
        return (null == roboto_regular) ? Typeface.createFromAsset(mContext.getAssets(), NewsAppConstants.HEADER_TYPEFACE) : roboto_regular;
    }


    /**
     * Get Request Urls
     **/
    public static String getReqUrlWithMod(String moduleName) {

        NewsAppUtil.logMessage("request url is: ", NewsAppConstants.BASE_URL + "&module=" + moduleName);
        return getBaseUrl() + "&module=" + moduleName;
    }

    public static String getReqUrlWithLastId(String moduleName, String viewName, String lastId) {

        NewsAppUtil.logMessage("request url is date: ", NewsAppConstants.BASE_URL + "&lid=" + lastId);
        return getBaseUrl() + "&lid=" + lastId;
    }

    public static String getReqUrlWithModDate(String moduleName, String date) {

        NewsAppUtil.logMessage("request url is date: ", getReqUrlWithMod(moduleName) + "&udate=" + date);
        return getReqUrlWithMod(moduleName) + "&udate=" + date;
    }

    public static String getReqUrlWithModAndView(String moduleName, String viewName) {
        return getBaseUrl() + "&module=" + moduleName
                + "&view=" + viewName;
    }

    /**
     * Get Request Urls
     **/
    public static String getReqUrlWithModViewAndLid(String moduleName, String viewName, String lid) {
        return getReqUrlWithModAndView(moduleName, viewName) + "&lid=" + lid;
    }

    public static String getReqUrlWithNewsModuleAndCategory() {
        return getReqUrlWithModAndView(NewsAppConstants.MODULE_ARTICLES, NewsAppConstants.VIEW_ARCHIVE);
    }

    public static String getReqUrlWithNewsModuleCategoryAndUpdatedDateTime(String updatedDateTime) {
        // Log.d("New IUrl:")
        return getBaseUrl() + "&module=" + NewsAppConstants.MODULE_ARTICLES + "&u_time=" + updatedDateTime;
    }

    public static String getReqUrlWithNewsModuleCategoryAndCID(String lid) {
        return getReqUrlWithModViewAndLid(NewsAppConstants.MODULE_ARTICLES, NewsAppConstants.VIEW_ARCHIVE, lid);
    }

    public static String getReqUrlWithNewsModuleCategoryCIDAndLID(String lid, String cid) {
        return getReqUrlWithModViewAndLid(NewsAppConstants.MODULE_ARTICLES, NewsAppConstants.VIEW_ARCHIVE, lid) + "&cid=" + cid;
    }

    /**
     * Google Analytics
     **/
    public GoogleAnalytics analytics;
    public Tracker tracker;

    public synchronized Tracker getDefaultTracker() {
        if (tracker == null) {
            registerAnalytics();
        }
        return tracker;
    }

    public void registerAnalytics() {
        analytics = GoogleAnalytics.getInstance(mContext);
        analytics.setLocalDispatchPeriod(1800);
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        tracker = analytics.newTracker("UA-64456798-1");
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
        tracker.setAppVersion(BuildConfig.VERSION_NAME);
    }

    /**
     * Just For Logging
     **/

    public static void logMessage(String logMessage) {
        if (logSwitch) {
            logMessage(TAG, logMessage);
        }
    }

    public static void logMessage(String tag, String logMessage) {
        if (logSwitch) {

            Log.d(tag, logMessage);
        }
    }

    public static void logError(String logMessage) {

        if (logSwitch) {

            logError(TAG, logMessage);
        }
    }

    public static void logError(String tag, String logMessage) {
        if (logSwitch) {
            Log.e(tag, logMessage);
        }
    }

    public static String getBaseUrl() {

        String DEVICE_ID = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        String DEV_URL = "http://www.tamilflashnews.com/api/index.php?version=" + NewsAppConstants.APP_VERSION + "&platform=android" + "&Device_ID=" + DEVICE_ID;
        // String DEV_URL = "http://www.tamilflashnews.com/api/index.php";
        String BASE_URL = DEV_URL;
        return BASE_URL;
    }
}
