package com.tamilflashnews.util.gcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.tamilflashnews.model.NewsAppConstants;
import com.tamilflashnews.model.VolleyJsonObjectResponder;
import com.tamilflashnews.util.NewsAppUtil;
import com.tamilflashnews.util.network.VolleyRequester;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CIPL0233 on 11/25/2015.
 */
public class OnUpdateReceiver extends BroadcastReceiver {
    public static Context mContext;
    private int mCurrentVersion = 1;

    public static void registerGCM() {
        String token1 = NewsAppUtil.getInstance(mContext).getSecurePrefs().getString(NewsAppConstants.PUSH_TOKEN_KEY, "");
        if (token1.isEmpty() && NewsAppUtil.checkPlayServices(mContext)) {

            // Start IntentService to register this application with GCM.
            SharedPreferences preferences = mContext.getSharedPreferences("PushNotification", Context.MODE_PRIVATE);
            SharedPreferences.Editor editorPref = preferences.edit();
            editorPref.putBoolean("isActive", true);
            editorPref.commit();

            Intent pushintent1 = new Intent(mContext, RegistrationIntentService.class);
            mContext.startService(pushintent1);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        try {
            mCurrentVersion = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String pushStatus = NewsAppUtil.getInstance(mContext).getSecurePrefs()
                .getString("PushStatus", "");
        // if ((mCurrentVersion >= 6)) {
        //  if (pushStatus.equals("")) {
        if (intent.getAction().equals("android.intent.action.PACKAGE_REPLACED")) {

            NewsAppUtil.getInstance(mContext).getSecurePrefs().edit()
                    .putString("PushStatus", "AppVersionAPI").commit();

            String token = NewsAppUtil.getInstance(mContext).getSecurePrefs().getString(NewsAppConstants.PUSH_TOKEN_KEY, "");
            if (!token.isEmpty() && NewsAppUtil.checkPlayServices(mContext)) {
                // Start IntentService to Unregister this application with GCM.

                SharedPreferences preferences = mContext.getSharedPreferences("PushNotification", Context.MODE_PRIVATE);
                SharedPreferences.Editor editorPref = preferences.edit();
                editorPref.putBoolean("isActive", false);
                editorPref.commit();

                Intent pushintent = new Intent(mContext, RegistrationIntentService.class);
                pushintent.putExtra("Register", "Register GCM");
                mContext.startService(pushintent);

            }
            getAllArticles(mContext);
        }
    }

    private void getAllArticles(final Context mContext) {
        VolleyRequester.getInstance(mContext)
                .sendVolleyJsonObjectRequest(NewsAppConstants.ALL_ARTICLES_REQUEST_TAG,
                        NewsAppUtil.getInstance(mContext).getReqUrlWithMod(NewsAppConstants.MODULE_ARTICLES), new VolleyJsonObjectResponder() {

                            @Override
                            public void onJsonObjectResponseSuccess(JSONObject response) {
                                try {
                                    if (!response.isNull("bpath")) {
                                        String appendImagePath = response.getString("bpath");
                                        Log.e("URL", "" + appendImagePath);
                                        SharedPreferences sharedpreferencesImagePath = mContext.getSharedPreferences("downloadedAllArticles", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor imagePath = sharedpreferencesImagePath.edit();
                                        imagePath.putString("bpath", appendImagePath);
                                        imagePath.commit();
                                    }
                                } catch (JSONException je) {
                                    je.printStackTrace();
                                }
                            }

                            @Override
                            public void onJsonObjectResponseFailure(VolleyError volleyError) {
                                NewsAppUtil.logError("Cannot retrieve bpath " + volleyError);
                                if (volleyError.networkResponse == null) {
                                    if (volleyError.getClass().equals(TimeoutError.class)) {
                                        // Show timeout error message
                                       getAllArticles(mContext);
                                    }
                                }
                            }
                        });

    }
}
