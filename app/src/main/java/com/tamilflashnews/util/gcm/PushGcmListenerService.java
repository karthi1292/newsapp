/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tamilflashnews.util.gcm;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gms.gcm.GcmListenerService;
import com.tamilflashnews.R;
import com.tamilflashnews.service.UpdatedArticlesSync;
import com.tamilflashnews.splash.SplashActivity;
import com.tamilflashnews.util.NewsAppUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;

public class PushGcmListenerService extends GcmListenerService {


    private static final String TAG = "PushGcmListenerService";
    SharedPreferences sharedPreferencesPushNotifications;
    SharedPreferences.Editor editorPushNotifications;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {

       /* sharedPreferencesPushNotifications = getSharedPreferences("PushNotifications", MODE_PRIVATE);
        editorPushNotifications = sharedPreferencesPushNotifications.edit();
        editorPushNotifications.putBoolean("PushReceived",true);
        editorPushNotifications.commit();*/

        Log.i("push message", "message received");
        NewsAppUtil.logMessage("push message", "message received");
        String message = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        JSONObject json = new JSONObject();
        try {
            if (message != null) {
                json = new JSONObject(message);
                if (json != null)
                    json = new JSONObject(json.getJSONObject("aps_data").toString());
                Log.d(TAG, "Json: " + json.toString() + "\n");
            }

        } catch (JSONException e) {
            Log.e("push message", "unexpected JSON exception", e);
        }
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        String pushStatus = NewsAppUtil.getInstance(this).getSecurePrefs().getString("pushEnabled", "notSet");
        //   UpdatedArticlesSync updatedArticlesSync=new UpdatedArticlesSync();
        //   getApplicationContext().u

        if (pushStatus.equals("notSet") || pushStatus.equals("set")) {
            if (message != null && json != null)
                sendNotification(json);
        }

        /*if (pushStatus.equals("set")) {
            sendNotification(json);
        } else {
            Toast.makeText(getApplicationContext(), "Push Notification turned off", Toast.LENGTH_LONG).show();
        }*/

    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(JSONObject message) {

        NewsAppUtil.logMessage("push message", "message received is: " + message);

        String articleId = "";
        String articleTitle = "";
        String imageUrl = "";
        boolean isExpandedRemoteView = false;
        try {
            articleId = message.getString("article_id");
            articleTitle = message.getString("article_title");
            imageUrl = message.getString("image");
        } catch (JSONException e) {
            Log.e("push message", "unexpected JSON exception", e);
        }


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

        Intent intent = new Intent(this, SplashActivity.class);
        NewsAppUtil.logMessage("push message1", "message received is: " + articleId);
        intent.putExtra("articleId", articleId);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, Integer.parseInt(articleId) /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        notificationBuilder.setContentIntent(pendingIntent);

        //notificationBuilder.setContentIntent(sharePendingIntent);
        // Sets the ticker text
        notificationBuilder.setTicker("Tamil Flash News");
        // Sets the small icon for the ticker
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);

        notificationBuilder.setSound(defaultSoundUri);

        // Cancel the notification when clicked
        notificationBuilder.setAutoCancel(true);

        //notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(""));
        //notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle());
        // Build the notification
        Notification notification = notificationBuilder.build();

        // Inflate the notification layout as RemoteViews
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_notification);

        // Set text on a TextView in the RemoteViews programmatically.
        final String time = DateFormat.getTimeInstance().format(new Date()).toString();

        contentView.setTextViewText(R.id.timeTextView, time);

        contentView.setTextViewText(R.id.contentTextView, articleTitle);

        /* Workaround: Need to set the content view here directly on the notification.
         * NotificationCompatBuilder contains a bug that prevents this from working on platform
         * versions HoneyComb.
         * See https://code.google.com/p/android/issues/detail?id=30495
         */
        notification.contentView = contentView;

        // Add a big content view to the notification if supported.
        // Support for expanded notifications was added in API level 16.
        // (The normal contentView is shown when the notification is collapsed, when expanded the
        // big content view set here is displayed.)

        if (Build.VERSION.SDK_INT >= 16) {

            //notification = new Notifica

            // Inflate and set the layout for the expanded notification view
            RemoteViews expandedView =
                    new RemoteViews(getPackageName(), R.layout.custom_notification_expanded);

            Bitmap bitmapImage = null;
            try {
                URL url = new URL(imageUrl);
                bitmapImage = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Intent shareIntent = new Intent(this, SplashActivity.class);
            NewsAppUtil.logMessage("push message2", "message received is: " + articleId);
            shareIntent.putExtra("shareArticleId", articleId);
            shareIntent.putExtra("shareClick", true);
            shareIntent.putExtra("shareArticleTitle", articleTitle);
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent sharePendingIntent = PendingIntent.getActivity(this, Integer.parseInt(articleId) /*Request code*/, shareIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            expandedView.setTextViewText(R.id.txt_article_title, articleTitle);
            expandedView.setTextViewText(R.id.time_text_view, time);
            expandedView.setImageViewBitmap(R.id.richImageView, bitmapImage);
            expandedView.setOnClickPendingIntent(R.id.shareImageView, sharePendingIntent);

            notification.bigContentView = expandedView;
        }

        // onResetStartWebService();

        // Use the NotificationManager to show the notification
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NewsAppUtil.logMessage("push message3", "message received is: " + articleId);
        nm.notify(Integer.parseInt(articleId), notification);
    }


    //Reset Updating Article Web Service and Start it again to show exact article
    public void onResetStartWebService() {
        SharedPreferences sharedPreferencesUpdateArticlesAlarm;
        boolean UpdatedArticlesStartAPIAlarm, UpdatedArticlesStopAPIAlarm;

        //Reset Alarm
        sharedPreferencesUpdateArticlesAlarm = getSharedPreferences("UpdatedArticlesAlarm", MODE_PRIVATE);
        UpdatedArticlesStopAPIAlarm = sharedPreferencesUpdateArticlesAlarm.getBoolean("UpdatedArticlesAPIAlarm", false);
        if (UpdatedArticlesStopAPIAlarm) {
            Intent cancelUpdateArticleIntent = new Intent(this, UpdatedArticlesSync.class);
            PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 234324243, cancelUpdateArticleIntent, 0);
            AlarmManager cancelAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            cancelAlarmManager.cancel(cancelPendingIntent);

            SharedPreferences.Editor editorUpdateArticles = sharedPreferencesUpdateArticlesAlarm.edit();
            editorUpdateArticles.putBoolean("UpdatedArticlesAPIAlarm", false);
            editorUpdateArticles.commit();
        }

        //Start Alarm
        UpdatedArticlesStartAPIAlarm = sharedPreferencesUpdateArticlesAlarm.getBoolean("UpdatedArticlesAPIAlarm", false);
        if (!UpdatedArticlesStartAPIAlarm) {
            Intent intent = new Intent(this, UpdatedArticlesSync.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 234324243, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            // alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), (60000), pendingIntent);
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, (60000), pendingIntent);

            SharedPreferences.Editor editorUpdateArticles = sharedPreferencesUpdateArticlesAlarm.edit();
            editorUpdateArticles.putBoolean("UpdatedArticlesAPIAlarm", true);
            editorUpdateArticles.commit();
        }
    }
}

