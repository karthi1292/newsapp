/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tamilflashnews.util.gcm;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;

import com.tamilflashnews.model.NewsAppConstants;
import com.android.volley.VolleyError;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.tamilflashnews.R;
import com.tamilflashnews.model.NewsAppConstants;
import com.tamilflashnews.model.VolleyJsonObjectResponder;
import com.tamilflashnews.util.NewsAppUtil;
import com.tamilflashnews.util.network.VolleyRequester;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    private String registerGCM;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        registerGCM = intent.getStringExtra("Register");
        try {
            // In the (unlikely) event that multiple refresh operations occur simultaneously,
            // ensure that they are processed sequentially.
            synchronized (TAG) {
                SharedPreferences preferences = getSharedPreferences("PushNotification", Context.MODE_PRIVATE);
                boolean isActive = preferences.getBoolean("isActive", false);

                String pushStatus = NewsAppUtil.getInstance(getApplicationContext()).getSecurePrefs().getString("pushEnabled", "notSet");

                String token = null;
                InstanceID instanceID;
                if (isActive) {
                    // [START register_for_gcm]
                    // Initially this call goes out to the network to retrieve the token, subsequent calls
                    // are local.
                    // [START get_token]
                    instanceID = InstanceID.getInstance(this);
                    token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    // [END get_token]
                    NewsAppUtil.logMessage(TAG, "GCM Registration Token: " + token);
                    sendRegistrationToServer(token);
                } else {
                    //Delete tokens and Instance IDs
                    String authorizedEntity = getString(R.string.gcm_defaultSenderId);
                    String scope = "GCM";
                    InstanceID.getInstance(this).deleteToken(authorizedEntity, scope);
                }

                // TODO: Implement this method to send any registration to your app's servers.

                subscribeTopics(token);
                // [END register_for_gcm]
            }
        } catch (Exception e) {
            NewsAppUtil.logMessage(TAG, "Failed to complete token refresh " + e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            NewsAppUtil.getInstance(getApplicationContext()).getSecurePrefs().edit()
                    .putString(NewsAppConstants.PUSH_TOKEN_KEY, "").commit();
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(NewsAppConstants.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }


    /**
     * Persist registration to third-party servers.
     * <p/>
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {

        String DEVICE_ID = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        NewsAppUtil.logMessage("SENDRegToServer245", "http://www.tamilflashnews.com/api?module=addtoken&platform=android&udid=" + DEVICE_ID + "&device_token=" + token);
        NewsAppUtil.logMessage("SENDRegToServer245", "Device id: " + DEVICE_ID);
        String reqURL = "http://www.tamilflashnews.com/api?module=addtoken&version=" + NewsAppConstants.APP_VERSION + "&platform=android&udid=" + DEVICE_ID + "&device_token=" + token;
        VolleyRequester.getInstance(getApplicationContext())
                .sendVolleyJsonObjectRequest("registerIntent", reqURL, new VolleyJsonObjectResponder() {
                    @Override
                    public void onJsonObjectResponseSuccess(JSONObject response) {
                        try {
                            String result = response.getString("rstatus");
                            if (result.toLowerCase().equals("success")) {
                                // You server have received the token.
                                NewsAppUtil.getInstance(getApplicationContext()).getSecurePrefs().edit()
                                        .putString(NewsAppConstants.PUSH_TOKEN_KEY, token).commit();

                                if (registerGCM!=null){
                                    if(registerGCM.equals("Register GCM")){
                                        OnUpdateReceiver.registerGCM();
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            // You server have received the token.
                            NewsAppUtil.getInstance(getApplicationContext()).getSecurePrefs().edit()
                                    .remove(NewsAppConstants.PUSH_TOKEN_KEY).commit();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onJsonObjectResponseFailure(VolleyError error) {

                    }
                });



    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        for (String topic : TOPICS) {
            GcmPubSub pubSub = GcmPubSub.getInstance(this);
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
}
