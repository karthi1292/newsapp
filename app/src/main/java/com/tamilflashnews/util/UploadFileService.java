package com.tamilflashnews.util;

import com.tamilflashnews.home.UserCredentialFragment;
import com.tamilflashnews.model.UserContentData;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.PartMap;
import retrofit.mime.TypedFile;

/**
 * Created by ${zakirhussain} on 12/29/2015.
 */
public interface UploadFileService {


    @Multipart
    @POST("/api/index.php?module=add_user_content")
    void upload(@Part(UserContentData.USER_NAME) String userName,
                @Part(UserContentData.USER_MAIL) String userMail,
                @Part(UserContentData.USER_PHONE) String userPhone,
                @Part(UserContentData.ARTICLE_TITLE) String title,
                @Part(UserContentData.DESCRIPTION) String description,
                @PartMap Map<String,TypedFile> imageFile,
                @Part(UserContentData.MEDIA_TYPE) String mediaType,
                @Part(UserContentData.MEDIA_FILE) TypedFile mediaFile,
                @Part(UserContentData.GEO_TAG) String tag,
                @Part(UserContentData.DEVICE_ID) String deviceId,
                @Part(UserContentData.APP_VERSION) String version,
                @Part(UserContentData.PLATFORM) String platForm,
                @Part(UserContentData.YOUTUBE_URL) String youTubeUrl,
                Callback<UserCredentialFragment.Response> cb);
}
