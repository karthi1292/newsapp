package com.tamilflashnews.util.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tamilflashnews.R;
import com.tamilflashnews.home.UserContentActivity;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by zakirhussain on 12/22/2015.
 */
public class CameraPreviewAdapter extends PagerAdapter {

    private ArrayList<String> mCapturedImageUris;
    private UserContentActivity mContext;

    public CameraPreviewAdapter(
            UserContentActivity userContentActivity,
            ArrayList<String> capturedImageUris) {
        mContext = userContentActivity;
        mCapturedImageUris = capturedImageUris;
    }

    @Override
    public int getCount() {
        return mCapturedImageUris.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.user_content_image_view, container, false);

        ImageView cameraPreviewImageView = (ImageView) view.findViewById(R.id.userContentImageView);

        // bitmap factory
        BitmapFactory.Options options = new BitmapFactory.Options();

        // downsizing image as it throws OutOfMemory Exception for larger
        // images
        options.inSampleSize = 3;

       // Bitmap bitmap = BitmapFactory.decodeFile(mCapturedImageUris.get(position));

        final Bitmap bitmap = BitmapFactory.decodeFile(mCapturedImageUris.get(position),
                options);
        for (String uris: mCapturedImageUris) {
            Log.i("Test", uris + "\n");
        }
        //Uri uri = Uri.parse(mCapturedImageUris.get(position));
        String device_name = Build.MANUFACTURER;

        Log.i("Sam", "Samsung " + device_name);
        Bitmap afterRotation = null;

        if (device_name.equals("samsung")) {
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(mCapturedImageUris.get(position));
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            switch(orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    afterRotation = rotateImage(bitmap, 90);
                    cameraPreviewImageView.setImageBitmap(afterRotation);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    afterRotation = rotateImage(bitmap, 180);
                    cameraPreviewImageView.setImageBitmap(afterRotation);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    afterRotation = rotateImage(bitmap, 270);
                    cameraPreviewImageView.setImageBitmap(afterRotation);
                    break;
                default:
                    cameraPreviewImageView.setImageBitmap(bitmap);
            }

        } else  {
            cameraPreviewImageView.setImageBitmap(bitmap);
        }

        cameraPreviewImageView.setAdjustViewBounds(true);
        cameraPreviewImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

        container.addView(view);
        return view;
    }



    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        return retVal;
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void changeDataSet(String imgUri){
        mCapturedImageUris.add(imgUri);
        notifyDataSetChanged();
    }



}
