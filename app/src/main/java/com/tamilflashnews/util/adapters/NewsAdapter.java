package com.tamilflashnews.util.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.android.volley.VolleyError;
import com.tamilflashnews.home.OldArticleInterface;
import com.tamilflashnews.dbhelpers.Repo;
import com.tamilflashnews.dbhelpers.RepoArticles;
import com.tamilflashnews.home.HomeContentFragment;
import com.tamilflashnews.model.UpdatedArticleTable;
import com.tamilflashnews.model.Bookmark;
import com.tamilflashnews.model.UpdatedLikeTable;
import com.tamilflashnews.model.NewsAppConstants;
import com.tamilflashnews.model.VolleyJsonObjectResponder;
import com.tamilflashnews.util.NewsAppUtil;
import com.tamilflashnews.util.network.VolleyRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Pager adapter for Show news
 */
public class NewsAdapter extends FragmentStatePagerAdapter {

    String TAG = "NEWS ADAPTER";
    public Boolean toggle;
    public List<UpdatedArticleTable> updatedArticleTableList;
    public OldArticleInterface oldArticleInterface;
    public Context context;
    public List<String> selectedCategoryArrayList;
    public String language="Tamil";
    SharedPreferences sharedpreferences,sharedpreferencesLanguage;
    SharedPreferences.Editor sharedpreferencesEditor;

    //TextView category;
    public NewsAdapter(Context context, FragmentManager fragmentManager, Boolean toggle, Boolean all, String id,List<String> selectedCategoryArrayList) {
        super(fragmentManager);
        this.toggle = toggle;
        this.context = context;
        this.oldArticleInterface = (OldArticleInterface) context;
        this.selectedCategoryArrayList=selectedCategoryArrayList;
        NewsAppUtil.logMessage("adapter", "yes coming in");

//        updatedArticleTableList.clear();

        sharedpreferences = context.getSharedPreferences("CategoryClicked", Context.MODE_PRIVATE);
        String categorySelected = sharedpreferences.getString("NewsCategory", null);

        sharedpreferencesLanguage = context.getSharedPreferences("language", Context.MODE_PRIVATE);
        language= sharedpreferencesLanguage.getString("LanguageSelected","Tamil");

        if (all&& selectedCategoryArrayList==null){
            this.updatedArticleTableList = Repo.getInstance(context).repoArticles.getAllArticlesByDescenDateTime(language);
            if(updatedArticleTableList.size()==0){
               // this.updatedArticleTableList = Repo.getInstance(context).repoArticles.getAllArticlesByDescenDateTime("Tamil");

            }
        }
            //  this.articleList = Repo.getInstance(context).repoArticles.getAllArticles();
        else if (id == null && selectedCategoryArrayList==null) {

            if (categorySelected.equals("Bookmarks")) {
                this.updatedArticleTableList = bookmarksToArticles(Repo.getInstance(context).repoBookmarks.getAll(language));
            } else if (categorySelected.equals("LikedArticles")) {
                this.updatedArticleTableList = LikeToArticles(Repo.getInstance(context).repoLike.getAll(language));
            }
        }
        else if(selectedCategoryArrayList!=null){
            this.updatedArticleTableList= Repo.getInstance(context).repoArticles.getSelectedCategoryArticleByID(selectedCategoryArrayList,language);
        }
        else
            this.updatedArticleTableList = Repo.getInstance(context).repoArticles.getArticleByID(id,language);
    }

    public NewsAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public List<UpdatedArticleTable> bookmarksToArticles(List<Bookmark> bookmarkList) {

        List<UpdatedArticleTable> updatedArticleTableList = new ArrayList<>();

        for (int index = 0; index < bookmarkList.size(); ++index) {

            updatedArticleTableList.add(index, new UpdatedArticleTable().fromBookmark(bookmarkList.get(index)));
        }
        return updatedArticleTableList;
    }

    public List<UpdatedArticleTable> LikeToArticles(List<UpdatedLikeTable> updatedLikeTableList) {

        List<UpdatedArticleTable> updatedArticleTableList = new ArrayList<>();

       // updatedArticleTableList.clear();
        for (int index = 0; index < updatedLikeTableList.size(); ++index) {

            updatedArticleTableList.add(index, new UpdatedArticleTable().fromLike(updatedLikeTableList.get(index)));
        }
        return updatedArticleTableList;
    }


    @Override
    public int getCount() {
        return updatedArticleTableList.size();
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


    public Fragment getItem(int position) {
        Log.d(TAG, "Viewing Position" + position);
        Log.d(TAG, "ArticleDesc" + updatedArticleTableList.get(position).getArticleDesc());
        List<UpdatedArticleTable> insertedUpdatedArticleTableList;
        HomeContentFragment homeContentFragment = null;

        SharedPreferences sharedpreferences = context.getSharedPreferences("CategoryClicked", Context.MODE_PRIVATE);
        String categoryName = sharedpreferences.getString("NewsCategory", null);

        //  Integer lastId = articleList.get(position).getArticleId();
        //To get oldArticleId
        // updatedArticleTableList.size();
        //insertedUpdatedArticleTableList=Repo.getInstance(context).repoArticles.getAllArticlesByDateTime();
        if (position == updatedArticleTableList.size() - 1) {
            // HomeContentFragment.progressBarRefreshOldArticle.setVisibility();
            String categoryId = updatedArticleTableList.get(position).getCategoryId();
            Integer lastId = updatedArticleTableList.get(position).getArticleId();
            if (categoryName != null) {
                if (categoryName.equals("AllNews")) {
                    oldArticleInterface.getOldArticlesBasedOnAllNews(lastId);
                } else if (!categoryName.equals("Bookmarks") && (!categoryName.equals("LikedArticles"))&&(selectedCategoryArrayList==null)) {
                    oldArticleInterface.getOldArticlesBasedOnCategory(categoryId, lastId);
                }

                homeContentFragment = new HomeContentFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("toggle", toggle);
                bundle.putSerializable(UpdatedArticleTable.class.getName(), updatedArticleTableList.get(position));
                homeContentFragment.setArguments(bundle);
            }
        } else {
            try {
                homeContentFragment = new HomeContentFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("toggle", toggle);
                bundle.putSerializable(UpdatedArticleTable.class.getName(), updatedArticleTableList.get(position));
                homeContentFragment.setArguments(bundle);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        return homeContentFragment;
    }

    public void addAricle(UpdatedArticleTable article) {
        updatedArticleTableList.add(article);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }


    public void getOldArticles() {

        // Integer lastUpdatedArticleId = getLastUpdatedArticleId();

        String url = "http://www.tamilflashnews.com/api/?module=article&view=archive&lid=3400";

        NewsAppUtil.logMessage("check lastId", "fetching articles");
        VolleyRequester.getInstance(context)
                .sendVolleyJsonObjectRequest(NewsAppConstants.ALL_ARTICLES_REQUEST_TAG, url
                        , new VolleyJsonObjectResponder() {

                    @Override
                    public void onJsonObjectResponseSuccess(JSONObject response) {
                        try {
                            Log.d("Response", "response");
                            if (!response.isNull("a_list")) {

                                JSONArray newArticleArray = response.getJSONArray("a_list");
                                RepoArticles repoArticles = Repo.getInstance(context).repoArticles;
                                NewsAppUtil.logMessage("Articles table Count before articles insertion " + repoArticles.getCount());

                                for (int i = 0; i < newArticleArray.length(); ++i) {
                                    JSONObject categoryObj = newArticleArray.getJSONObject(i);
                                    String categoryID = categoryObj.getString("cid");
                                    UpdatedArticleTable article = new UpdatedArticleTable().fromJSON(newArticleArray.getJSONObject(i));
                                    article.setCategoryId(categoryID);

                                    Repo.getInstance(context).repoArticles.insert(article);
                                    updatedArticleTableList.add(article);
                                }
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onJsonObjectResponseFailure(VolleyError error) {
                        NewsAppUtil.logError("Cannot retrieve articles " + error);
                    }
                });

    }

    public void changeDataSet(List<UpdatedArticleTable> articleList) {
        this.updatedArticleTableList = articleList;
        notifyDataSetChanged();
    }
}