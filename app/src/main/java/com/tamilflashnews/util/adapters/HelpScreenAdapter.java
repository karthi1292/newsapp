package com.tamilflashnews.util.adapters;

/**
 * Created by ${zakirhussain} on 1/21/2016.
 */

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tamilflashnews.R;
import com.tamilflashnews.settings.HelpActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;




/**
 * Created by CIPL307 on 9/22/2015.
 */
public class HelpScreenAdapter extends PagerAdapter {


    private Context mContext;
    private HelpAdapterListener mAdapterListener;


    public interface HelpAdapterListener{
        void onClick();
    }

    public void setOnAdapterLister(HelpAdapterListener adapterLister) {
        mAdapterListener = adapterLister;
    }

    private LayoutInflater inflater;
    private Integer imgArray[] = {R.drawable.intro_3, R.drawable.multicategory_help};
    public HelpScreenAdapter(LayoutInflater inflater,
                             HelpActivity helpActivity){
        mContext = helpActivity;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return imgArray.length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View rootView = inflater.inflate(R.layout.item_help_screen_images,container,false);
        TextView tapToStart = (TextView) rootView.findViewById(R.id.helpScreenTapToStart);
        RelativeLayout relativeLayout = (RelativeLayout) rootView.findViewById(R.id.helpScreenRelativeLayout);
        if (getCount() - 1 == position) {
            tapToStart.setVisibility(View.VISIBLE);
            relativeLayout.setBackgroundColor(Color.BLACK);
        } else {
            tapToStart.setVisibility(View.GONE);
            relativeLayout.setBackgroundColor(Color.BLACK);
        }

        tapToStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mAdapterListener != null) {
                    mAdapterListener.onClick();
                }
            }
        });
        ImageView img = (ImageView) rootView.findViewById(R.id.help_screen_image);
        Bitmap bitmap = BitmapFactory.decodeResource(inflater.getContext().getResources(), imgArray[position]);
        img.setImageBitmap(bitmap);

        container.addView(rootView);
        return rootView;
    }
    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename + ".png")));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View)object);
    }
}

