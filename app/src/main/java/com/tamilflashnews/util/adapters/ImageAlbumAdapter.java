package com.tamilflashnews.util.adapters;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.tamilflashnews.R;
import com.tamilflashnews.home.ImageAlbumActivity;
import com.tamilflashnews.home.PageCountInterface;
import com.tamilflashnews.model.NewsAppConstants;

/**
 * Created by CIPL0233 on 11/6/2015.
 */
public class ImageAlbumAdapter extends PagerAdapter {

    private ImageAlbumActivity mContext;
    private String[] albumUrls;
    private PageCountInterface pageCountInterface;

    public ImageAlbumAdapter(ImageAlbumActivity imageAlbumActivity, String[] albumUrls) {

        this.albumUrls = albumUrls;
        this.mContext = imageAlbumActivity;
        this.pageCountInterface = (PageCountInterface) imageAlbumActivity;
    }

    @Override
    public int getCount() {
        pageCountInterface.pageCount();
        return albumUrls.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.image_album_view, container, false);

        ImageView imageViewAlbum = (ImageView) view.findViewById(R.id.imageview_album);


        SharedPreferences sharedpreferencesImagePath = mContext.getSharedPreferences("downloadedAllArticles", Context.MODE_PRIVATE);
        String baseImageUrl = sharedpreferencesImagePath.getString("bpath", null);
        String imageUrl = baseImageUrl + albumUrls[position];

       /* Picasso.with(mContext)
                .load(imageUrl).
                placeholder(R.drawable.news_app_logo_high_res)
                .into(imageViewAlbum);
*/
        Picasso.with(mContext).load(imageUrl)
                .into(imageViewAlbum);
        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View) object);
    }
}
