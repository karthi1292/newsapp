package com.tamilflashnews.util.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import com.tamilflashnews.R;

/**
 * Created by developgopi on 25/6/15.
 */
public class ViewClip extends View {

    public static Paint datePaint;


    private  int lineColor, bgColor;

    public ViewClip(Context context, AttributeSet attr){
        super(context, attr);
        TypedArray a = context.getTheme().obtainStyledAttributes(attr, R.styleable.ViewClipStyle, 0, 0);
        try {
            //get the text and colors specified using the names in attrs.xml

            lineColor = a.getInteger(R.styleable.ViewClipStyle_lineColor, 0);//0 is default

            bgColor = a.getInteger(R.styleable.ViewClipStyle_bgColor, 0);
        } finally {
            a.recycle();
        }
    }



    @SuppressLint("NewApi")
    public ViewClip(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
       /* TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ViewClipStyle, 0, 0);
        try {
            //get the text and colors specified using the names in attrs.xml
            lineColor = a.getInteger(R.styleable.ViewClipStyle_lineColor, 0);//0 is default
            bgColor = a.getInteger(R.styleable.ViewClipStyle_bgColor, 0);
        } finally {
            a.recycle();
        }*/
    }


    @Override
    protected void onDraw(Canvas canvas) {


        int width = getWidth();
        int height = getHeight();
        int xPath = width -(int) (width / 9.9);
        int yPathBig = (int) (height / 2.0);
        int yPathSmall = (int) (height / 2.5);

        /*
        * <path d="M0 0 L0 120   L250 120   L250 80  Z" fill="green"/>
          <path d="M0 0 L250 80 L240 100   Z" fill="blue"/>
        * */

        datePaint = new Paint();
        datePaint.setStyle(Paint.Style.FILL);

        datePaint.setColor(Color.WHITE);
        //datePaint.setColor(bgColor);

        Path datePath = new Path();
        datePath.lineTo(0, height); /*80*/
        datePath.lineTo(width, height);/*60*/
        datePath.lineTo(width, yPathSmall);
        datePath.close();

        canvas.drawPath(datePath, datePaint);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        //paint.setColor(lineColor);
        paint.setColor(Color.rgb(203,50,50));
        paint.setStrokeWidth(2);

        Path path = new Path();

        path.moveTo(0, 0);

        path.lineTo(xPath, yPathBig); /*80*/
        path.lineTo(width, yPathSmall);/*60*/

        path.close();
        canvas.drawPath(path, paint);
        super.onDraw(canvas);

    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        int desiredHeight= 600;
/*
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);*/
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

       /* //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }*/

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = 500;
        }

        //MUST CALL THIS
        setMeasuredDimension(widthMeasureSpec, height);
    }

    public void setbg (int i) {
        if(i==1)
       datePaint.setColor(Color.WHITE);
        else
            datePaint.setColor(Color.BLACK);

    }
}