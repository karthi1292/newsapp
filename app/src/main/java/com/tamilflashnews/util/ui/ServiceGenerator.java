package com.tamilflashnews.util.ui;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by ${zakirhussain} on 12/29/2015.
 */
public class ServiceGenerator {

    public static final String API_BASE_URL = "http://www.tamilflashnews.com";

    private static RestAdapter.Builder builder = new RestAdapter.Builder()
            .setEndpoint(API_BASE_URL);

    /*static UrlConnectionClient client = new UrlConnectionClient() {
        @Override
        protected HttpURLConnection openConnection(Request request) throws IOException {
            HttpURLConnection connection = super.openConnection(request);
            connection.setConnectTimeout(15 * 1000);
            connection.setReadTimeout(30 * 1000);
            connection.setInstanceFollowRedirects(false);
            return connection;
        }
    };*/

    public static <S> S createService(Class<S> serviceClass) {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(1, TimeUnit.MINUTES);
        okHttpClient.setConnectTimeout(1, TimeUnit.MINUTES);
        builder.setClient(new OkClient(okHttpClient));
        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }

    /*public class MyUrlConnectionClient extends UrlConnectionClient {

        protected HttpURLConnection openConnection(Request request) {
            HttpURLConnection connection = null;
            try {
                connection = super.openConnection(request);
            } catch (IOException e) {
                e.printStackTrace();
            }
            connection.setConnectTimeout(15 * 1000);
            connection.setReadTimeout(30 * 1000);
            return connection;
        }

    }*/
}
