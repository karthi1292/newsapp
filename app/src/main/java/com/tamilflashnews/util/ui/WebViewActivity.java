package com.tamilflashnews.util.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.tamilflashnews.R;

public class WebViewActivity extends AppCompatActivity {

	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	WebView webview;
	String url;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);

		Toolbar toolbar = (Toolbar) findViewById(R.id.title_bar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		url =getIntent().getStringExtra("url");

		webview=(WebView)findViewById(R.id.webView1);
		webview.setWebViewClient(new MyWebViewClient());
		webview.getSettings().setJavaScriptEnabled(true);
		//webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		openURL(url);
	}

	/** Opens the URL in a browser */
	private void openURL(String url) {
		webview.loadUrl(url);
		webview.requestFocus();
	}

}