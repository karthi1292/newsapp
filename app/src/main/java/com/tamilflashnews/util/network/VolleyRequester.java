package com.tamilflashnews.util.network;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import com.tamilflashnews.model.VolleyJsonArrayResponder;
import com.tamilflashnews.model.VolleyJsonObjectResponder;
import com.tamilflashnews.model.VolleyStringResponder;
import com.tamilflashnews.util.NewsAppUtil;

import java.util.HashMap;
import java.util.Map;

public class VolleyRequester {

    private static VolleyRequester mInstance;
    private RequestQueue mRequestQueue;
    private Context mContext;
    private final String TAG = VolleyRequester.class.getSimpleName();

    public VolleyRequester(Context context) {
        this.mContext = context;
    }

    public static synchronized VolleyRequester getInstance(Context context) {
        mInstance = (null == mInstance) ? new VolleyRequester(context) : mInstance;
        return mInstance;
    }

    /**
     * Volley Request Queue
     **/
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void sendVolleyStringGetRequest(String requestKey, String reqURL, final VolleyStringResponder volleyStringResponder) {
        // Request a string response from the provided URL.
        StringRequest stringGetRequest = new StringRequest(Request.Method.GET, reqURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        NewsAppUtil.logMessage(TAG, response);
                        volleyStringResponder.onStringResponseSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NewsAppUtil.logError(TAG, error.getMessage());
                volleyStringResponder.onStringResponseFailure(error);
            }
        });

        addToRequestQueue(stringGetRequest, requestKey);
    }


    public void sendVolleyJsonObjectRequest(String requestKey, String reqURL, final VolleyJsonObjectResponder volleyJsonObjectResponder) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, reqURL,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        NewsAppUtil.logMessage(TAG, response.toString());
                        volleyJsonObjectResponder.onJsonObjectResponseSuccess(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NewsAppUtil.logError(TAG, "Error: " + error.getMessage());
                volleyJsonObjectResponder.onJsonObjectResponseFailure(error);
            }
        });
        //To fix time out error
        int socketTimeout = 30000;//30 seconds - change to what you want
        int DEFAULT_MAX_RETRIES = 1;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);
        addToRequestQueue(jsonObjReq, requestKey);
    }

    public void sendVolleyJsonArrayRequest(String requestKey, String reqURL, final VolleyJsonArrayResponder volleyJsonArrayResponder) {

        JsonArrayRequest jsonArrayResponder = new JsonArrayRequest(Request.Method.GET, reqURL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        NewsAppUtil.logMessage(TAG, response.toString());
                        volleyJsonArrayResponder.onJsonArrayResponseSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NewsAppUtil.logError(TAG, "Error: " + error.getMessage());
                volleyJsonArrayResponder.onJsonArrayResponseFailure(error);
            }
        });
        addToRequestQueue(jsonArrayResponder, requestKey);
    }
}