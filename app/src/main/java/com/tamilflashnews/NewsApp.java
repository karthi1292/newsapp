package com.tamilflashnews;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.facebook.stetho.Stetho;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.tamilflashnews.dbhelpers.Repo;
import com.tamilflashnews.service.UpdatedArticlesSync;
import com.tamilflashnews.util.NewsAppUtil;


public class NewsApp extends Application {

    private static GoogleAnalytics analytics;
    private static Tracker tracker;
    private boolean UpdatedArticlesAPIAlarm =false;
    public static GoogleAnalytics analytics() {
        return analytics;
    }
    private SharedPreferences sharedPreferencesUpdateArticlesAlarm;
    public static Tracker tracker() {
        return tracker;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        analytics = GoogleAnalytics.getInstance(this);
        tracker = analytics.newTracker("UA-66437119-1");
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
        tracker.setAppVersion(BuildConfig.VERSION_NAME);

        sharedPreferencesUpdateArticlesAlarm = getSharedPreferences("UpdatedArticlesAlarm", MODE_PRIVATE);
        UpdatedArticlesAPIAlarm=sharedPreferencesUpdateArticlesAlarm.getBoolean("UpdatedArticlesAPIAlarm", false);


        if (!UpdatedArticlesAPIAlarm) {
            Intent intent = new Intent(this, UpdatedArticlesSync.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 234324243, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,1000, (60000), pendingIntent);
            //alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), (60000), pendingIntent);

            SharedPreferences.Editor editorUpdateArticles = sharedPreferencesUpdateArticlesAlarm.edit();
            editorUpdateArticles.putBoolean("UpdatedArticlesAPIAlarm", true);
            editorUpdateArticles.commit();
        }

       /* OnUpdateReceiver mNetworkChangeReceiver = new OnUpdateReceiver();
        IntentFilter networkPackageIntentFilter = new IntentFilter("android.intent.action.PACKAGE_REPLACED");
        registerReceiver(mNetworkChangeReceiver, networkPackageIntentFilter);*/

       Stetho.initializeWithDefaults(this);
        initDefaultConfigurations();
    }


    private void initDefaultConfigurations() {
        // Turn on only for debugging
        NewsAppUtil.logSwitch = true;
      /*  SharedPreferences prefs = getSharedPreferences("apprater", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("DIALOG_SELECTION","RATE");
        editor.commit();*/

        Repo.getInstance(this).buildDao();
    }
}
