package com.tamilflashnews.category;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;

import com.tamilflashnews.R;


public class CategoryWidget extends View {

    private int text_color, circle_color;
    private String text_string;
    private Paint widget_paint,widget_paint1,widget_paint2;
    private Button b;
    private Animation anim;
    private Canvas c;
    private int x, y;


    public GestureDetector.SimpleOnGestureListener mGestureListener;

    private void createAnimation(Canvas canvas, int centerX, int centerY, int radius) {
        //anim = new RotateAnimation(0, -360, centerX, centerY);
        anim = new TranslateAnimation(centerX + radius, centerX, centerY + radius, centerY);
        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(120L);
        //startAnimation(anim);
        //transformCircle(canvas,centerX,centerY);
    }

    public void transformCircle(Canvas canvas, int x, int y) {
        canvas.rotate(-180, x, y);
    }

    public CategoryWidget(Context context, AttributeSet attrs) {

        super(context, attrs);

        widget_paint = new Paint();
        widget_paint1 = new Paint();
        widget_paint2 = new Paint();
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CategoryWidget, 0, 0);

        try {
            //get the text and colors specified using the names in attrs.xml
            text_string = a.getString(R.styleable.CategoryWidget_text_string);
            circle_color = a.getInteger(R.styleable.CategoryWidget_circle_color, 0);//0 is default
            text_color = a.getInteger(R.styleable.CategoryWidget_text_color, 0);
        } finally {
            a.recycle();
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        // Starts the animation to rotate the circle.
        c = canvas;
        //get half of the width and height as we are working with a circle
        Point p = new Point();
        int viewWidthHalf;
        viewWidthHalf = p.x = this.getMeasuredWidth() - 10;
        int viewHeightHalf;
        viewHeightHalf = p.y = this.getMeasuredHeight() - 10;
        x = p.x;
        y = p.y;
        if (anim == null)
            createAnimation(canvas, p.x, p.y, 300);
        int width, height;
        float startAngle, sweepAngle;
        startAngle = 180f;
        sweepAngle = 90f;

        RectF oval;
        RectF green_oval;
        RectF grey_bg;


        //int radius = 0;
        if (viewWidthHalf > viewHeightHalf) {

            float t;
            t = p.x;
            p.x = p.y;
            p.y = (int) t;
            oval = new RectF(0, p.y - p.x, p.x * 2, p.y + p.x);
        }
        //radius=viewHeightHalf-10;
        else {
            width = height = viewWidthHalf - 10;
            oval = new RectF(0, p.y - p.x, p.x * 2, p.y + p.x);

            // radius=viewWidthHalf-10;
        }
        float green_bottom = oval.bottom / 2;
        float green_right = oval.right / 2;
        //green_oval=new RectF(oval.right/all,oval.top+(oval.right/all),oval.right,oval.bottom);
        float oval_center_x, oval_center_y, oval_width, oval_height;
        oval_width = oval.right - oval.left;
        oval_height = oval.bottom - oval.top;
        oval_center_x = oval_width / 2;
        oval_center_y = oval_height;

        green_oval = new RectF(oval.centerX() - (oval.right / 6), oval.centerY() - (oval.right / 6), oval.centerX() + (oval.right / 6), oval.centerY() + (oval.right / 6));
        grey_bg = new RectF((oval.centerX() - (oval.right / 6)) - 30, (oval.centerY() - (oval.right / 6)) - 30, (oval.centerX() + (oval.right / 6)) + 30, (oval.centerY() + (oval.right / 6)) + 30);
        widget_paint.setStyle(Paint.Style.FILL);
        widget_paint.setAntiAlias(true);
        widget_paint.setColor(circle_color);
        //canvas.drawCircle(viewWidthHalf, viewHeightHalf, radius, widget_paint);

        //Me
        RectF oval2=new RectF(0+11, (p.y - p.x)+11, (p.x * 2)-12, (p.y + p.x)-12);
        widget_paint1.setStyle(Paint.Style.STROKE);
        widget_paint1.setStrokeWidth(20);
        float[] hsv1 = new float[3];
        Color.RGBToHSV(204, 45, 45, hsv1);
        widget_paint1.setColor(Color.HSVToColor(hsv1));
        //widget_paint1.setColor(Color.RED);
        canvas.drawArc(oval2, startAngle, sweepAngle, false, widget_paint1);
        //Me

        RectF temp = new RectF((float) (oval.left - 3), (float) (oval.top - 3), (float) (oval.right + 3), (float) (oval.bottom + 3));

        RectF oval1=new RectF(0+19, (p.y - p.x)+18, (p.x * 2)-18, (p.y + p.x)-18);
        widget_paint.setColor(circle_color);
        canvas.drawArc(oval1, startAngle, sweepAngle, true, widget_paint);

        widget_paint.setColor(Color.GRAY);
        canvas.drawArc(grey_bg, startAngle, sweepAngle, true, widget_paint);


        float[] hsv = new float[3];
        Color.RGBToHSV(204, 45, 45, hsv);
        widget_paint.setColor(Color.HSVToColor(hsv));
        canvas.drawArc(green_oval, startAngle, sweepAngle, true, widget_paint);

        widget_paint.setColor(Color.GRAY);
        RectF grey_bg_small = new RectF(green_oval.centerX() - (green_oval.right / 8), green_oval.centerY() - (green_oval.right / 8),
                green_oval.centerX() + (green_oval.right / 8), green_oval.centerY() + (green_oval.right / 8));
        canvas.drawArc(grey_bg_small, startAngle, sweepAngle, true, widget_paint);


    }


}
