package com.tamilflashnews.category;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tamilflashnews.R;
import com.tamilflashnews.dbhelpers.Repo;
import com.tamilflashnews.home.HomeActivity;
import com.tamilflashnews.model.UpdatedArticleTable;
import com.tamilflashnews.model.Bookmark;
import com.tamilflashnews.util.NewsAppUtil;
import com.tamilflashnews.util.adapters.NewsAdapter;

import java.util.List;


public class MoreCategories extends DialogFragment {

    public View view;
    public ImageView image1, image2, image3, image4, image5, image6, image7, image8, image9;
    public TextView text1, text2, text3, text4, text5, text6, text7, text8, text9;
    public ImageView[] imageViewArray;
    public TextView[] textViewArray;
    public ViewPager vpPager;
    public NewsAdapter adapterViewPager;
    public Context context;
    public TextView categoryName;
    public ImageView bookmark_icon;


    public static MoreCategories newInstance() {
        MoreCategories f = new MoreCategories();
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);


    }
    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            d.getWindow().setLayout(width, height);
        }
    }
    public static int m;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));

        view = inflater.inflate(R.layout.more_categories_layout, container, false);


        text1 = (TextView) view.findViewById(R.id.text1);
        text2 = (TextView) view.findViewById(R.id.text2);
        text3 = (TextView) view.findViewById(R.id.text3);
        text4 = (TextView) view.findViewById(R.id.text4);
        text5 = (TextView) view.findViewById(R.id.text5);
        text6 = (TextView) view.findViewById(R.id.text6);
        text7 = (TextView) view.findViewById(R.id.text7);
        text8 = (TextView) view.findViewById(R.id.text8);
        text9 = (TextView) view.findViewById(R.id.text9);

        image1 = (ImageView) view.findViewById(R.id.image1);
        image2 = (ImageView) view.findViewById(R.id.image2);
        image3 = (ImageView) view.findViewById(R.id.image3);
        image4 = (ImageView) view.findViewById(R.id.image4);
        image5 = (ImageView) view.findViewById(R.id.image5);
        image6 = (ImageView) view.findViewById(R.id.image6);
        image7 = (ImageView) view.findViewById(R.id.image7);
        image8 = (ImageView) view.findViewById(R.id.image8);
        image9 = (ImageView) view.findViewById(R.id.image9);

        textViewArray = new TextView[]{
                text1, text2, text3, text4, text5, text6, text6, text7, text8, text9
        };

        imageViewArray = new ImageView[]{
                image1, image2, image3, image4, image5, image6, image7, image8, image9
        };


        for (m = 0; m < CategoryFragment.cidMoreArray.length; m++) {

            final String name = CategoryFragment.cnameMoreArray[m];
            textViewArray[m].setText(name);
            textViewArray[m].setTag(Integer.parseInt(CategoryFragment.cidMoreArray[m]));
            textViewArray[m].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);
                }
            });

            if (!CategoryFragment.ciconMoreArray[m].equals("0")) {
                imageViewArray[m].setImageResource(CategoryFragment.iconCollection[Integer.parseInt(CategoryFragment.ciconMoreArray[m]) - 1]);
            } else {
                imageViewArray[m].setImageResource(R.drawable.newspaper);
            }
            imageViewArray[m].setTag(Integer.parseInt(CategoryFragment.cidMoreArray[m]));
            imageViewArray[m].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);
                }
            });

            textViewArray[m].setVisibility(View.VISIBLE);
            imageViewArray[m].setVisibility(View.VISIBLE);

        }


        return view;
    }

    public void refreshPagerBasedOnCategory(int cid, String category_name) {


        vpPager = HomeActivity.vpPager;

        vpPager.removeAllViews();

        HomeActivity.present_category_id = String.valueOf(cid);


        if (HomeActivity.themeRes == null)
            HomeActivity.adapterViewPager = new NewsAdapter(context, getActivity().getSupportFragmentManager(), true, false, String.valueOf(cid),null);
        else
            HomeActivity.adapterViewPager = new NewsAdapter(context, getActivity().getSupportFragmentManager(), HomeActivity.themeRes, false, String.valueOf(cid),null);

        vpPager.setAdapter(HomeActivity.adapterViewPager);
        vpPager.getAdapter().notifyDataSetChanged();
        categoryName = (TextView) getActivity().findViewById(R.id.categoryName);
        categoryName.setText(category_name);
        Toast.makeText(context, category_name, Toast.LENGTH_SHORT).show();
        bookmark_icon = (ImageView) getActivity().findViewById(R.id.bookmark_icon);
        setBookmarkIcon(0);
        dismiss();

        HomeActivity.currentPosition = 0;
    }

    public Boolean checkBookmark(String cid, String aid) {

        List<Bookmark> bookmarkList = Repo.getInstance(context).repoBookmarks.getArticleBycIDandaID(cid, aid);
        if (bookmarkList == null || bookmarkList.size() == 0)
            return false;
        else
            return true;


    }

    public void setBookmarkIcon(int position) {

        if(HomeActivity.adapterViewPager.updatedArticleTableList != null && HomeActivity.adapterViewPager.updatedArticleTableList.size() != 0) {

            UpdatedArticleTable updatedArticleTable = HomeActivity.adapterViewPager.updatedArticleTableList.get(position);
            String article_id = updatedArticleTable.getArticleId()+"";
            if (checkBookmark(HomeActivity.present_category_id,article_id)) {
                NewsAppUtil.logMessage("sailesh", "check for on updatedArticleTable already in db, set on"+ HomeActivity.present_category_id + ": " +article_id);
                bookmark_icon.setImageResource(R.drawable.bookmark_icon_on_new);
            } else {
                bookmark_icon.setImageResource(R.drawable.bookmark_icon);
                NewsAppUtil.logMessage("sailesh", "check for on updatedArticleTable not in db, set off"+ HomeActivity.present_category_id + ": " +article_id);
            }
        }
    }


}


