package com.tamilflashnews.category;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.tamilflashnews.NewsApp;
import com.tamilflashnews.R;
import com.tamilflashnews.dbhelpers.Repo;
import com.tamilflashnews.home.HomeActivity;
import com.tamilflashnews.model.Bookmark;
import com.tamilflashnews.model.Category;
import com.tamilflashnews.model.UpdatedArticleTable;
import com.tamilflashnews.util.NewsAppUtil;
import com.tamilflashnews.util.adapters.NewsAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CategoryFragment extends DialogFragment {

    public static String[] cidMoreArray;
    public static String[] cnameMoreArray;
    public static String[] ciconMoreArray;
    public static int[] iconCollection, iconCollectionSelected;
    public static Typeface font;
    public static boolean categoryIconImageButtonSelected = false, categoryIconImageButton2Selected = false, categoryIconImageButton3Selected = false, categoryIconImageButton4Selected = false, categoryIconImageButton5Selected = false, categoryIconImageButton6Selected = false, categoryIconImageButton7Selected = false, categoryIconImageButton8Selected = false, categoryIconImageButton9Selected = false;
    static int l;
    public View view;
    public Context context;
    public String cidArray[];
    public String cnameArray[];
    public String cicon[];
    public List<Category> category_list;
    public Integer category_length;
    public ViewPager vpPager;
    public ImageView bookmark_icon;
    public Integer currentPage;
    public boolean categoryIconLongPressed = false;
    public boolean catgeoryIconSelected = false;
    ImageView[] imageArray = new ImageView[9];
    TextView[] textArray = new TextView[9];
    List<String> selectedCategoryArrayList, selectedList;
    ImageView b;
    RelativeLayout relativeLayout;
    TextView categoryName;
    ImageView category_image;
    int[] imageidarray;
    int[] textidarray;
    ViewPager viewPager;
    TextView textViewOK, textViewSeithiPirivugal;
    boolean multiCategoryHelp;
    private SharedPreferences sharedpreferencesMultiCategory;
    private SharedPreferences.Editor editorMultiCategory;
    private ImageView imageViewCategoryMenu, imageViewArrow;
    private TextView textViewMultiCategory;

    private String deviceId;

    public static CategoryFragment newInstance(Integer current_page) {
        CategoryFragment f = new CategoryFragment();
        Bundle args = new Bundle();
        args.putInt("currentPage", current_page);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle s) {
        super.onCreate(s);

        sharedpreferencesMultiCategory = getActivity().getSharedPreferences("MultiCategoryArticles", Context.MODE_PRIVATE);
        editorMultiCategory = sharedpreferencesMultiCategory.edit();
        multiCategoryHelp = sharedpreferencesMultiCategory.getBoolean("MultiCategoryHelp", false);

        categoryIconLongPressed = sharedpreferencesMultiCategory.getBoolean("CategoryIconLongPressed", false);
        String selectedCategorySerialized = sharedpreferencesMultiCategory.getString("MultiCategory", null);

        if (selectedCategorySerialized != null)
            selectedList = Arrays.asList(TextUtils.split(selectedCategorySerialized, ","));

        currentPage = getArguments().getInt("currentPage");

        category_list = Repo.getInstance(context).repoCategories.getAll();
        category_length = category_list.size();

        deviceId = Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        cidArray = new String[category_length];
        cnameArray = new String[category_length];
        cicon = new String[category_length];
        selectedCategoryArrayList = new ArrayList<>();
        if (category_list != null) {
            for (int i = 0; i < category_length; ++i) {
                cidArray[i] = category_list.get(i).getCategoryId();
                cnameArray[i] = category_list.get(i).getCategoryName();
                cicon[i] = category_list.get(i).getIcon();
            }
        }


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    @Override
    public void onStart() {
        super.onStart();
        NewsAppUtil.logMessage("Log", "On Start");
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);

        NewsAppUtil.logMessage("Infor", "OnActivityCreatedCalled");
    }

    public void category(String ccid)

    {
        Toast.makeText(getActivity().getApplicationContext(), "Clicked " + ccid, Toast.LENGTH_SHORT).show();
        NewsAppUtil.logMessage("CategoryClicked", "Selected Category" + ccid);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));

        view = inflater.inflate(R.layout.category_widget_fragment, container, false);


        iconCollection = new int[]
                {
                        R.drawable.agri, R.drawable.all, R.drawable.army, R.drawable.astrology, R.drawable.automobile,
                        R.drawable.blog, R.drawable.buisness, R.drawable.cinema, R.drawable.crime, R.drawable.editorspage,
                        R.drawable.enter, R.drawable.environment, R.drawable.finance, R.drawable.fun,
                        R.drawable.health, R.drawable.info, R.drawable.international, R.drawable.latest, R.drawable.miscellenous,
                        R.drawable.newspaper, R.drawable.politics, R.drawable.spiritual, R.drawable.sports, R.drawable.tech, R.drawable.travel
                };
        iconCollectionSelected = new int[]
                {
                        R.drawable.agri, R.drawable.all, R.drawable.army, R.drawable.astrology, R.drawable.automobile,
                        R.drawable.blog, R.drawable.buisness_tick, R.drawable.cinema_tick, R.drawable.crime, R.drawable.editorspage,
                        R.drawable.enter, R.drawable.environment, R.drawable.finance, R.drawable.fun,
                        R.drawable.health, R.drawable.info, R.drawable.international_tick, R.drawable.latest_fade, R.drawable.miscellenous_tick,
                        R.drawable.newspaper, R.drawable.politics_tick, R.drawable.spiritual_tick, R.drawable.sports_tick, R.drawable.tech_tick, R.drawable.travel
                };

        viewPager = (ViewPager) view.findViewById(R.id.FlashViewpager);

        initialize(view);
        // View myView = view.findViewById(R.id.imageview_category_button);
        imageViewCategoryMenu = (ImageView) view.findViewById(R.id.imageview_category_button);
        imageViewArrow = (ImageView) view.findViewById(R.id.imageview_arrow);
        textViewMultiCategory = (TextView) view.findViewById(R.id.textview_multicategory);
        RelativeLayout relative_root_layout = (RelativeLayout) view.findViewById(R.id.relative_root_layout);
        textViewOK = (TextView) view.findViewById(R.id.textview_ok);
        textViewSeithiPirivugal = (TextView) view.findViewById(R.id.textview_seithi_pirivugal);

        if (multiCategoryHelp) {
            imageViewArrow.setVisibility(view.GONE);
            textViewMultiCategory.setVisibility(view.GONE);

        } /*else {
            for (int i = 0; i < 9; i++) {
                View v = (ImageView) view.findViewById(imageidarray[i]);
                v.setClickable(false);
                View txtV = (TextView) view.findViewById(textidarray[i]);
                txtV.setClickable(false);
            }
        }*/
        relative_root_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewArrow.setVisibility(view.GONE);
                textViewMultiCategory.setVisibility(view.GONE);
                editorMultiCategory.putBoolean("MultiCategoryHelp", true);
                editorMultiCategory.commit();
            }
        });
        textViewOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  List<UpdatedArticleTable> updatedArticleTableList = Repo.getInstance(context).repoArticles.getSelectedCategoryArticleByID(selectedCategoryArrayList);
                refreshPagerBasedOnMultiCategory(selectedCategoryArrayList);
                StringBuilder gABuilder = null;
                StringBuilder gABuilderComma = null;
                if (selectedCategoryArrayList != null) {
                    gABuilder = new StringBuilder();
                    gABuilderComma = new StringBuilder();
                    for (String category : selectedCategoryArrayList) {
                        gABuilder.append(category);
                        gABuilderComma.append(category + "-");
                    }
                }
                Log.i("GA Analytics", "category " + gABuilder.toString());
                Log.i("GA Analytics", "category comma  " + gABuilderComma.toString());

                sendCategoryIdsToGA(gABuilder, gABuilderComma);
              /*//  Set<String> set = myScores.getStringSet("key", null);

                Set<String> set = new HashSet<String>();
                set.addAll(selectedCategoryArrayList);

                ObjectSerializer.serialize(selectedCategoryArrayList);*/

                editorMultiCategory.putString("MultiCategory", TextUtils.join(",", selectedCategoryArrayList).toString());
                editorMultiCategory.putBoolean("CategoryIconLongPressed", true);
                editorMultiCategory.commit();
                dismiss();
            }
        });
        imageViewCategoryMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                multiCategoryHelp = sharedpreferencesMultiCategory.getBoolean("MultiCategoryHelp", false);
                if (multiCategoryHelp)
                    dismiss();
            }
        });
      /*    myView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //At this point the layout is complete and the
                //dimensions of myView and any child views are known.
                //initialize(view);
                initialize(view);

                Point p = new Point();
                //Display d=getWindowManger().getDefaultDisplay();
                //int x=getWindow()
                p.x = (int) b.getX();
                p.y = (int) b.getY();

                int rad = (int) ((p.x / 2) + (p.x / 4) + (p.x / 10));

                //generate4Points(1, p.x - 14, p.y - 14, (int) (rad * .15f), 0);

                generate4Points(2, p.x - 14, p.y - 14, (int) (rad * .55f), 0);

                generate4Points(3, p.x - 14, p.y - 14, rad - 30, 4);
            }
        });
        CategoryWidget categoryWidget = (CategoryWidget) view.findViewById(R.id.custView);
        categoryWidget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

*/
        multiCategorySelected();
        return view;
    }

    private void sendCategoryIdsToGA(StringBuilder builder, StringBuilder builderComma) {
        Tracker t = ((NewsApp) getActivity().getApplicationContext()).tracker();
        t.send(new HitBuilders.EventBuilder()
                .setCategory("Android")
                .setAction("MultiCategory")
                .setLabel(builderComma.toString())
                .setValue(Integer.parseInt(builder.toString())).setCustomDimension(2, builderComma.toString())
                .build());
    }

    void initialize(View v) {

        //  relativeLayout = (RelativeLayout) v.findViewById(R.id.relativeLayout);
        // category_image = (ImageView) v.findViewById(R.id.categories_image);

        imageidarray = new int[]{R.id.imageButton, R.id.imageButton2, R.id.imageButton3, R.id.imageButton4,
                R.id.imageButton5, R.id.imageButton6, R.id.imageButton7, R.id.imageButton8,
                R.id.imageButton9};

        textidarray = new int[]{R.id.Settings, R.id.textView2, R.id.textView3, R.id.textView4,
                R.id.textView5, R.id.textView6, R.id.bookmarksText, R.id.textView8, R.id.textView9};

        // b = (ImageView) v.findViewById(R.id.button);

        font = Typeface.createFromAsset(context.getAssets(), "Roboto-Light.ttf");

        for (int i = 0; i < 9; i++) {
            imageArray[i] = (ImageView) v.findViewById(imageidarray[i]);
            textArray[i] = (TextView) v.findViewById(textidarray[i]);
            textArray[i].setTypeface(font);

            //  textArray.
           /* int density = getResources().getDisplayMetrics().densityDpi;
            switch (density) {
                case DisplayMetrics.DENSITY_LOW:

                    Toast.makeText(getActivity().getApplicationContext(), "LDPI", Toast.LENGTH_SHORT).show();
                    break;
                case DisplayMetrics.DENSITY_MEDIUM:
                    Toast.makeText(getActivity().getApplicationContext(), "MDPI", Toast.LENGTH_SHORT).show();
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    Toast.makeText(getActivity().getApplicationContext(), "HDPI", Toast.LENGTH_SHORT).show();
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    Toast.makeText(getActivity().getApplicationContext(), "XHDPI", Toast.LENGTH_SHORT).show();
                    textArray[i].setPadding(10, 0, 0, 0);
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    Toast.makeText(getActivity().getApplicationContext(), "XXHDPI", Toast.LENGTH_SHORT).show();
                    textArray[i].setPadding(27, 23, 0, 5);

                    break;
            }*/
        }

        if (cidArray.length <= 9) {

            for (l = 0; l < cidArray.length; l++) {

                final String name = cnameArray[l];

              /*  if (textidarray[l] == R.id.textView3) {
                    textArray[l].setText(cnameArray[3]);
                } else if (textidarray[l] == R.id.textView4) {
                    textArray[l].setText(cnameArray[2]);
                } else{
                    textArray[l].setText(name);
                }*/
               /* if (textidarray[l] == R.id.textView4) {
                    textArray[l].setText(cnameArray[5]);
                } else if (textidarray[l] == R.id.textView6) {
                    textArray[l].setText(cnameArray[3]);
                } else if (textidarray[l] == R.id.textView3) {
                    textArray[l].setText(cnameArray[8]);
                } else if (textidarray[l] == R.id.textView9) {
                    textArray[l].setText(cnameArray[2]);
                } else {
                    textArray[l].setText(name);
                }*/
                // textArray[l].setText(name);
               /* if (name.equalsIgnoreCase("International News") || name.equalsIgnoreCase("international ")) {
                    textArray[l].setText("   Pedia");
                } else {

                }*/
                //final String name = cnameArray[l];
                textArray[l].setText(name);
                textArray[l].setTag(Integer.parseInt(cidArray[l]));
                textArray[l].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        multiCategoryHelp = sharedpreferencesMultiCategory.getBoolean("MultiCategoryHelp", false);
                      /*  if (Integer.parseInt(v.getTag().toString()) == 4) {
                            refreshPagerBasedOnCategory(6, cnameArray[5]);
                        } else if (Integer.parseInt(v.getTag().toString()) == 6) {
                            refreshPagerBasedOnCategory(4, cnameArray[3]);
                        } else if (Integer.parseInt(v.getTag().toString()) == 9) {
                            refreshPagerBasedOnCategory(10, cnameArray[8]);
                        } else if (Integer.parseInt(v.getTag().toString()) == 10) {
                            refreshPagerBasedOnCategory(9, cnameArray[2]);
                        } else {
                            refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);
                        }*/
                        if (!categoryIconLongPressed && multiCategoryHelp)
                            refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);

                    }
                });
                //set image icon here
                if (!cicon[l].equals("0")) {
                    //   imageArray[l].setPadding(5, 5, 5, 5);
                   /*   if (imageidarray[l] == R.id.imageButton3) {
                        imageArray[l].setImageResource(iconCollection[16]);
                    } else if (imageidarray[l] == R.id.imageButton4) {
                        imageArray[l].setImageResource(iconCollection[7]);
                    } else {
                          imageArray[l].setImageResource(iconCollection[Integer.parseInt(cicon[l]) - 1]);
                      }*/
                   /* if (imageidarray[l] == R.id.imageButton4) {
                        imageArray[l].setImageResource(iconCollection[22]);
                    } else if (imageidarray[l] == R.id.imageButton6) {
                        imageArray[l].setImageResource(iconCollection[16]);
                    } else if (imageidarray[l] == R.id.imageButton3) {
                        imageArray[l].setImageResource(iconCollection[18]);
                    } else if (imageidarray[l] == R.id.imageButton9) {
                        imageArray[l].setImageResource(iconCollection[7]);
                    } else {
                        imageArray[l].setImageResource(iconCollection[Integer.parseInt(cicon[l]) - 1]);
                    }*/
                    imageArray[l].setImageResource(iconCollection[Integer.parseInt(cicon[l]) - 1]);
                    Log.d("cicon", " " + (Integer.parseInt(cicon[l]) - 1));
                    Log.d("ciconcalues", "" + cicon[l]);
                } else
                    imageArray[l].setImageResource(R.drawable.newspaper);
                imageArray[l].setTag(Integer.parseInt(cidArray[l]));
                imageArray[l].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewsAppUtil.logMessage("category clicked", "clicked clicked 1!!");

                        multiCategoryHelp = sharedpreferencesMultiCategory.getBoolean("MultiCategoryHelp", false);
                      /*  if (imageidarray[l] == R.id.imageButton4) {
                            refreshPagerBasedOnCategory(6, cnameArray[5]);
                        } else if (imageidarray[l] == R.id.imageButton6) {
                            refreshPagerBasedOnCategory(4,  cnameArray[3]);
                        } else if (imageidarray[l] == R.id.imageButton3) {
                            refreshPagerBasedOnCategory(10,  cnameArray[8]);
                        } else if (imageidarray[l] == R.id.imageButton9) {
                            refreshPagerBasedOnCategory(9,  cnameArray[2]);
                        } else {
                            refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);
                        }*/
                      /*  if (Integer.parseInt(v.getTag().toString()) == 4) {
                            refreshPagerBasedOnCategory(6, cnameArray[5]);
                        } else if (Integer.parseInt(v.getTag().toString()) == 6) {
                            refreshPagerBasedOnCategory(4, cnameArray[3]);
                        } else if (Integer.parseInt(v.getTag().toString()) == 9) {
                            refreshPagerBasedOnCategory(10, cnameArray[8]);
                        } else if (Integer.parseInt(v.getTag().toString()) == 10) {
                            refreshPagerBasedOnCategory(9, cnameArray[2]);
                        } else {
                            refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);
                        }*/

                       /* SparseArray<Integer> sparseArray=new SparseArray<Integer>();
                        sparseArray.pu*/
                        if (categoryIconLongPressed && multiCategoryHelp) {
                            switch (v.getId()) {
                                case R.id.imageButton:
                                    break;
                                case R.id.imageButton2:
                                    if (selectedCategoryArrayList.contains(cidArray[1])) {
                                        imageArray[1].setImageResource(iconCollection[Integer.parseInt(cicon[1]) - 1]);
                                        selectedCategoryArrayList.remove(selectedCategoryArrayList.indexOf(cidArray[1]));
                                    } else {
                                        imageArray[1].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[1]) - 1]);
                                        selectedCategoryArrayList.add(cidArray[1]);
                                    }
                                    break;
                                case R.id.imageButton3:
                                    if (selectedCategoryArrayList.contains(cidArray[2])) {
                                        imageArray[2].setImageResource(iconCollection[Integer.parseInt(cicon[2]) - 1]);
                                        selectedCategoryArrayList.remove(selectedCategoryArrayList.indexOf(cidArray[2]));
                                    } else {
                                        imageArray[2].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[2]) - 1]);
                                        selectedCategoryArrayList.add(cidArray[2]);
                                    }

                                    break;
                                case R.id.imageButton4:
                                    if (selectedCategoryArrayList.contains(cidArray[3])) {
                                        imageArray[3].setImageResource(iconCollection[Integer.parseInt(cicon[3]) - 1]);
                                        selectedCategoryArrayList.remove(selectedCategoryArrayList.indexOf(cidArray[3]));
                                    } else {
                                        imageArray[3].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[3]) - 1]);
                                        selectedCategoryArrayList.add(cidArray[3]);
                                        categoryIconImageButton4Selected = true;
                                    }
                                    break;
                                case R.id.imageButton5:
                                    if (selectedCategoryArrayList.contains(cidArray[4])) {
                                        imageArray[4].setImageResource(iconCollection[Integer.parseInt(cicon[4]) - 1]);
                                        selectedCategoryArrayList.remove(selectedCategoryArrayList.indexOf((cidArray[4])));

                                    } else {
                                        imageArray[4].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[4]) - 1]);
                                        selectedCategoryArrayList.add(cidArray[4]);
                                        categoryIconImageButton5Selected = true;
                                    }

                                    break;
                                case R.id.imageButton6:
                                    if (selectedCategoryArrayList.contains(cidArray[5])) {
                                        imageArray[5].setImageResource(iconCollection[Integer.parseInt(cicon[5]) - 1]);
                                        selectedCategoryArrayList.remove(selectedCategoryArrayList.indexOf(cidArray[5]));
                                    } else {
                                        imageArray[5].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[5]) - 1]);
                                        selectedCategoryArrayList.add(cidArray[5]);
                                        categoryIconImageButton6Selected = true;
                                    }
                                    break;
                                case R.id.imageButton7:
                                    if (selectedCategoryArrayList.contains(cidArray[6])) {
                                        imageArray[6].setImageResource(iconCollection[Integer.parseInt(cicon[6]) - 1]);
                                        selectedCategoryArrayList.remove(selectedCategoryArrayList.indexOf(cidArray[6]));
                                    } else {
                                        imageArray[6].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[6]) - 1]);
                                        selectedCategoryArrayList.add(cidArray[6]);
                                        categoryIconImageButton7Selected = true;
                                    }
                                    break;
                                case R.id.imageButton8:
                                    if (selectedCategoryArrayList.contains((cidArray[7]))) {
                                        imageArray[7].setImageResource(iconCollection[Integer.parseInt(cicon[7]) - 1]);
                                        selectedCategoryArrayList.remove(selectedCategoryArrayList.indexOf(cidArray[7]));
                                    } else {
                                        imageArray[7].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[7]) - 1]);
                                        selectedCategoryArrayList.add(cidArray[7]);
                                        categoryIconImageButton8Selected = true;
                                    }
                                    break;
                                case R.id.imageButton9:
                                    if (selectedCategoryArrayList.contains(cidArray[8])) {
                                        imageArray[8].setImageResource(iconCollection[Integer.parseInt(cicon[8]) - 1]);
                                        selectedCategoryArrayList.remove(selectedCategoryArrayList.indexOf(cidArray[8]));
                                    } else {
                                        imageArray[8].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[8]) - 1]);
                                        selectedCategoryArrayList.add(cidArray[8]);
                                        categoryIconImageButton9Selected = true;
                                    }
                                    break;
                            }
                            if (selectedCategoryArrayList.isEmpty()) {
                                imageArray[0].setImageResource(iconCollection[Integer.parseInt(cicon[0]) - 1]);
                                textArray[0].setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                                textViewSeithiPirivugal.setVisibility(View.VISIBLE);
                                textViewOK.setVisibility(View.GONE);
                                categoryIconLongPressed = false;
                                editorMultiCategory.putBoolean("CategoryIconLongPressed", false);
                                editorMultiCategory.putString("MultiCategory", null);
                                editorMultiCategory.commit();
                                selectedCategoryArrayList.clear();
                            }
                        } else if (multiCategoryHelp) {
                            refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);

                            Log.e("Value", "" + v.getTag().toString());
                        }
                    }
                });
                imageArray[l].setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (!categoryIconLongPressed) {
                            switch (v.getId()) {
                                case R.id.imageButton:
                                    break;
                                case R.id.imageButton2:
                                    textArray[0].setTextColor(ContextCompat.getColor(getActivity(), R.color.fade_gray));
                                    textViewSeithiPirivugal.setVisibility(View.GONE);
                                    textViewOK.setVisibility(View.VISIBLE);
                                    imageArray[0].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[0]) - 1]);
                                    imageArray[1].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[1]) - 1]);
                                    selectedCategoryArrayList.add(cidArray[1]);
                                    categoryIconLongPressed = true;
                                    break;
                                case R.id.imageButton3:
                                    textArray[0].setTextColor(ContextCompat.getColor(getActivity(), R.color.fade_gray));
                                    textViewSeithiPirivugal.setVisibility(View.GONE);
                                    textViewOK.setVisibility(View.VISIBLE);
                                    imageArray[0].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[0]) - 1]);
                                    imageArray[2].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[2]) - 1]);
                                    selectedCategoryArrayList.add(cidArray[2]);
                                    categoryIconLongPressed = true;
                                    break;
                                case R.id.imageButton4:
                                    textArray[0].setTextColor(ContextCompat.getColor(getActivity(), R.color.fade_gray));
                                    textViewSeithiPirivugal.setVisibility(View.GONE);
                                    textViewOK.setVisibility(View.VISIBLE);
                                    imageArray[0].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[0]) - 1]);
                                    imageArray[3].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[3]) - 1]);
                                    selectedCategoryArrayList.add(cidArray[3]);
                                    categoryIconLongPressed = true;
                                    break;
                                case R.id.imageButton5:
                                    textArray[0].setTextColor(ContextCompat.getColor(getActivity(), R.color.fade_gray));
                                    textViewSeithiPirivugal.setVisibility(View.GONE);
                                    textViewOK.setVisibility(View.VISIBLE);
                                    imageArray[0].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[0]) - 1]);
                                    imageArray[4].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[4]) - 1]);
                                    selectedCategoryArrayList.add(cidArray[4]);
                                    categoryIconLongPressed = true;
                                    break;
                                case R.id.imageButton6:
                                    textArray[0].setTextColor(ContextCompat.getColor(getActivity(), R.color.fade_gray));
                                    textViewSeithiPirivugal.setVisibility(View.GONE);
                                    textViewOK.setVisibility(View.VISIBLE);
                                    imageArray[0].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[0]) - 1]);
                                    imageArray[5].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[5]) - 1]);
                                    selectedCategoryArrayList.add(cidArray[5]);
                                    categoryIconLongPressed = true;
                                    break;
                                case R.id.imageButton7:
                                    textArray[0].setTextColor(ContextCompat.getColor(getActivity(), R.color.fade_gray));
                                    textViewSeithiPirivugal.setVisibility(View.GONE);
                                    textViewOK.setVisibility(View.VISIBLE);
                                    imageArray[0].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[0]) - 1]);
                                    imageArray[6].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[6]) - 1]);
                                    selectedCategoryArrayList.add(cidArray[6]);
                                    categoryIconLongPressed = true;
                                    break;
                                case R.id.imageButton8:
                                    textArray[0].setTextColor(ContextCompat.getColor(getActivity(), R.color.fade_gray));
                                    textViewSeithiPirivugal.setVisibility(View.GONE);
                                    textViewOK.setVisibility(View.VISIBLE);
                                    imageArray[0].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[0]) - 1]);
                                    imageArray[7].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[7]) - 1]);
                                    selectedCategoryArrayList.add(cidArray[7]);
                                    categoryIconLongPressed = true;
                                    break;
                                case R.id.imageButton9:
                                    textArray[0].setTextColor(ContextCompat.getColor(getActivity(), R.color.fade_gray));
                                    textViewSeithiPirivugal.setVisibility(View.GONE);
                                    textViewOK.setVisibility(View.VISIBLE);
                                    imageArray[0].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[0]) - 1]);
                                    imageArray[8].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[8]) - 1]);
                                    selectedCategoryArrayList.add(cidArray[8]);
                                    categoryIconLongPressed = true;
                                    break;
                            }
                        }
                        return true;
                    }
                });
            }
        } else {

            for (l = 0; l < 9; l++) {
                if (l != 4) {

                    final String name = cnameArray[l];
                    textArray[l].setText(name);
                    textArray[l].setTag(Integer.parseInt(cidArray[l]));
                    textArray[l].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {
                                NewsAppUtil.logMessage("category clicked", "clicked clicked 2!!");

/*
                                if (textidarray[l] == R.id.textView4) {
                                    refreshPagerBasedOnCategory(6, cnameArray[5]);
                                } else if (textidarray[l] == R.id.textView6) {
                                    refreshPagerBasedOnCategory(4, cnameArray[3]);
                                } else if (textidarray[l] == R.id.textView3) {
                                    refreshPagerBasedOnCategory(10, cnameArray[8]);
                                } else if (textidarray[l] == R.id.textView9) {
                                    refreshPagerBasedOnCategory(9, cnameArray[2]);
                                } else {
                                    refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);
                                }*/

                              /*  if (Integer.parseInt(v.getTag().toString()) == 4) {
                                    refreshPagerBasedOnCategory(6, cnameArray[5]);
                                } else if (Integer.parseInt(v.getTag().toString()) == 6) {
                                    refreshPagerBasedOnCategory(4, cnameArray[3]);
                                } else if (Integer.parseInt(v.getTag().toString()) == 9) {
                                    refreshPagerBasedOnCategory(10, cnameArray[8]);
                                } else if (Integer.parseInt(v.getTag().toString()) == 10) {
                                    refreshPagerBasedOnCategory(9, cnameArray[2]);
                                } else {
                                    refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);
                                }*/

                                refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);

                            } catch (Exception e) {
                                NewsAppUtil.logMessage("category clicked", "inside 0" + e.toString());
                            }
                        }
                    });


                    if (!cicon[l].equals("0")) {

                        imageArray[l].setImageResource(iconCollection[Integer.parseInt(cicon[l]) - 1]);
                    } else {
                        imageArray[l].setImageResource(R.drawable.newspaper);

                    }

                    imageArray[l].setTag(Integer.parseInt(cidArray[l]));
                    imageArray[l].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            NewsAppUtil.logMessage("category clicked", "clicked clicked 3!!");

                          /*  if (Integer.parseInt(v.getTag().toString()) == 4) {
                                refreshPagerBasedOnCategory(6, cnameArray[5]);
                            } else if (Integer.parseInt(v.getTag().toString()) == 6) {
                                refreshPagerBasedOnCategory(4, cnameArray[3]);
                            } else if (Integer.parseInt(v.getTag().toString()) == 9) {
                                refreshPagerBasedOnCategory(10, cnameArray[8]);
                            } else if (Integer.parseInt(v.getTag().toString()) == 10) {
                                refreshPagerBasedOnCategory(9, cnameArray[2]);
                            } else {
                                refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);
                            }*/
                            refreshPagerBasedOnCategory(Integer.parseInt(v.getTag().toString()), name);

                        }
                    });
                } else {
                    textArray[l].setText("More");
                    imageArray[l].setImageResource(R.drawable.miscellenous);
                    imageArray[l].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dismiss();
                            MoreCategories f = new MoreCategories();
                            f.show(getActivity().getSupportFragmentManager(), "More");

                        }
                    });
                    textArray[l].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dismiss();
                            MoreCategories f = new MoreCategories();
                            f.show(getActivity().getSupportFragmentManager(), "More");

                        }
                    });
                }
            }

            cidMoreArray = new String[cidArray.length - 8];
            cnameMoreArray = new String[cidArray.length - 8];
            ciconMoreArray = new String[cidArray.length - 8];
            int index;
            for (l = 9, index = 0; l < cidArray.length; l++, index++) {
                cidMoreArray[index] = cidArray[l];
                cnameMoreArray[index] = cnameArray[l];
                ciconMoreArray[index] = cicon[l];
            }
            cidMoreArray[index] = cidArray[4];
            cnameMoreArray[index] = cnameArray[4];
            ciconMoreArray[index] = cicon[4];
        }


       /* b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();

            }
        });*/

    }

    public void generate4Points(int no, int centerX, int centerY, int radius, int num) {
        //imageArray[0].setImageResource(R.drawable.);
        float deltaAngle = 90 / (no + 1);
        //int num=0;
        //int radius=centerX-50;
        float angle = 180;
        float tlocx = 20, tlocy = 0, ix = 0;
        float xfactor = 0, yfactor = 0;

        int density = getResources().getDisplayMetrics().densityDpi;
       /* if ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE) {

            Toast.makeText(getActivity().getApplicationContext(), "7 Inch Tab", Toast.LENGTH_SHORT).show();
        }*/

        switch (density) {
            case DisplayMetrics.DENSITY_LOW:
                //xfactor=
                ix = 5;
                tlocy = 40;
                xfactor = 15;
                yfactor = 10;

                // Toast.makeText(getActivity().getApplicationContext(), "LDPI", Toast.LENGTH_SHORT).show();
                break;
            case DisplayMetrics.DENSITY_MEDIUM:

                if ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_LARGE) {

                    xfactor = 30;
                    tlocx = -5;
                    yfactor = 35;
                    //radius+=5;
                    ix = 5;
                    tlocy = 75;
                    //    Toast.makeText(getActivity().getApplicationContext(), "7 inch Tab & MDPI", Toast.LENGTH_SHORT).show();
                } else if ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_XLARGE) {
                    xfactor = 20;
                    tlocx = -8;
                    yfactor = 55;
                    //radius+=5;
                    ix = 5;
                    tlocy = 100;
                    //    Toast.makeText(getActivity().getApplicationContext(), "10 inch Tab MDPI", Toast.LENGTH_SHORT).show();
                }
                break;
            case DisplayMetrics.DENSITY_HIGH:
                tlocx = -5;
                tlocy = 70;
                ix = 5;
                tlocx = -5;
                xfactor = 25;
                yfactor = 40;
                //   Toast.makeText(getActivity().getApplicationContext(), "HDPI", Toast.LENGTH_SHORT).show();
                break;
            case DisplayMetrics.DENSITY_XHIGH:

                if ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_LARGE) {
                    tlocx = -5;
                    tlocy = 155;
                    xfactor = 35;
                    yfactor = 90;
                    //    Toast.makeText(getActivity().getApplicationContext(), "7 inch & XHDPI", Toast.LENGTH_SHORT).show();
                } else {

                    tlocx = -5;
                    tlocy = 95;
                    xfactor = 35;
                    yfactor = 60;
                    //    Toast.makeText(getActivity().getApplicationContext(), "XHDPI", Toast.LENGTH_SHORT).show();
                }
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                tlocx = -5;
                tlocy = 140;
                xfactor = 40;
                yfactor = 95;
                //  Toast.makeText(getActivity().getApplicationContext(), "XXHDPI", Toast.LENGTH_SHORT).show();

                break;
            case DisplayMetrics.DENSITY_XXXHIGH:

                tlocx = -5;
                tlocy = 190;
                xfactor = 45;
                yfactor = 125;

                break;
        }
       /* xfactor=35;
        yfactor=50;*/
        while (angle <= 270 && num < 9) {
            Point p = new Point();
            p.x = (int) (centerX + Math.cos(angle * Math.PI / 180F) * radius);
            p.y = (int) (centerY + Math.sin(angle * Math.PI / 180F) * radius);

            relativeLayout.invalidate();
            relativeLayout.requestLayout();
            imageArray[num].setX(p.x - 10 + ix);
            imageArray[num].setY(p.y - yfactor);
            textArray[num].setX(p.x + 10 - xfactor - tlocx);/* + 10 - xfactor - tlocx*/
            textArray[num].setY(p.y - yfactor + tlocy);
            num++;
            angle += deltaAngle;
        }
    }

    public void refreshPagerBasedOnCategory(int cid, String category_name) {

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("CategoryClicked", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("NewsCategory", category_name);
        editor.commit();

        NewsAppUtil.logMessage("category clicked", "cid is: " + cid + "");
        NewsAppUtil.logMessage("category clicked", "category name is: " + category_name);
        vpPager = HomeActivity.vpPager;


        HomeActivity.present_category_id = String.valueOf(cid);


        boolean getAllNews = (cid == 1000 ? true : false);

        if (HomeActivity.themeRes == null)
            HomeActivity.adapterViewPager = new NewsAdapter(context, getActivity().getSupportFragmentManager(), true, getAllNews, String.valueOf(cid), selectedList);
        else
            HomeActivity.adapterViewPager = new NewsAdapter(context, getActivity().getSupportFragmentManager(), HomeActivity.themeRes, getAllNews, String.valueOf(cid), selectedList);

        if (HomeActivity.adapterViewPager.updatedArticleTableList.size() == 0) {
            // Intent intent = new Intent(getActivity(), HomeContentFragment.class);
            //  startActivity(intent);
            // finish();

            Toast.makeText(context, "There are no articled to display in this category", Toast.LENGTH_SHORT).show();
            return;
        }

        vpPager.removeAllViews();

        HomeActivity.currentPosition = 0;

        vpPager.setAdapter(HomeActivity.adapterViewPager);
        vpPager.getAdapter().notifyDataSetChanged();
        categoryName = (TextView) getActivity().findViewById(R.id.categoryName);
        categoryName.setText(category_name);
        Toast.makeText(context, category_name, Toast.LENGTH_SHORT).show();
        bookmark_icon = (ImageView) getActivity().findViewById(R.id.bookmark_icon);
        setBookmarkIcon(0);
        dismiss();
    }

    public void refreshPagerBasedOnMultiCategory(List<String> selectedCategoryArrayList) {


        vpPager = HomeActivity.vpPager;

        //   HomeActivity.present_category_id = String.valueOf(cid);

        boolean getAllNews = false;

        if (HomeActivity.themeRes == null)
            HomeActivity.adapterViewPager = new NewsAdapter(context, getActivity().getSupportFragmentManager(), true, getAllNews, null, selectedCategoryArrayList);
        else
            HomeActivity.adapterViewPager = new NewsAdapter(context, getActivity().getSupportFragmentManager(), HomeActivity.themeRes, getAllNews, null, selectedCategoryArrayList);

        if (HomeActivity.adapterViewPager.updatedArticleTableList.size() == 0) {
            // Intent intent = new Intent(getActivity(), HomeContentFragment.class);
            //  startActivity(intent);
            // finish();

            Toast.makeText(context, "There are no articled to display in this category", Toast.LENGTH_SHORT).show();
            return;
        }

        vpPager.removeAllViews();

        HomeActivity.currentPosition = 0;

        vpPager.setAdapter(HomeActivity.adapterViewPager);
        vpPager.getAdapter().notifyDataSetChanged();
        categoryName = (TextView) getActivity().findViewById(R.id.categoryName);
        categoryName.setText("My Genre");
        Toast.makeText(context, "My Genre", Toast.LENGTH_SHORT).show();
        bookmark_icon = (ImageView) getActivity().findViewById(R.id.bookmark_icon);
        setBookmarkIcon(0);
        dismiss();
    }


    public Boolean checkBookmark(String cid, String aid) {

        List<Bookmark> bookmarkList = Repo.getInstance(context).repoBookmarks.getArticleBycIDandaID(cid, aid);
        if (bookmarkList == null || bookmarkList.size() == 0)
            return false;
        else
            return true;


    }

    public void setBookmarkIcon(int position) {

        if (HomeActivity.adapterViewPager.updatedArticleTableList != null && HomeActivity.adapterViewPager.updatedArticleTableList.size() != 0) {

            UpdatedArticleTable updatedArticleTable = HomeActivity.adapterViewPager.updatedArticleTableList.get(position);
            String article_id = updatedArticleTable.getArticleId() + "";
            if (checkBookmark(HomeActivity.present_category_id, article_id)) {
                NewsAppUtil.logMessage("sailesh", "check for on updatedArticleTable already in db, set on" + HomeActivity.present_category_id + ": " + article_id);
                bookmark_icon.setImageResource(R.drawable.bookmark_icon_on_new);
            } else {
                bookmark_icon.setImageResource(R.drawable.bookmark_icon);
                NewsAppUtil.logMessage("sailesh", "check for on updatedArticleTable not in db, set off" + HomeActivity.present_category_id + ": " + article_id);
            }
        }
    }

    private void multiCategorySelected() {
        if (categoryIconLongPressed) {
            textViewOK.setVisibility(View.VISIBLE);
            textViewSeithiPirivugal.setVisibility(View.GONE);
            textArray[0].setTextColor(ContextCompat.getColor(getActivity(), R.color.fade_gray));
            imageArray[0].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[0]) - 1]);
            if (selectedList.contains(cidArray[1])) {
                selectedCategoryArrayList.add(cidArray[1]);
                imageArray[1].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[1]) - 1]);
            }
            if (selectedList.contains(cidArray[2])) {
                selectedCategoryArrayList.add(cidArray[2]);
                imageArray[2].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[2]) - 1]);
            }
            if (selectedList.contains(cidArray[3])) {
                selectedCategoryArrayList.add(cidArray[3]);
                imageArray[3].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[3]) - 1]);
            }
            if (selectedList.contains(cidArray[4])) {
                selectedCategoryArrayList.add(cidArray[4]);
                imageArray[4].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[4]) - 1]);
            }
            if (selectedList.contains(cidArray[5])) {
                selectedCategoryArrayList.add(cidArray[5]);
                imageArray[5].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[5]) - 1]);
            }
            if (selectedList.contains(cidArray[6])) {
                selectedCategoryArrayList.add(cidArray[6]);
                imageArray[6].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[6]) - 1]);
            }
            if (selectedList.contains(cidArray[7])) {
                selectedCategoryArrayList.add(cidArray[7]);
                imageArray[7].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[7]) - 1]);
            }
            if (selectedList.contains(cidArray[8])) {
                selectedCategoryArrayList.add(cidArray[8]);
                imageArray[8].setImageResource(iconCollectionSelected[Integer.parseInt(cicon[8]) - 1]);
            }
        }
    }
}
