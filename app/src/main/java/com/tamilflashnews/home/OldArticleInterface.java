package com.tamilflashnews.home;

/**
 * Created by CIPL0233 on 10/7/2015.
 */
public interface OldArticleInterface {
    public void getOldArticlesBasedOnCategory(String categoryId, Integer articleId);
    public void getOldArticlesBasedOnAllNews(Integer articleId);
}
