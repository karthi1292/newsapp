package com.tamilflashnews.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import com.tamilflashnews.R;

import java.io.File;

public class VideoPreviewActivity extends AppCompatActivity {

    private VideoView videoPreview;
    private MediaController mediaControls;

    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_preview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String videoPath = intent.getStringExtra(UserContentActivity.VIDEO_URI);

        previewVideo(videoPath);

    }

    private void previewVideo(String videoPath) {
        videoPreview = (VideoView) findViewById(R.id.videoPreview);

        videoPreview.setVisibility(View.VISIBLE);

       /* if (mediaControls == null) {
            mediaControls = new MediaController(this);
        }

        try {
            videoPreview.setMediaController(mediaControls);
            //videoPreview.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/raw/" + videoFile));
            videoPreview.setVideoPath(videoPath);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoPreview.requestFocus();
        videoPreview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                videoPreview.seekTo(position);
                if (position == 0) {
                    videoPreview.start();
                } else {
                    videoPreview.pause();
                }
            }
        });*/

        videoPreview.setVideoURI(Uri.fromFile(new File(videoPath)));
        videoPreview.setMediaController(new MediaController(this));
        videoPreview.requestFocus();
        videoPreview.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
