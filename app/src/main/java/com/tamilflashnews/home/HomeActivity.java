package com.tamilflashnews.home;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.comscore.analytics.comScore;
import com.comscore.instrumentation.InstrumentedFragmentActivity;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.tamilflashnews.NewsApp;
import com.tamilflashnews.R;
import com.tamilflashnews.dbhelpers.Repo;
import com.tamilflashnews.dbhelpers.RepoArticles;
import com.tamilflashnews.model.Bookmark;
import com.tamilflashnews.model.NewsAppConstants;
import com.tamilflashnews.model.UpdatedArticleTable;
import com.tamilflashnews.model.UpdatedLikeTable;
import com.tamilflashnews.model.VolleyJsonObjectResponder;
import com.tamilflashnews.rate.AppRater;
import com.tamilflashnews.service.UpdatedArticlesSync;
import com.tamilflashnews.settings.SettingsFragment;
import com.tamilflashnews.util.NewsAppUtil;
import com.tamilflashnews.util.adapters.NewsAdapter;
import com.tamilflashnews.util.network.VolleyRequester;
import com.tamilflashnews.util.ui.transformers.DepthPageTransformer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class HomeActivity extends InstrumentedFragmentActivity implements OldArticleInterface, ViewPager.OnPageChangeListener,LanguageInterface {

    public static NewsAdapter adapterViewPager;
    public static ViewPager vpPager;
    public static Boolean themeRes;
    public static String present_category_id;
    public static int present_article_id;
    public static int currentPosition = 0;
    public int currentPage;
    public ImageView categoryImage;
    public ImageView bookmark_icon, mWriteContent;
    public SharedPreferences mode;
    public TextView categoryName;
    public String category_name;
    public int currentPos;
    public ImageView more_icon;
    public Boolean toggle;
    public String android_id;
    public HomeContentFragment homeFragment;
    public TextView newStories;
    public TextView storiesCount;
    public ScrollView pagerScroller;
    public FrameLayout mainFrameLayout;
    List<String> selectedList;
    String language = "Tamil",lastSelectedLanguage;
    private Tracker tracker;
    private Animation scaleAnimation, rotationAnimation;
    private String DEVICE_ID;
    private boolean UpdatedArticlesAPIAlarm;
    private SharedPreferences sharedPreferencesUpdateArticlesAlarm, sharedpreferencesMultiCategory,sharedpreferencesLanguage;
    private boolean articlePresent;
    private SharedPreferences.Editor editorMultiCategory,sharedpreferencesEditor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // AppRater.app_launched(this);

        sharedpreferencesMultiCategory = getSharedPreferences("MultiCategoryArticles", Context.MODE_PRIVATE);
        editorMultiCategory = sharedpreferencesMultiCategory.edit();
        String selectedCategorySerialized = sharedpreferencesMultiCategory.getString("MultiCategory", null);

        sharedpreferencesLanguage = getSharedPreferences("language", Context.MODE_PRIVATE);
        lastSelectedLanguage= sharedpreferencesLanguage.getString("LanguageSelected", "Tamil");


        if (selectedCategorySerialized != null)
            selectedList = Arrays.asList(TextUtils.split(selectedCategorySerialized, ","));

        DEVICE_ID = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        // Initialize comScore Application Tag library
        comScore.setAppContext(this.getApplicationContext());
        comScore.setCustomerC2("20455672");
        comScore.setPublisherSecret("5795bbedd783374eab662d5da0bd8059");

        currentPos = 0;

        android_id = Secure.getString(this.getContentResolver(),
                Secure.ANDROID_ID);


        //Remove title bar
        setContentView(R.layout.home_activity_container);


        comScore.onUxActive();
        comScore.enableAutoUpdate(120, true);
        comScore.getVersion();
        comScore.onUserInteraction();

        mode = getSharedPreferences("date", MODE_PRIVATE);
        themeRes = !NewsAppUtil.getInstance(this).getSecurePrefs().getBoolean("toggle", false);

        rotationAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate);
        scaleAnimation = AnimationUtils.loadAnimation(this, R.anim.zoom);

        mWriteContent = (ImageView) findViewById(R.id.writeContent);
        mWriteContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent content = new Intent(HomeActivity.this, UserContentActivity.class);
                startActivity(content);
            }
        });

        mainFrameLayout = (FrameLayout) findViewById(R.id.main_frame_layout);
        storiesCount = (TextView) findViewById(R.id.newStoriesId);
        storiesCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((NewsAdapter) vpPager.getAdapter()).updatedArticleTableList = Repo.getInstance(getApplicationContext()).repoArticles.getAllArticlesByDescenDateTime(language);
                //((NewsAdapter) vpPager.getAdapter()).updatedArticleTableList = Repo.getInstance(getApplicationContext()).repoArticles.getAllArticles();
                ((NewsAdapter) vpPager.getAdapter()).notifyDataSetChanged();
                vpPager.setAdapter(adapterViewPager);

                present_category_id = null;
                currentPos = 0;
                categoryName = (TextView) findViewById(R.id.categoryName);
                categoryName.setText("All News");
                NewsAppUtil.logMessage("check", "stories count clicked");
                storiesCount.setVisibility(View.INVISIBLE);

                SharedPreferences sharedpreferences = getSharedPreferences("updatedArticles", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("updatedCount", "0");
                editor.commit();
            }
        });

        SharedPreferences sharedpreferences = getSharedPreferences("updatedArticles", MODE_PRIVATE);
        String updatedCount = sharedpreferences.getString("updatedCount", "0");
        NewsAppUtil.logMessage("onscroll", "data is: " + updatedCount);
        if (!updatedCount.equals("0")) {
            storiesCount.setVisibility(View.VISIBLE);
            storiesCount.setText("+" + updatedCount);
        }
        setHomeViews();
        Intent intent = getIntent();
        final String articleId = intent.getStringExtra("articleId");

        Intent shareIntent = getIntent();
        String shareArticleId = intent.getStringExtra("shareArticleId");
        boolean isShareImageCliked = shareIntent.getBooleanExtra("shareClick", false);
        String shareArticleTitle = shareIntent.getStringExtra("shareArticleTitle");

        NewsAppUtil.logMessage("push message Homeactivity", "parsed updatedArticleTable id is before: " + shareArticleId + articleId);


        // Share intent content
        /*Intent shareIntent = getIntent();
        String shareArticleId = shareIntent.getStringExtra("shareArticleId");
        boolean isShareImageCliked = shareIntent.getBooleanExtra("shareClick", false);*/

        if (isShareImageCliked) {
            //Toast.makeText(this, "Success and UpdatedArticleTable id = " + shareArticleId, Toast.LENGTH_LONG).show();
            if (shareArticleId != null) {
                int position = findArticlePosition(shareArticleId);
                NewsAppUtil.logMessage("push message", "parsed updatedArticleTable id is before: " + shareArticleId + findArticlePosition(shareArticleId));
                vpPager.setCurrentItem(position);
                vpPager.getAdapter().notifyDataSetChanged();

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareArticleTitle + "\n" + "Get the app at " + NewsAppConstants.APP_LINK);
                startActivity(sharingIntent);
            }
        } else {
            //Toast.makeText(this, "Failure and UpdatedArticleTable id = " + articleId, Toast.LENGTH_LONG).show();
            if (articleId != null) {
                NewsAppUtil.logMessage("push message", "parsed updatedArticleTable id is before: " + articleId
                        + findArticlePosition(articleId));
                int position = findArticlePosition(articleId);

                if (articlePresent) {
                    vpPager.setCurrentItem(position);
                    vpPager.getAdapter().notifyDataSetChanged();
                } else {
                    Toast.makeText(getApplicationContext(), "Article is not available", Toast.LENGTH_LONG).show();
                }
            }
        }
        comScore.onUxInactive();
    }

  /* @Override
    protected void onResume()
    {
        super.onResume();
        NewsAppUtil.logMessage("checkback", "resume called");
        //Restore state here
    }*/

    @Override
    protected void onPause() {
        super.onPause();

        AppEventsLogger.deactivateApp(this);
        comScore.onExitForeground();

        sharedPreferencesUpdateArticlesAlarm = getSharedPreferences("UpdatedArticlesAlarm", MODE_PRIVATE);
        UpdatedArticlesAPIAlarm = sharedPreferencesUpdateArticlesAlarm.getBoolean("UpdatedArticlesAPIAlarm", false);
        if (UpdatedArticlesAPIAlarm) {
            Intent cancelUpdateArticleIntent = new Intent(this, UpdatedArticlesSync.class);
            PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 234324243, cancelUpdateArticleIntent, 0);
            AlarmManager cancelAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            cancelAlarmManager.cancel(cancelPendingIntent);
            SharedPreferences.Editor editorUpdateArticles = sharedPreferencesUpdateArticlesAlarm.edit();
            editorUpdateArticles.putBoolean("UpdatedArticlesAPIAlarm", false);
            editorUpdateArticles.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        NewsAppUtil.logMessage("checkback", "resume called");
        AppEventsLogger.activateApp(this);
        comScore.onEnterForeground();

        sharedPreferencesUpdateArticlesAlarm = getSharedPreferences("UpdatedArticlesAlarm", MODE_PRIVATE);
        UpdatedArticlesAPIAlarm = sharedPreferencesUpdateArticlesAlarm.getBoolean("UpdatedArticlesAPIAlarm", false);
        if (!UpdatedArticlesAPIAlarm) {
            Intent intent = new Intent(this, UpdatedArticlesSync.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 234324243, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, (60000), pendingIntent);
            //  alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), (60000), pendingIntent);
            SharedPreferences.Editor editorUpdateArticles = sharedPreferencesUpdateArticlesAlarm.edit();
            editorUpdateArticles.putBoolean("UpdatedArticlesAPIAlarm", true);
            editorUpdateArticles.commit();
        }
    }

    private int findArticlePosition(String rcvdArticleId) {

        List<UpdatedArticleTable> updatedArticleTableList;
        int foundPosition = 0;
        updatedArticleTableList = adapterViewPager.updatedArticleTableList;
        int articleId = 0;

        NewsAppUtil.logMessage("push message", "parsed received id is: " + rcvdArticleId + "art" + articleId);
        for (int i = 0; i < updatedArticleTableList.size(); i++) {

            articleId = adapterViewPager.updatedArticleTableList.get(i).getArticleId();
            NewsAppUtil.logMessage("push message", "parsed updatedArticleTable id is0: " + articleId);
            NewsAppUtil.logMessage("push message", "parsed received id is1: " + rcvdArticleId);
            if (rcvdArticleId.equals(articleId + "")) {
                NewsAppUtil.logMessage("push message", "parsed updatedArticleTable id is after20: " + articleId);
                NewsAppUtil.logMessage("push message", "parsed received id is2: " + rcvdArticleId);
                foundPosition = i;
                articlePresent = true;
            }
        }
        currentPos = foundPosition;
        NewsAppUtil.logMessage("push message", "current position is23: " + currentPos);
        return currentPos;
    }

    private void setHomeViews() {

        categoryName = (TextView) findViewById(R.id.categoryName);
        NewsAppUtil.logMessage("checkback", "getting category id");
        NewsAppUtil.logMessage("checkback", "getting category id" + present_category_id);

        if (null != present_category_id && !present_category_id.equals("1000")) {
            NewsAppUtil.logMessage("checkback", "coming in");
            category_name = Repo.getInstance(this).repoCategories.getCategoryByID(present_category_id);
        } else {
            category_name = "All News";
        }

        if (null != present_category_id && present_category_id.equals("1000")) {
            present_category_id = null;
        }

        if (selectedList != null) {
            categoryName.setText("My Genre");
        } else {
            if (category_name == null)
                categoryName.setText("All News");
            else
                categoryName.setText(category_name);
        }

        NewsAppUtil.logMessage("checkback", "category name is: " + category_name);
        bookmark_icon = (ImageView) findViewById(R.id.bookmark_icon);
        bookmark_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ImageView) v).startAnimation(scaleAnimation);
                bookmark();
            }
        });

        findViewById(R.id.settings_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NewsAppUtil.logMessage("sailesh", "opening settings " + present_category_id);
                // SettingsFragment.newInstance(present_category_id).show(getSupportFragmentManager(), SettingsFragment.class.getName());
                SettingsFragment frag = SettingsFragment.newInstance(present_category_id);
                android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(frag, "loading");
                transaction.commitAllowingStateLoss();
                ((ImageView) v).startAnimation(rotationAnimation);
            }
        });

        setHomeAdapter();
    }

    public void setHomeAdapter() {

        if (themeRes) {
            mainFrameLayout.setBackgroundColor(getResources().getColor(R.color.white));
            setTheme(R.style.AppTheme_Light);
        } else {
            setTheme(R.style.AppTheme_Dark);
            mainFrameLayout.setBackgroundColor(getResources().getColor(R.color.black));
        }

        vpPager = (ViewPager) findViewById(R.id.FlashViewpager);
        vpPager.addOnPageChangeListener(this);
      /*  pagerScroller = (ScrollView) findViewById(R.id.vpscroll);*/

        //vpPager.setPageTransformer(true, new DepthPageTransformer()); //TRansformation
        /*----------------------------------------------------------*/

        if (!(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)) {
        }
        vpPager.setPageTransformer(true, new DepthPageTransformer()); //TRansformation

        /*-----------------------------------------------------------*/

        /**
         * Create NewsAdapter with FragmentManager, News_JSON, length
         * */
        if (present_category_id == null) {
            adapterViewPager = new NewsAdapter(this, getSupportFragmentManager(), themeRes, true, null, selectedList);
            vpPager.setAdapter(adapterViewPager);
            vpPager.addOnPageChangeListener(new OnArticlePageChange());
        } else if (present_article_id != -23) {
            adapterViewPager = new NewsAdapter(this, getSupportFragmentManager(), themeRes, false, present_category_id, selectedList);

            vpPager.setAdapter(adapterViewPager);
            vpPager.addOnPageChangeListener(new OnArticlePageChange());
            vpPager.setCurrentItem(currentPosition);
        }


        switch (categoryName.getText().toString()) {
            case "Bookmarks":

                List<Bookmark> bookmarkList = Repo.getInstance(this).repoBookmarks.getAll(language);

                //Toast.makeText(HomeActivity.this, "" + currentPosition, Toast.LENGTH_SHORT).show();

                adapterViewPager.changeDataSet(adapterViewPager.bookmarksToArticles(bookmarkList));
                vpPager.setCurrentItem(currentPosition);

                break;
            case "Liked Articles":

                List<UpdatedLikeTable> updatedLikeTableList = Repo.getInstance(this).repoLike.getAll(language);

                // Toast.makeText(HomeActivity.this, "" + currentPosition, Toast.LENGTH_SHORT).show();

                adapterViewPager.changeDataSet(adapterViewPager.LikeToArticles(updatedLikeTableList));
                vpPager.setCurrentItem(currentPosition);

                break;

            case "All News":

                //adapterViewPager = new NewsAdapter(this, getSupportFragmentManager(), themeRes, false, present_category_id);
                adapterViewPager = new NewsAdapter(this, getSupportFragmentManager(), themeRes, true, null, selectedList);

                vpPager.setAdapter(adapterViewPager);
                vpPager.setCurrentItem(currentPosition);
                vpPager.addOnPageChangeListener(new OnArticlePageChange());

                break;
        }

        vpPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    private void sendArticleId(int pos) {
        try {

            NewsAppUtil.logMessage("analytics", "send analytics");
            Tracker t = ((NewsApp) getApplication()).tracker();

            String categoryId = (adapterViewPager.updatedArticleTableList.get(pos).getCategoryId() == null) ? "0" : adapterViewPager.updatedArticleTableList.get(pos).getCategoryId() + "";
            String articleId = (adapterViewPager.updatedArticleTableList.get(pos).getArticleId() == null) ? "0" : adapterViewPager.updatedArticleTableList.get(pos).getArticleId() + "";
            String articleTitle = (adapterViewPager.updatedArticleTableList.get(pos).getArticleTitle() == null) ? "0" : adapterViewPager.updatedArticleTableList.get(pos).getArticleTitle() + "";

            NewsAppUtil.logMessage("analytics", "updatedArticleTable id is: " + articleId);
            t.setScreenName(categoryId + "-" + articleId);
            t.setTitle("Title - " + articleTitle);
            t.send(new HitBuilders.ScreenViewBuilder().setCustomDimension(1, DEVICE_ID)
                            .build()
            );
        } catch (Exception e) {
        }
    }

    @Override
    public void getOldArticlesBasedOnCategory(String categoryId, Integer articleId) {

        HomeContentFragment.progressBarRefreshOldArticle.setVisibility(View.VISIBLE);

        // HomeContentFragment .progressBarRefreshOldArticle.getIndeterminateDrawable().setColorFilter(0xffFF3366, PorterDuff.Mode.SRC_IN);
       /*
        Integer[] ids = getfirstInsertedArticleIdCategoryId();
        Integer articleId = ids[0];
        Integer categoryId = ids[1];*/

        // String url="http://www.tamilflashnews.com/api/?module=article&view=archive&lid=3460";

        NewsAppUtil.logMessage("check lastId", "fetching updatedArticleTable");
        VolleyRequester.getInstance(this)
                .sendVolleyJsonObjectRequest(NewsAppConstants.ALL_ARTICLES_REQUEST_TAG, NewsAppUtil.getInstance(getApplicationContext()).getReqUrlWithNewsModuleCategoryCIDAndLID(articleId + "", categoryId + "")
                        , new VolleyJsonObjectResponder() {

                    @Override
                    public void onJsonObjectResponseSuccess(JSONObject response) {
                        try {
                            Log.d("Response", "response");
                            if (!response.isNull("a_list")) {

                                JSONArray newArticleArray = response.getJSONArray("a_list");
                                RepoArticles repoArticles = Repo.getInstance(HomeActivity.this).repoArticles;


                                for (int i = 0; i < newArticleArray.length(); ++i) {
                                    JSONObject categoryObj = newArticleArray.getJSONObject(i);
                                    String categoryID = categoryObj.getString("cid");
                                    UpdatedArticleTable updatedArticleTable = new UpdatedArticleTable().fromJSON(newArticleArray.getJSONObject(i));
                                    updatedArticleTable.setCategoryId(categoryID);

                                    Repo.getInstance(HomeActivity.this).repoArticles.insert(updatedArticleTable);
                                    adapterViewPager.addAricle(updatedArticleTable);
                                }
                                Log.d("UArticle after insert", " " + repoArticles.getCount());
                                HomeContentFragment.progressBarRefreshOldArticle.setVisibility(View.GONE);
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                        adapterViewPager.notifyDataSetChanged();
                    }

                    @Override
                    public void onJsonObjectResponseFailure(VolleyError error) {
                        NewsAppUtil.logError("Cannot retrieve updatedArticleTable " + error);
                    }
                });

    }

    @Override
    public void getOldArticlesBasedOnAllNews(Integer articleId) {
        HomeContentFragment.progressBarRefreshOldArticle.setVisibility(View.VISIBLE);
       /* HomeContentFragment .progressBarRefreshOldArticle.getIndeterminateDrawable().setColorFilter(0xffFF3366, PorterDuff.Mode.SRC_IN);
        Integer[] ids = getfirstInsertedArticleIdCategoryId();
        Integer articleId = ids[0];*/

        // String url="http://www.tamilflashnews.com/api/?module=article&view=archive&lid=3460";

        NewsAppUtil.logMessage("check lastId", "fetching updatedArticleTable");
        VolleyRequester.getInstance(this)
                .sendVolleyJsonObjectRequest(NewsAppConstants.ALL_ARTICLES_REQUEST_TAG, NewsAppUtil.getInstance(getApplicationContext()).getReqUrlWithLastId(NewsAppConstants.MODULE_ARTICLES, NewsAppConstants.VIEW_ARCHIVE, articleId + "")
                        , new VolleyJsonObjectResponder() {

                    @Override
                    public void onJsonObjectResponseSuccess(JSONObject response) {
                        try {
                            Log.d("Response", "response");
                            if (!response.isNull("a_list")) {

                                JSONArray newArticleArray = response.getJSONArray("a_list");
                                RepoArticles repoArticles = Repo.getInstance(HomeActivity.this).repoArticles;


                                for (int i = 0; i < newArticleArray.length(); ++i) {
                                    JSONObject categoryObj = newArticleArray.getJSONObject(i);
                                    String categoryID = categoryObj.getString("cid");
                                    UpdatedArticleTable updatedArticleTable = new UpdatedArticleTable().fromJSON(newArticleArray.getJSONObject(i));
                                    updatedArticleTable.setCategoryId(categoryID);

                                    Repo.getInstance(HomeActivity.this).repoArticles.insert(updatedArticleTable);
                                    adapterViewPager.addAricle(updatedArticleTable);
                                }
                                Log.d("UArticle after insert", " " + repoArticles.getCount());
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                        adapterViewPager.notifyDataSetChanged();
                        HomeContentFragment.progressBarRefreshOldArticle.setVisibility(View.GONE);
                    }

                    @Override
                    public void onJsonObjectResponseFailure(VolleyError error) {
                        NewsAppUtil.logError("Cannot retrieve updatedArticleTable " + error);
                    }
                });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        currentPosition = position;

        long randomNo;
        Random random = new Random();

        randomNo = random.nextInt(30);
        // AppRater.app_launched(this);
        if (randomNo == position)
            AppRater.app_launched(this);

    }

    @Override
    public void onPageScrollStateChanged(int state) {


    }

  /*  @Override
    public void changeBackground() {
        mainFrameLayout.setBackgroundColor(getResources().getColor(R.color.black));
        mainFrameLayout.setBackgroundColor(getResources().getColor(R.color.white));
    }
*/

    public void bookmark() {

        Log.e("DEVICE ID", DEVICE_ID);
        UpdatedArticleTable updatedArticleTable = adapterViewPager.updatedArticleTableList.get(vpPager.getCurrentItem());
        String article_id = updatedArticleTable.getArticleId() + "";
        String category_id = updatedArticleTable.getCategoryId();
        Bookmark bookmark = new Bookmark().fromArticle(updatedArticleTable);

        if (checkBookmark(category_id, article_id)) {
            bookmark_icon.setImageResource(R.drawable.bookmark_icon);
            Repo.getInstance(HomeActivity.this).repoBookmarks.deleteById(article_id);
            NewsAppUtil.logMessage("sailesh", "deleting updatedArticleTable " + category_id + ": " + article_id + "in the db");
        } else {
            bookmark_icon.setImageResource(R.drawable.bookmark_icon_on_new);
            Repo.getInstance(HomeActivity.this).repoBookmarks.insert(bookmark);
            NewsAppUtil.logMessage("sailesh", "inserting updatedArticleTable" + category_id + ": " + article_id + " in the db");

            Tracker t = ((NewsApp) getApplication()).tracker();
            String articleId = (adapterViewPager.updatedArticleTableList.get(vpPager.getCurrentItem()).getArticleId() == null) ? "0" : adapterViewPager.updatedArticleTableList.get(vpPager.getCurrentItem()).getArticleId() + "";
            NewsAppUtil.logMessage("analytics", "bookmark updatedArticleTable id is: " + articleId);
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("Android")
                    .setAction("Bookmark")
                    .setLabel(article_id)
                    .setValue(Integer.parseInt(article_id)).setCustomDimension(1, DEVICE_ID)
                    .build());
        }
    }

    public Boolean checkBookmark(String cid, String aid) {

        List<Bookmark> bookmarkList = Repo.getInstance(HomeActivity.this).repoBookmarks.getArticleBycIDandaID(cid, aid);
        if (bookmarkList == null || bookmarkList.size() == 0)
            return false;
        else
            return true;
    }

    public void setBookmarkIcon(int position) {

        if (adapterViewPager.updatedArticleTableList != null && adapterViewPager.updatedArticleTableList.size() != 0) {

            UpdatedArticleTable updatedArticleTable = adapterViewPager.updatedArticleTableList.get(position);
            String article_id = updatedArticleTable.getArticleId() + "";
            String category_id = updatedArticleTable.getCategoryId();
            if (checkBookmark(category_id, article_id)) {
                NewsAppUtil.logMessage("sailesh", "check for on updatedArticleTable already in db, set on" + category_id + ": " + article_id);
                bookmark_icon.setImageResource(R.drawable.bookmark_icon_on_new);
            } else {
                bookmark_icon.setImageResource(R.drawable.bookmark_icon);
                NewsAppUtil.logMessage("sailesh", "check for on updatedArticleTable not in db, set off" + category_id + ": " + article_id);
            }
        }
    }

    @Override
    protected void onDestroy() {

        NewsAppUtil.logMessage("backed", "destrouuyed");
        super.onDestroy();
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        SharedPreferences.Editor editor = mode.edit();
        editor.putString("lastdate", date);
        editor.commit();

    }

    @Override
    public void onBackPressed() {

        NewsAppUtil.logMessage("backed", "back pressed");
        super.onBackPressed();
        present_article_id = 0;
        present_category_id = null;
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        menu.getItem(1).setEnabled(false);
        return true;
    }

    @Override
    public void languageChanged() {

        language= sharedpreferencesLanguage.getString("LanguageSelected", "Tamil");
        ((NewsAdapter) vpPager.getAdapter()).updatedArticleTableList = Repo.getInstance(getApplicationContext()).repoArticles.getAllArticlesByDescenDateTime(language);

        List<UpdatedArticleTable> updatedArticleTableList= ((NewsAdapter) vpPager.getAdapter()).updatedArticleTableList;
        if(updatedArticleTableList.size()==0){
            Toast.makeText(getApplicationContext(),"No english articles to display",Toast.LENGTH_LONG).show();
            sharedpreferencesEditor = sharedpreferencesLanguage.edit();
            sharedpreferencesEditor.putInt("SelectedLanguagePosition", 0);
            sharedpreferencesEditor.putInt("LanguagePosition", 0);
            sharedpreferencesEditor.putString("LanguageSelected",lastSelectedLanguage);
            sharedpreferencesEditor.commit();

        }else{
            ((NewsAdapter) vpPager.getAdapter()).notifyDataSetChanged();
            vpPager.setAdapter(adapterViewPager);
        }

      //  setHomeAdapter();
    }

    // Listener on updatedArticleTable page change
    class OnArticlePageChange implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            vpPager.getParent().requestDisallowInterceptTouchEvent(true);
            NewsAppUtil.logMessage("scroll test", "on oage scrolled");
            //TODO BookMarks
        }

        @Override
        public void onPageSelected(int position) {

            NewsAppUtil.logMessage("scroll test", "on page selected");
            NewsAppUtil.logMessage("scroll test", "on page selected: position " + position);

            sendArticleId(position);
            currentPos = position;
            setBookmarkIcon(position);
            present_article_id = position;

            SharedPreferences sharedpreferences = getSharedPreferences("updatedArticles", MODE_PRIVATE);
            String updatedCount = sharedpreferences.getString("updatedCount", "0");
            NewsAppUtil.logMessage("onscroll", "data is: " + updatedCount);
            if (!updatedCount.equals("0")) {
                TextView storiesCount = (TextView) findViewById(R.id.newStoriesId);
                storiesCount.setVisibility(View.VISIBLE);
                storiesCount.setText("+" + updatedCount);
            }

            if (position == ViewPager.SCROLL_STATE_DRAGGING) {
                NewsAppUtil.logMessage("scroll test", "it is dragging");

                // Prevent the ScrollView from intercepting this event now that the page is changing.
                // When this drag ends, the ScrollView will start accepting touch events again.
                vpPager.requestDisallowInterceptTouchEvent(true);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

            NewsAppUtil.logMessage("scroll test", "scroll state changed");
            vpPager.requestDisallowInterceptTouchEvent(true);

            if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                NewsAppUtil.logMessage("scroll test", "it is dragging");

                // Prevent the ScrollView from intercepting this event now that the page is changing.
                // When this drag ends, the ScrollView will start accepting touch events again.
                vpPager.requestDisallowInterceptTouchEvent(true);
            }
        }
    }

  /*  @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }*/
}
