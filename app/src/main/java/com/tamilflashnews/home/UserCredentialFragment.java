package com.tamilflashnews.home;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tamilflashnews.R;
import com.tamilflashnews.model.UserContentData;
import com.tamilflashnews.util.UploadFileService;
import com.tamilflashnews.util.ui.ServiceGenerator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;

/**
 * Created by ${zakirhussain} on 12/24/2015.
 */
public class UserCredentialFragment extends android.support.v4.app.DialogFragment {

    public static final String USER_NAME = "UserName";
    public static final String E_MAIL = "EMail";
    public static final String USER_PHONE = "UserPhone";
    private EditText mUsernameEditText,
            mEmailEditText, mMobileEditText;
    private Button mCancelButton, mPostButton;

    private String mDialogBoxTitle;
    private UserContentData mData;

    private ProgressBar mProgressBar;

    private SharedPreferences preferences;

    public UserCredentialFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.user_credentials, container);

        mUsernameEditText = (EditText) rootView.findViewById(R.id.username_editText);
        mEmailEditText = (EditText) rootView.findViewById(R.id.email_editText);
        mMobileEditText = (EditText) rootView.findViewById(R.id.mobileEditText);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        mPostButton = (Button) rootView.findViewById(R.id.postButton);
        mCancelButton = (Button) rootView.findViewById(R.id.cancelButton);

        preferences = getPreferences();
        if (preferences != null) {
            mUsernameEditText.setText(preferences.getString(USER_NAME, ""));
            mEmailEditText.setText(preferences.getString(E_MAIL, ""));
            mMobileEditText.setText(preferences.getString(USER_PHONE, ""));
        }

        mPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mUsernameEditText.getText().toString().trim().equals("")) {
                    if (isValidEmail(mEmailEditText.getText().toString())) {
                        String mobileNumber = mMobileEditText.getText().toString();
                        if (isValidMobile(mobileNumber) && (!mobileNumber.trim().equals("") &&
                            mobileNumber.length() == 10)) {
                            showProgressBar();

                            requestData(mData);

                        } else {
                            mMobileEditText.setError("Please insert a valid mobile number");
                            mMobileEditText.requestFocus();
                        }

                    } else {
                        mEmailEditText.setError("Please Enter valid email");
                        mEmailEditText.requestFocus();
                    }
                } else {
                    mUsernameEditText.setError("Name field shouldn't be empty");
                    mUsernameEditText.requestFocus();
                }

            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "Cancel Pressed", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
        getDialog().setTitle(mDialogBoxTitle);
        return rootView;
    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    //---set the title of the dialog window---
    public void setDialogTitle(String title) {
        mDialogBoxTitle = title;
    }

    public void setUserContentData(UserContentData data) {
        mData = data;
    }

    public void requestData(UserContentData data) {
        UploadFileService service =
                ServiceGenerator.createService(UploadFileService.class);

        Map<String, TypedFile> getImages = null;
        String userName = mUsernameEditText.getText().toString();
        String userMail = mEmailEditText.getText().toString();
        String userPhone = mMobileEditText.getText().toString();

        storeUserCredentials(userName, userMail, userPhone);

        String titleText = data.getArticleTitle();
        String descriptionText = data.getDescription();
        String youTubeUrl = null;
        if (data.getYoutubeUrl() != null) {
            youTubeUrl = data.getYoutubeUrl();
        }

        ArrayList<String> imageFile = null;
        if (data.getImageFile() != null) {
            imageFile = data.getImageFile();
        }

        for (String image : imageFile) {
            Log.i("UserContentData", image);
            Map<String, TypedFile> imageFiles = new HashMap<>();
            TypedFile typedFile = new TypedFile("image/*", new File(image));
            imageFiles.put(UserContentData.IMAGE_FILE, typedFile);
            getImages = imageFiles;
        }

        if (getImages != null) {
            printMap(getImages);
        }

        String media_type = data.getMediaType();
        TypedFile mediaFile = null;
        if (media_type != null) {
            if (media_type.equals("2")) {
                mediaFile = new TypedFile("audio/*", new File(data.getMediaFile()));
            } else if (media_type.equals("3")) {
                mediaFile = new TypedFile("video/mp4", new File(data.getMediaFile()));
            } else {
                Log.e("UserCredentialFragment", "Error");
            }
        }

        String latLong = data.getLatitude() + "," + data.getLongitude();

        String deviceId = data.getDeviceId();
        String appVersion = data.getAppVersion();
        String platform = data.getPlatform();

        Log.i("UserContentActivity", "Title " + titleText + "\n" +
                "Description " + descriptionText + "\n" +
                "Media Type " + media_type + "\n" +
                "Media File " + mediaFile + "\n" +
                "Geo tag " + latLong + "\n" +
                "Device Id " + deviceId + "\n" +
                "App Version " + appVersion + "\n" +
                "Platform " + platform + "\n" +
                "Username " + mUsernameEditText.getText().toString() + "\n" +
                "Email " + mEmailEditText.getText().toString() + "\n" +
                "Mobile " + mMobileEditText.getText().toString() + "\n" +
                "Youtube url " + youTubeUrl + "\n");

        service.upload(userName, userMail, userPhone, titleText,
                descriptionText, getImages, media_type, mediaFile,
                latLong, deviceId, appVersion, platform, youTubeUrl, new Callback<Response>() {
                    @Override
                    public void success(Response response, retrofit.client.Response response2) {

                        if (response.getRstatus().equals("Error")) {
                            showUploadFailureToast();
                            hideProgressBar();
                        } else {
                            //showUploadSuccessToast();
                            hideProgressBar();
                            dismiss();
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(R.string.thanks)
                                    .setMessage(R.string.compliment_to_user)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Nothing to do here
                                        }
                                    })
                                    .show();
                            Log.i("Result", "Response " + response.getRstatus());
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showUploadFailureToast();
                        hideProgressBar();
                        dismiss();
                        Log.i("Result", "Response " + error.getMessage());
                    }
                });
    }

    private void storeUserCredentials(String userName, String userMail, String userPhone) {
        SharedPreferences sharedPref = getPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(USER_NAME, userName);
        editor.putString(E_MAIL, userMail);
        editor.putString(USER_PHONE, userPhone);
        editor.commit();
    }

    private SharedPreferences getPreferences() {
        return getActivity().getPreferences(Context.MODE_PRIVATE);
    }

    public class Response {
        String rstatus;

        public String getRstatus() {
            return rstatus;
        }

        public void setRstatus(String rstatus) {
            this.rstatus = rstatus;
        }

    }

    public static void printMap(Map mp) {
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Log.i("UserContentData",pair.getKey() + " = " + pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private boolean isValidMobile(String phone)
    {
        return Patterns.PHONE.matcher(phone).matches();
    }

    public void showUploadSuccessToast() {
        Toast.makeText(getActivity(), "Your content has been uploaded successfully", Toast.LENGTH_SHORT).show();
    }

    public void showUploadFailureToast() {
        Toast.makeText(getActivity(), "Server Failed.Please try again", Toast.LENGTH_SHORT).show();
    }

}
