package com.tamilflashnews.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.tamilflashnews.R;
import com.tamilflashnews.util.ui.transformers.DepthPageTransformer;

/**
 * Created by Zakirhussain on 10/1/2015.
 */
public class AppIntroduction extends AppCompatActivity
        implements AppIntroductionAdapter.AdapterListener {

    private String mArticleId;
    private boolean mMultiCategory;
    private SharedPreferences appIntroPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_screen);
        appIntroPreference = getSharedPreferences("App Intro", Context.MODE_PRIVATE);
        mArticleId = getIntent().getStringExtra("articleId");
        mMultiCategory = getIntent().getBooleanExtra("multiCategoryIntro", false);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        AppIntroductionAdapter appIntroductionAdapter = new AppIntroductionAdapter(getLayoutInflater(),
                appIntroPreference,
                mArticleId,
                this);
        appIntroductionAdapter.setOnAdapterLister(this);
        viewPager.setAdapter(appIntroductionAdapter);

        viewPager.setPageTransformer(true, new DepthPageTransformer());

    }


    private void loadHomeActivity(){
        SharedPreferences.Editor editor = appIntroPreference.edit();
        editor.putBoolean("App Intro Completed", true);
        editor.commit();

        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("articleId", mArticleId);
        startActivity(intent);
        finish();
    }


    @Override
    public void onClick() {
        loadHomeActivity();
    }
}
