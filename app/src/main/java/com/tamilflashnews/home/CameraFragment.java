package com.tamilflashnews.home;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tamilflashnews.R;

/**
 * Created by CIPL319 on 11/19/2015.
 */
public class CameraFragment extends Fragment {

    private static final int CAMERA_REQUEST = 1888;
    private ImageView camerImage;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.camera_activity, container, false);

        camerImage = (ImageView) rootView.findViewById(R.id.iv_camOutput);


        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);


        return rootView;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA_REQUEST) {
            if (data != null) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                camerImage.setImageBitmap(photo);
            }
        }

    }


}