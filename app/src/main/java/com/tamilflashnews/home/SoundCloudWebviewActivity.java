package com.tamilflashnews.home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.tamilflashnews.R;

public class SoundCloudWebviewActivity extends AppCompatActivity {


    private WebView soundCloudWebview;
    private Intent homeContentIntent;
    private String soundCloudUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState)     {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sound_cloud_webview_activity);

        homeContentIntent = getIntent();
        soundCloudUrl = homeContentIntent.getStringExtra("soundCloudUrl");

        Toolbar toolbar = (Toolbar) findViewById(R.id.title_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        soundCloudWebview = (WebView) findViewById(R.id.webview_sound_cloud);

        soundCloudWebview.setWebViewClient(new MyWebViewClient());
        soundCloudWebview.getSettings().setJavaScriptEnabled(true);
       openURL(soundCloudUrl);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    /**
     * Opens the URL in a browser
     */
    private void openURL(String url) {
        soundCloudWebview.loadUrl(url);
        soundCloudWebview.requestFocus();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        soundCloudWebview.destroy();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            soundCloudWebview.destroy();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //soundCloudWebview.stopLoading();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        soundCloudWebview.destroy();
        finish();
    }
}
