package com.tamilflashnews.home;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.tamilflashnews.R;
import com.tamilflashnews.util.adapters.ImageAlbumAdapter;

public class ImageAlbumActivity extends AppCompatActivity implements PageCountInterface ,View.OnClickListener {

    private Intent homeContentIntent;
    private String imageAlbumUrl, articleTitle;
    private TextView txtImageCount, txtClose, txtArticleTitle;
    private ViewPager imageAlbumViewPager;
    private String albumUrls[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_album_activity);

        homeContentIntent = getIntent();
        imageAlbumUrl = homeContentIntent.getStringExtra("imageAlbumUrl");
        articleTitle = homeContentIntent.getStringExtra("articleTitle");

        albumUrls = imageAlbumUrls(imageAlbumUrl);

        imageAlbumViewPager = (ViewPager) findViewById(R.id.image_album_view_pager);
        txtImageCount = (TextView) findViewById(R.id.textview_image_count);
        txtClose = (TextView) findViewById(R.id.textview_close);
        txtArticleTitle = (TextView) findViewById(R.id.textview_article_title);

        txtClose.setOnClickListener(this);
        ImageAlbumAdapter imageAlbumAdapter = new ImageAlbumAdapter(this, albumUrls);
        imageAlbumViewPager.setAdapter(imageAlbumAdapter);

    }

    public String[] imageAlbumUrls(String imageAlbumUrl) {

        String imageUrls[] = new String[20];
        imageUrls = imageAlbumUrl.split(",");

        return imageUrls;
    }

    @Override
    public void pageCount() {
        int imageCount = imageAlbumViewPager.getCurrentItem() + 1;

        txtImageCount.setText("Photos " + imageCount + " of " + albumUrls.length);
        txtArticleTitle.setText(articleTitle);
    }

    @Override
    public void onClick(View v) {
        finish();

    }
}
