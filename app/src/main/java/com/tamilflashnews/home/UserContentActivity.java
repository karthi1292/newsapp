package com.tamilflashnews.home;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tamilflashnews.R;
import com.tamilflashnews.model.NewsAppConstants;
import com.tamilflashnews.model.UserContentData;
import com.tamilflashnews.util.CircleIndicator;
import com.tamilflashnews.util.adapters.CameraPreviewAdapter;
import com.thefinestartist.ytpa.enums.Quality;
import com.thefinestartist.ytpa.utils.YouTubeThumbnail;
import com.thefinestartist.ytpa.utils.YoutubeUrlParser;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.tamilflashnews.R.id.touchListenerView;

/**
 * Created by CIPL319 on 11/19/2015.
 */
public class UserContentActivity extends AppCompatActivity implements View.OnClickListener,
        MediaRecorder.OnInfoListener, ActivityCompat.OnRequestPermissionsResultCallback {

    private static final int LONG_DELAY = 3500; // 3.5 seconds
    public static final String VIDEO_URI = "VideoUri";
    public static final String AUDIO_URI = "AudioUri";
    private static final String TAG = UserContentActivity.class.getSimpleName();
    public static final String YOUTUBE_URL = "YoutubeUrl";
    public Button camera, video, voice, send, buttonCamera, buttonGalery, mYoutubeUrlButton;
    public EditText description, title;
    ImageView demo, mVideoPlayImageView, mRecordAudioImageView,
            mPlayAudioImageView, mStopAudioImageView;

    View mTouchListenerView;

    private FloatingActionButton mFab;

    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final int IMAGE_FROM_GALERY = 300;
    private static final int VIDEO_FROM_GALERY = 400;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 3;
    public static final int MEDIA_TYPE_AUDIO = 2;

    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".mp4";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";

    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "TamilFlashNews";

    private Uri fileUri;
    private Uri mAudioFileUri; //  url to store audio file
    private Uri mImageFileUri; // uri to store image file
    private Uri mVideoFileUri; // uri to store video file

    private MediaRecorder recorder = null;
    private int currentFormat = 0;
    private int output_formats[] = {MediaRecorder.OutputFormat.MPEG_4,
            MediaRecorder.OutputFormat.THREE_GPP};
    private String file_exts[] = {AUDIO_RECORDER_FILE_EXT_MP4,
            AUDIO_RECORDER_FILE_EXT_3GP};


    private Toolbar toolbar;
    private LinearLayout mChosePictureContainer;
    private RelativeLayout mAudioContainer;

    private ArrayList<String> capturedImageUris;
    private ArrayList<String> imageFromGaleryUris;

    private ViewPager mUserContentViewPager;
    private CameraPreviewAdapter adapter;

    private String img_Decodable_Str;


    private MediaRecorder mRecorder;
    private MediaPlayer mediaPlayer;

    private File mOutputFile;
    //private long Audio_MAX_FILE_SIZE = 26214400;//25Mb--1Mb=1000000bytes
    private long Audio_MAX_FILE_SIZE = 1100000;//1Mb
    private int AUDIO_MAX_TIME_DURATION = (60000 * 3);
    private Handler mHandler = new Handler();
    private Runnable mTickExecutor = new Runnable() {
        @Override
        public void run() {
            tick();
            mHandler.postDelayed(mTickExecutor, 100);
        }
    };

    private int[] amplitudes = new int[100];
    private int i = 0;
    private long mStartTime = 0;
    private TextView mTimerTextView;

    private int mLength;
    private boolean isAudioPaused = false;
    private boolean isAudioResumed = false;
    private boolean isUploadVideoChecked = false;
    private boolean isAudioCompletedPlaying = false;
    private boolean isMediaContainerVisible = true;
    private boolean isAudioPermissionGranted = false;
    private boolean isYouTubeThumbnailFound = false;

    private String mLongitude;
    private String mLatitude;
    private String mDeviceId;

    // Circle indicator
    private CircleIndicator mCircleIndicator;

    private double startTime = 0;
    private Handler myHandler = new Handler();

    private Location location;

    /**
     * Id to identify a camera permission request.
     */
    private static final int REQUEST_CAMERA = 0;
    /**
     * Id to identify a audio permission request.
     */
    private static final int REQUEST_AUDIO = 1;
    /**
     * Id to identify a WRITE_EXTERNAL_STORAGE permission request.
     */
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 2;

    /**
     * Id to identify a Location permission request.
     */
    private static final int REQUEST_LOCATION = 3;

    private RelativeLayout mRootView;

    private String videoId;

    private String youtubeUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_content);

        mDeviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setTitle("User Content Contribution");
            toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        capturedImageUris = new ArrayList<>();

        title = (EditText) findViewById(R.id.et_uTitle);
        description = (EditText) findViewById(R.id.et_uDescription);
        camera = (Button) findViewById(R.id.iv_uCamera);
        video = (Button) findViewById(R.id.iv_uVideo);
        voice = (Button) findViewById(R.id.iv_uVoice);
        send = (Button) findViewById(R.id.nextButton);
        demo = (ImageView) findViewById(R.id.demo_view);
        mChosePictureContainer = (LinearLayout) findViewById(R.id.chosePictureContainer);
        buttonCamera = (Button) findViewById(R.id.cameraButton);
        buttonGalery = (Button) findViewById(R.id.galeryButton);
        mVideoPlayImageView = (ImageView) findViewById(R.id.videoPlayImageView);
        mUserContentViewPager = (ViewPager) findViewById(R.id.user_content_view_pager);
        mAudioContainer = (RelativeLayout) findViewById(R.id.audioContainer);
        mRecordAudioImageView = (ImageView) findViewById(R.id.recordAudioImageView);
        mPlayAudioImageView = (ImageView) findViewById(R.id.playAudioImageView);
        mStopAudioImageView = (ImageView) findViewById(R.id.stopAudioImageView);
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mTouchListenerView = findViewById(touchListenerView);
        mCircleIndicator = (CircleIndicator) findViewById(R.id.indicator_default);
        mTimerTextView = (TextView) findViewById(R.id.timerTextView);
        mRootView = (RelativeLayout) findViewById(R.id.rootView);
        mYoutubeUrlButton = (Button) findViewById(R.id.youtubeUrlButton);


        camera.setOnClickListener(this);
        video.setOnClickListener(this);
        voice.setOnClickListener(this);
        send.setOnClickListener(this);
        buttonCamera.setOnClickListener(this);
        buttonGalery.setOnClickListener(this);
        mVideoPlayImageView.setOnClickListener(this);

        mRecordAudioImageView.setOnClickListener(this);
        mPlayAudioImageView.setOnClickListener(this);
        mStopAudioImageView.setOnClickListener(this);

        mTouchListenerView.setOnClickListener(this);

        mFab.setOnClickListener(this);
        mYoutubeUrlButton.setOnClickListener(this);

        //LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);


        // location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        showLog(TAG, "Checking permission for location.");
        // Check if the Location permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Location permission has not been granted.
            requestLocationPermission();

        } else {

            // Location permissions is already available, .
            showLog(TAG, "Location permission has already been granted. Now we can get current location.");
            LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_uCamera:
                mCircleIndicator.setVisibility(View.VISIBLE);
                hideSelectPictureContainer();
                mAudioContainer.setVisibility(View.GONE);
                mVideoPlayImageView.setVisibility(View.GONE);
                hideYouTubeUrlButton();
                isUploadVideoChecked = false;
                mUserContentViewPager.setVisibility(View.VISIBLE);
                if (capturedImageUris != null && capturedImageUris.size() == 8) {
                    hideFab();
                } else {
                    showFab();
                }
                if (capturedImageUris.size() == 0 && imageFromGaleryUris == null) {
                    showDemo(getResources().getDrawable(R.drawable.photo_banner));
                    showFab();
                } else {
                    hideDemo();
                }
                break;
            case R.id.iv_uVideo:
                mCircleIndicator.setVisibility(View.GONE);
                hideSelectPictureContainer();
                mAudioContainer.setVisibility(View.GONE);
                mUserContentViewPager.setVisibility(View.GONE);
                hideYouTubeUrlButton();
                hideSelectPictureContainer();
                boolean isTrue = mVideoFileUri == null;
                showLog("Video", "Is True " + isTrue);
                if (mVideoFileUri == null) {
                    showDemo(getResources().getDrawable(R.drawable.video_banner));
                } else {
                    if (!isYouTubeThumbnailFound) {
                        demo.setVisibility(View.VISIBLE);
                        demo.setImageBitmap(createThumbnailFromPath(mVideoFileUri.getPath(), MediaStore.Images.Thumbnails.MINI_KIND));
                        mVideoPlayImageView.setVisibility(View.VISIBLE);
                    }

                }
                isUploadVideoChecked = true;
                showFab();
                break;
            case R.id.iv_uVoice:
                mCircleIndicator.setVisibility(View.GONE);
                if (mOutputFile == null) {
                    mPlayAudioImageView.setEnabled(false);
                    mStopAudioImageView.setEnabled(false);
                    mPlayAudioImageView.setAlpha(0.7f);
                    mStopAudioImageView.setAlpha(0.7f);
                    hideTimerTextView();
                    isUploadVideoChecked = false;
                    hideDemo();
                    hideFab();
                } else {
                    hideFab();
                    //audioPlayOperation();
                    //pauseAudio();
                    showTimerTextView();
                }

                hideSelectPictureContainer();
                mUserContentViewPager.setVisibility(View.GONE);
                mAudioContainer.setVisibility(View.VISIBLE);
                hideDemo();
                break;
            case R.id.nextButton:
                getUserCredentials();
                break;
            case R.id.cameraButton:
                if (isUploadVideoChecked) {
                    recordVideo();
                } else {
                    captureImage();
                }
                break;
            case R.id.galeryButton:
                if (isUploadVideoChecked) {
                    selectVideoFromGalery();
                } else {
                    selectImageFromGalery();
                }
                break;
            case R.id.videoPlayImageView:
                //mVideoPlayImageView.setVisibility(View.GONE);
                init();
                break;
            case R.id.recordAudioImageView:
                hideSelectPictureContainer();
                isAudioResumed = false;
                isAudioPaused = false;
                startRecording();
                if (isAudioPermissionGranted) {
                    mRecordAudioImageView.setEnabled(false);
                    mRecordAudioImageView.setImageResource(R.drawable.recording);
                } else {
                    mRecordAudioImageView.setEnabled(true);
                }
                break;
            case R.id.playAudioImageView:
                showLog("Testing", "play mode");
                audioPlayOperation();
                break;
            case R.id.stopAudioImageView:
                stopRecording(true);
                mRecordAudioImageView.setEnabled(true);

                break;

            case R.id.fab:
                if (isMediaContainerVisible) {
                    showSelectPictureContainer();
                    isMediaContainerVisible = false;
                    if (isUploadVideoChecked) {
                        showYouTubeUrlButton();
                    } else {
                        hideYouTubeUrlButton();
                    }
                } else {
                    hideSelectPictureContainer();
                    isMediaContainerVisible = true;
                    hideYouTubeUrlButton();
                }
                break;

            case R.id.youtubeUrlButton:
                showYouTubeUrlDialog();
                break;

            case R.id.touchListenerView:
                hideSelectPictureContainer();
                break;
        }
    }

    private void showYouTubeUrlDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.youtube_custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText youTubeUrlEditText = (EditText) dialogView.findViewById(R.id.youtubeUrlEditText);

        dialogBuilder.setTitle("Youtube Url");

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                youtubeUrl = youTubeUrlEditText.getText().toString();
                videoId = YoutubeUrlParser.getVideoId(youtubeUrl);
                try {
                    Picasso.with(UserContentActivity.this)
                            .load(YouTubeThumbnail.getUrlFromVideoId(videoId, Quality.HIGH)).
                            placeholder(R.drawable.video_banner)
                            .fit()
                            .centerCrop()
                            .into(demo);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                hideSelectPictureContainer();
                hideYouTubeUrlButton();
                hideCameraButton();
                hideVoiceButton();
                isYouTubeThumbnailFound = true;
                mVideoPlayImageView.setVisibility(View.GONE);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                hideSelectPictureContainer();
                hideYouTubeUrlButton();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private SharedPreferences getPreferences() {
        return getPreferences(Context.MODE_PRIVATE);
    }

    private void hideYouTubeUrlButton() {
        mYoutubeUrlButton.setVisibility(View.GONE);
    }

    private void showYouTubeUrlButton() {
        mYoutubeUrlButton.setVisibility(View.VISIBLE);
    }

    private boolean isValidYouTubeUrl(String youTubeUrl) {
        return Patterns.WEB_URL.matcher(youTubeUrl).matches();
    }

    private void hideTimerTextView() {
        mTimerTextView.setVisibility(View.GONE);
    }

    private void showTimerTextView() {
        mTimerTextView.setVisibility(View.VISIBLE);
    }

    private void audioPlayOperation() {
        /*if (isAudioPaused) {
            isAudioResumed = true;
            isAudioPaused = false;
            pauseAudio();
        } else {
            isAudioPaused = true;
            if (isAudioResumed) {
                isAudioPaused = true;
                isAudioResumed = false;
                resumeAudio();
            } else if (isAudioCompletedPlaying) {
                playAudio();
            } else {
                playAudio();
            }
        }*/

        Intent audioPreviewIntent = new Intent(this, AudioPlayerActivity.class);
        audioPreviewIntent.putExtra(AUDIO_URI, mOutputFile.getPath());
        startActivity(audioPreviewIntent);
    }


    private void hideDemo() {
        demo.setVisibility(View.GONE);
    }

    private void showDemo(Drawable drawable) {
        demo.setVisibility(View.VISIBLE);
        demo.setImageDrawable(drawable);
    }

    private void selectVideoFromGalery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, VIDEO_FROM_GALERY);
    }

    private void showFab() {
        mFab.setVisibility(View.VISIBLE);
        mFab.setEnabled(true);
    }

    private void hideFab() {
        mFab.setEnabled(true);
        mFab.setVisibility(View.GONE);
    }

    private void getUserCredentials() {

        UserContentData data = new UserContentData();
        String titleText = title.getText().toString();
        String descriptionText = description.getText().toString();

        if (location != null) {
            mLongitude = String.valueOf(location.getLongitude());
            mLatitude = String.valueOf(location.getLatitude());
        } else {
            mLatitude = null;
            mLatitude = null;
        }

        String latLong = mLongitude + "," + mLatitude;
        String deviceId = mDeviceId;
        String appVersion = NewsAppConstants.APP_VERSION + "";
        String platform = "android";


        data.setArticleTitle(titleText);
        data.setDescription(descriptionText);
        data.setImageFile(capturedImageUris);
        data.setLatitude(mLatitude);
        data.setLongitude(mLongitude);
        data.setDeviceId(deviceId);
        data.setAppVersion(appVersion);
        data.setPlatform(platform);

        if (youtubeUrl != null) {
            data.setYoutubeUrl(youtubeUrl);
        }

        if (capturedImageUris != null) {
            for (String image : capturedImageUris) {
                showLog("ImagePath", "Images: " + image + "\n");
            }
        }

        String media_type = null;
        String media_file = null;

        if (media_file == null) {
            if (mOutputFile != null) {
                media_type = MEDIA_TYPE_AUDIO + "";
                media_file = mOutputFile.getPath();
                data.setMediaType(media_type);
                data.setMediaFile(media_file);
            } else if (mVideoFileUri != null) {
                media_type = MEDIA_TYPE_VIDEO + "";
                media_file = mVideoFileUri.getPath();
                data.setMediaType(media_type);
                data.setMediaFile(media_file);
            } else {
                media_type = null;
                media_file = null;
            }
        }

        showLog("UserContentActivity", "Title " + titleText + "\n" +
                "Description " + descriptionText + "\n" +
                "Image File" + capturedImageUris.size() + "\n" +
                "Media Type " + media_type + "\n" +
                "Media File " + media_file + "\n" +
                "Geo tag " + latLong + "\n" +
                "Device Id " + deviceId + "\n" +
                "App Version " + appVersion + "\n" +
                "Platform " + platform + "\n" +
                "Youtube Url " + youtubeUrl);


        if (titleText.trim().equals("") || descriptionText.trim().equals("")) {
            new AlertDialog.Builder(this)
                    .setTitle("Oops!")
                    .setMessage("Title and Description shouldn't be empty!")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Nothing to do here
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            showUserCredentialsDialog(data);
        }
    }

    private void showLog(String tag, String message) {
        Log.i(tag, message);
    }

    private void showUserCredentialsDialog(UserContentData data) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        UserCredentialFragment credentialFragment = new UserCredentialFragment();
        credentialFragment.setDialogTitle("About you");
        credentialFragment.setUserContentData(data);
        Toast.makeText(UserContentActivity.this, R.string.notification_to_user, Toast.LENGTH_LONG).show();
        credentialFragment.show(fragmentManager, "User");
    }

    public void startRecording() {
        hideVideoButton();
        showVoiceButton();

        disableCameraAndVoice();
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            if (isAudioPermissionGranted) {
                showTimerTextView();
                mPlayAudioImageView.setEnabled(false);
                mPlayAudioImageView.setAlpha(0.7f);

                mStopAudioImageView.setEnabled(true);
                mStopAudioImageView.setAlpha(1.0f);
            } else {
                mPlayAudioImageView.setEnabled(false);
                mPlayAudioImageView.setAlpha(0.7f);

                mStopAudioImageView.setEnabled(false);
                mStopAudioImageView.setAlpha(0.7f);
            }
        } else {
            showTimerTextView();
            mPlayAudioImageView.setEnabled(false);
            mPlayAudioImageView.setAlpha(0.7f);

            mStopAudioImageView.setEnabled(true);
            mStopAudioImageView.setAlpha(1.0f);
        }


        showLog(TAG, "Show audio button pressed. Checking permission.");
        // Check if the Audio permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            // Audio permission has not been granted.
            requestAudioPermission();
        } else {
            // Audio permissions is already available, start recording.
            showLog(TAG, "AUDIO permission has already been granted. Audio started recording.");
            recordAudio();
        }
    }

    private void recordAudio() {
        mRecorder = new MediaRecorder();

        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
            mRecorder.setAudioEncodingBitRate(48000);
        } else {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.setAudioEncodingBitRate(64000);
        }

        mRecorder.setMaxDuration(AUDIO_MAX_TIME_DURATION);
        mRecorder.setOnInfoListener(this);

        mRecorder.setMaxFileSize(Audio_MAX_FILE_SIZE);
        mRecorder.setAudioSamplingRate(16000);
        mOutputFile = getOutputFile();

        mOutputFile.getParentFile().mkdirs();
        if (mOutputFile.exists()) {
            mOutputFile.delete();
        }

        showLog(TAG, "Show audio button pressed. Checking permission for WRITE_EXTERNAL_STORAGE.");
        // Check if the WRITE_EXTERNAL_STORAGE permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // WRITE_EXTERNAL_STORAGE permission has not been granted.
            requestWriteExternalStoragePermission();
        } else {
            // WRITE_EXTERNAL_STORAGE permissions is already available, now we can write to disk.

            showLog(TAG, "WRITE_EXTERNAL_STORAGE permission has already been granted. WRITING TO STORAGE now happening.");
            mRecorder.setOutputFile(mOutputFile.getAbsolutePath());
        }


        try {
            mRecorder.prepare();
            mRecorder.start();
            mStartTime = SystemClock.elapsedRealtime();
            mHandler.postDelayed(mTickExecutor, 100);
            Log.d("Voice Recorder", "started recording to " + mOutputFile.getAbsolutePath());
        } catch (IOException e) {
            Log.e("Voice Recorder", "prepare() failed " + e.getMessage());
        }
    }

    private void disableCameraAndVoice() {
        camera.setEnabled(false);
        voice.setEnabled(false);
    }

    private void showVoiceButton() {
        voice.setAlpha(1.0f);
        voice.setEnabled(true);
    }

    private void hideVideoButton() {
        video.setEnabled(false);
        video.setAlpha(0.7f);
    }

    public File getOutputFile() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US);
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString()
                + "/Voice Recorder/RECORDING_"
                + dateFormat.format(new Date())
                + ".m4a");
    }

    public void stopRecording(boolean saveFile) {

        enableCameraAndVoice();

        mPlayAudioImageView.setEnabled(true);
        mPlayAudioImageView.setAlpha(1.0f);
        mStopAudioImageView.setAlpha(0.7f);
        mStopAudioImageView.setEnabled(false);

        showTimerTextView();

        mRecordAudioImageView.setImageResource(R.drawable.record);
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        mStartTime = 0;
        mHandler.removeCallbacks(mTickExecutor);
        if (!saveFile && mOutputFile != null) {
            mOutputFile.delete();
        }
    }

    private void enableCameraAndVoice() {
        camera.setEnabled(true);
        voice.setEnabled(true);
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
            stopRecording(true);
            mRecordAudioImageView.setEnabled(true);
        }
    }

    private void playAudio() {
        mStopAudioImageView.setEnabled(false);
        mStopAudioImageView.setAlpha(0.7f);

        mPlayAudioImageView.setImageDrawable(getResources().getDrawable(R.drawable.pause));

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {

            mediaPlayer.setDataSource(mOutputFile.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mediaPlayer.prepare(); // must call prepare first
            mediaPlayer.start(); // then start
            startTime = mediaPlayer.getCurrentPosition();
            showLog("Testing", "Playing: Path" + mOutputFile.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        audioCompleted();
    }

    private void pauseAudio() {
        mediaPlayer.pause();
        mLength = mediaPlayer.getCurrentPosition();
        mPlayAudioImageView.setImageDrawable(getResources().getDrawable(R.drawable.play));
        showLog("Testing", "Audio Length" + mLength);
    }

    private void resumeAudio() {
        mPlayAudioImageView.setImageDrawable(getResources().getDrawable(R.drawable.pause));
        mediaPlayer.seekTo(mLength);
        mediaPlayer.start();
        mPlayAudioImageView.setImageDrawable(getResources().getDrawable(R.drawable.pause));
        audioCompleted();
    }

    private void audioCompleted() {

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            public void onCompletion(MediaPlayer mp) {
                showLog("Completion Listener", "Song Complete");
                //Toast.makeText(UserContentActivity.this, "Media Completed", Toast.LENGTH_SHORT).show();
                isAudioPaused = false;
                isAudioResumed = false;
                isAudioCompletedPlaying = true;
                mPlayAudioImageView.setImageDrawable(getResources().getDrawable(R.drawable.play));
            }
        });

    }

    private void selectImageFromGalery() {
        hideDemo();
        showCameraButton();
        /*video.setEnabled(false);
        video.setAlpha(0.7f);*/
        /*Intent intent = new Intent(UserContentActivity.this, CustomPhotoGaleryActivity.class);
        startActivityForResult(intent,IMAGE_FROM_GALERY);*/
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, IMAGE_FROM_GALERY);
    }


    private void showSelectPictureContainer() {
        mChosePictureContainer.setVisibility(View.VISIBLE);
    }

    private void hideSelectPictureContainer() {
        mChosePictureContainer.setVisibility(View.GONE);
    }

    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        fileUri = Uri.parse(file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat]);

        mAudioFileUri = fileUri;

        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat]);

    }


    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            Toast.makeText(UserContentActivity.this,
                    "Error: " + what + ", " + extra, Toast.LENGTH_SHORT).show();
        }
    };

    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            Toast.makeText(UserContentActivity.this,
                    "Warning: " + what + ", " + extra, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    /*------------------------------------End of Audio----------------------*/

    /**
     * Checking device has camera hardware or not
     */
    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /*
     * Capturing Camera Image will lauch camera app requrest image capture
     */
    private void captureImage() {
        showCameraButton();
        showLog(TAG, "Show camera button pressed. Checking permission.");
        // Check if the Camera permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Camera permission has not been granted.
            requestCameraPermission();

        } else {

            // Camera permissions is already available, show the camera preview.
            showLog(TAG, "CAMERA permission has already been granted. Displaying camera preview.");

            showCameraPreview();
        }
    }

    private void showCameraButton() {
        camera.setEnabled(true);
        camera.setAlpha(1.0f);
    }

    public void showCameraPreview() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        showLog(TAG, "Show camera button pressed. Checking permission for WRITE_EXTERNAL_STORAGE.");
        // Check if the WRITE_EXTERNAL_STORAGE permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // WRITE_EXTERNAL_STORAGE permission has not been granted.
            requestWriteExternalStoragePermission();

        } else {

            // WRITE_EXTERNAL_STORAGE permissions is already available, now we can write to disk.
            showLog(TAG, "WRITE_EXTERNAL_STORAGE permission has already been granted. WRITING TO STORAGE now happening.");

            mImageFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageFileUri);
            //intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION)
            //demo.setVisibility(View.GONE);
            // start the image capture Intent
            startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        }

    }

    /**
     * Requests the Camera permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestCameraPermission() {
        showLog(TAG, "CAMERA permission has NOT been granted. Requesting permission.");

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            showLog(TAG, "Displaying camera permission rationale to provide additional context.");
            Snackbar.make(mRootView, R.string.permission_camera_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(UserContentActivity.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    REQUEST_CAMERA);
                        }
                    })
                    .show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
    }

    /**
     * Requests the Audio permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */

    public void requestAudioPermission() {
        showLog(TAG, "Audio permission has NOT been granted. Requesting permission.");

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.RECORD_AUDIO)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            showLog(TAG, "Displaying audio permission rationale to provide additional context.");
            Snackbar.make(mRootView, R.string.permission_audio_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(UserContentActivity.this,
                                    new String[]{Manifest.permission.RECORD_AUDIO},
                                    REQUEST_AUDIO);
                        }
                    })
                    .show();
        } else {

            // Audio permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_AUDIO);
        }
    }

    /**
     * Requests the Write external storage permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */

    public void requestWriteExternalStoragePermission() {
        showLog(TAG, "WRITE_EXTERNAL permission has NOT been granted. Requesting permission.");

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            showLog(TAG, "Displaying write external storage permission rationale to provide additional context.");
            Snackbar.make(mRootView, R.string.permission_write_external_storage_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(UserContentActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    })
                    .show();
        } else {

            // Audio permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    /**
     * Requests the Camera permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestLocationPermission() {
        showLog(TAG, "Location permission has NOT been granted. Requesting permission.");

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            showLog(TAG, "Displaying location permission rationale to provide additional context.");
            Snackbar.make(mRootView, R.string.permission_location_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(UserContentActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_LOCATION);
                        }
                    })
                    .show();
        } else {

            // Location permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CAMERA) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            showLog(TAG, "Received response for Camera permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed
                showLog(TAG, "CAMERA permission has now been granted. Showing preview.");
                Snackbar.make(mRootView, R.string.permision_available_camera,
                        Snackbar.LENGTH_SHORT).show();
            } else {
                showLog(TAG, "CAMERA permission was NOT granted.");
                Snackbar.make(mRootView, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();

            }

        } else if (requestCode == REQUEST_AUDIO) {
            showLog(TAG, "Received response for audio permissions request.");

            // Check if the only required permission has been  granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Audio permission has been granted, Record can be done
                showLog(TAG, "AUDIO permission has now been granted. Recording.");
                Snackbar.make(mRootView, R.string.permision_available_audio,
                        Snackbar.LENGTH_SHORT).show();
            } else {
                showLog(TAG, "AUDIO permission was NOT granted.");
                Snackbar.make(mRootView, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();
                isAudioPermissionGranted = false;
            }

        } else if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE) {
            showLog(TAG, "Received response for Write external storage permissions request.");

            // Check if the only required permission has been  granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Write external storage permission has been granted, Record can be done
                showLog(TAG, "WRITE EXTERNAL STORAGE permission has now been granted. Recording.");
                Snackbar.make(mRootView, R.string.permision_available_write_external_storage,
                        Snackbar.LENGTH_SHORT).show();
                isAudioPermissionGranted = true;
            } else {
                showLog(TAG, "WRITE EXTERNAL STORAGE permission was NOT granted.");
                Snackbar.make(mRootView, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();
                isAudioPermissionGranted = false;
            }

        } else if (requestCode == REQUEST_LOCATION) {
            showLog(TAG, "Received response for location permissions request.");
            // Check if the only required permission has been  granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Write external storage permission has been granted, Record can be done
                showLog(TAG, "Location permission has now been granted. Recording.");
                Snackbar.make(mRootView, R.string.permision_available_location,
                        Snackbar.LENGTH_SHORT).show();
            } else {
                showLog(TAG, "Location permission was NOT granted.");
                Snackbar.make(mRootView, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    /*
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    /*
     * Recording video
     */
    private void recordVideo() {
        hideVoiceButton();

        /*Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        mVideoFileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
        // set video quality
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mVideoFileUri); // set the image file
        // name
        // start the video capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);*/

        Intent intent1 = new Intent(this, CustomVideoActivity.class);
        startActivityForResult(intent1, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    private void hideVoiceButton() {
        voice.setEnabled(false);
        voice.setAlpha(0.7f);
    }

    private void hideCameraButton() {
        camera.setEnabled(false);
        camera.setAlpha(0.7f);
    }


    /**
     * API for creating thumbnail from Video
     *
     * @param filePath - video file path
     * @param type     - size MediaStore.Images.Thumbnails.MINI_KIND or MICRO_KIND
     * @return thumbnail bitmap
     */
    public Bitmap createThumbnailFromPath(String filePath, int type) {
        return ThumbnailUtils.createVideoThumbnail(filePath, type);
    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                previewCapturedImage();
                //hideVideoAndVoiceButton();
                hideDemo();
                hideVideoButton();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                //Toast.makeText(getApplicationContext(),
                //       "User cancelled image capture", Toast.LENGTH_SHORT)
                //       .show();
            } else {
                // failed to capture image
               /* Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();*/
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // video successfully recorded
                // preview the recorded video
                //previewVideo();
                //init();
                showLog("Log", data.getStringExtra(CustomVideoActivity.VIDEO_FILE_URI));
                mVideoFileUri = Uri.parse(data.getStringExtra(CustomVideoActivity.VIDEO_FILE_URI));
                mVideoPlayImageView.setVisibility(View.VISIBLE);
                hideSelectPictureContainer();
                demo.setImageBitmap(createThumbnailFromPath(mVideoFileUri.getPath(), MediaStore.Images.Thumbnails.MINI_KIND));
                hideVoiceButton();
                hideCameraButton();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled recording
                /*Toast.makeText(getApplicationContext(),
                        "User cancelled video recording", Toast.LENGTH_SHORT)
                        .show();*/
            } else {
                // failed to record video
                /*Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to record video", Toast.LENGTH_SHORT)
                        .show();*/
            }
        } else if (requestCode == IMAGE_FROM_GALERY && resultCode == RESULT_OK) {
            // Get the Image from data

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = null;

            showLog(TAG, "Show camera button pressed. Checking permission for WRITE_EXTERNAL_STORAGE.");
            // Check if the WRITE_EXTERNAL_STORAGE permission is already available.
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // WRITE_EXTERNAL_STORAGE permission has not been granted.
                requestWriteExternalStoragePermission();

            } else {

                // WRITE_EXTERNAL_STORAGE permissions is already available, now we can write to disk.
                showLog(TAG, "WRITE_EXTERNAL_STORAGE permission has already been granted. WRITING TO STORAGE now happening.");
                // Get the cursor

                cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                img_Decodable_Str = cursor.getString(columnIndex);

                //capturedImageUris = new ArrayList<>();
                //capturedImageUris.add(img_Decodable_Str);

                mImageFileUri = Uri.parse(img_Decodable_Str);
                hideVideoButton();

                addImages(img_Decodable_Str);

                setDataToAdapter(capturedImageUris);

                cursor.close();
                hideSelectPictureContainer();
            }


        } else if (requestCode == VIDEO_FROM_GALERY && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            showLog(TAG, "Show camera button pressed. Checking permission for WRITE_EXTERNAL_STORAGE.");
            // Check if the WRITE_EXTERNAL_STORAGE permission is already available.
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // WRITE_EXTERNAL_STORAGE permission has not been granted.
                requestWriteExternalStoragePermission();

            } else {

                // WRITE_EXTERNAL_STORAGE permissions is already available, now we can write to disk.
                showLog(TAG, "WRITE_EXTERNAL_STORAGE permission has already been granted. WRITING TO STORAGE now happening.");
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();



                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                img_Decodable_Str = cursor.getString(columnIndex);

                mVideoFileUri = Uri.parse(img_Decodable_Str);
            }


            File file = new File(mVideoFileUri.getPath());

            long length = file.length();

            showLog("Audio", "Length: " + length);
            if (length > 3000000l) {
                Toast.makeText(UserContentActivity.this, "File size limit reached", Toast.LENGTH_SHORT).show();
            } else {
                hideSelectPictureContainer();
                demo.setImageBitmap(createThumbnailFromPath(mVideoFileUri.getPath(), MediaStore.Images.Thumbnails.MINI_KIND));
                mVideoPlayImageView.setVisibility(View.VISIBLE);
                hideVoiceButton();
                hideCameraButton();
                hideYouTubeUrlButton();
            }
        }
    }

    /*
     * Display image from a path to ImageView
     */
    private void previewCapturedImage() {
        try {
            // hide video preview
            /*demo.setVisibility(View.GONE);
            mVideoPlayImageView.setVisibility(View.GONE);*/
            hideSelectPictureContainer();

            //capturedImageUris.add(img_Decodable_Str);
            addImages(mImageFileUri.getPath());

            setDataToAdapter(capturedImageUris);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void addImages(String path) {

        // list = new ArrayList<>();
        //list.add(path);
        capturedImageUris.add(path);
        if (capturedImageUris.size() == 8) {
            hideFab();
        }
    }

    private int setDataToAdapter(ArrayList<String> capturedImageUris) {

        if (adapter == null) {
            adapter = new CameraPreviewAdapter(this, capturedImageUris);
            mUserContentViewPager.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
            // adapter.changeDataSet(capturedImageUris.get(capturedImageUris.size() - 1));
        }

        mCircleIndicator.setViewPager(mUserContentViewPager);

        return capturedImageUris.size();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /*
         * Previewing recorded video
         */
    private void previewVideo() {
        try {
            // hide image preview
            hideDemo();

            //videoPreview.setVisibility(View.VISIBLE);
            //videoPreview.setVideoPath(fileUri.getPath());
            // start playing
            // videoPreview.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*---------------------VIDEO VIEW-------------------------------*/

    public void init() {
        //demo.setVisibility(View.GONE);
        Intent videoPreviewIntent = new Intent(this, VideoPreviewActivity.class);
        videoPreviewIntent.putExtra(VIDEO_URI, mVideoFileUri.getPath());
        startActivity(videoPreviewIntent);
    }

    /*------------------------------------------------*/

    /**
     * ------------ Helper Methods ----------------------
     */

	/*
     * Creating file uri to store image/video
	 */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private void tick() {
        long time = (mStartTime < 0) ? 0 : (SystemClock.elapsedRealtime() - mStartTime);
        int minutes = (int) (time / 60000);
        int seconds = (int) (time / 1000) % 60;
        int milliseconds = (int) (time / 100) % 10;
        mTimerTextView.setText(minutes + ":" + (seconds < 10 ? "0" + seconds : seconds) + "." + milliseconds);
        if (mRecorder != null) {
            amplitudes[i] = mRecorder.getMaxAmplitude();
            //Log.d("Voice Recorder","amplitude: "+(amplitudes[i] * 100 / 32767));
            if (i >= amplitudes.length - 1) {
                i = 0;
            } else {
                ++i;
            }
        }
    }


}
