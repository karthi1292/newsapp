package com.tamilflashnews.home;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tamilflashnews.R;
import com.tamilflashnews.util.CameraHelper;

import java.io.File;
import java.io.IOException;

/**
 * Created by ${zakirhussain} on 1/12/2016.
 */
public class CustomVideoActivity extends AppCompatActivity
        implements MediaRecorder.OnInfoListener, ActivityCompat.OnRequestPermissionsResultCallback {

    public static final String VIDEO_FILE_URI = "VideoUri" ;
    private Camera mCamera;
    private TextureView mPreview;
    private MediaRecorder mMediaRecorder;
    private TextView mTimerTextView;

    private int VIDEO_MAX_TIME_DURATION = (60000 * 3);
    private long Audio_MAX_FILE_SIZE = 30000000; // 30Mb

    private boolean isRecording = false;
    private static final String TAG = CustomVideoActivity.class.getSimpleName();
    private Button captureButton;

    private File mVideoUri;

    private long mStartTime = 0;
    private int i = 0;
    private int[] amplitudes = new int[100];

    private Handler mHandler = new Handler();
    private Runnable mTickExecutor = new Runnable() {
        @Override
        public void run() {
            tick();
            mHandler.postDelayed(mTickExecutor, 100);
        }
    };

    /**
     * Id to identify a camera permission request.
     */
    private static final int REQUEST_CAMERA = 0;

    /**
     * Id to identify a WRITE_EXTERNAL_STORAGE permission request.
     */
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 2;

    private FrameLayout mRootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_custom_video);

        mPreview = (TextureView) findViewById(R.id.surface_view);
        captureButton = (Button) findViewById(R.id.button_capture);
        mTimerTextView = (TextView) findViewById(R.id.timerTextViewVideo);
        mRootView = (FrameLayout) findViewById(R.id.rootLayout);

        if (isRecording) {
            // stop recording and release camera
            mMediaRecorder.stop();  // stop the recording
            releaseMediaRecorder(); // release the MediaRecorder object
            mCamera.lock();         // take camera access back from MediaRecorder

            // inform the user that recording has stopped
            setCaptureButtonText("Capture");
            isRecording = false;
            releaseCamera();
            if (mVideoUri != null) {
                Log.i("VideoUri", mVideoUri.getPath());
                setIntentData();
            }

        } else {

            showLog(TAG, "Show camera button pressed. Checking permission.");
            // Check if the Camera permission is already available.
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                // Camera permission has not been granted.
                requestCameraPermission();
            } else {
                // Camera permissions is already available, show the camera preview.
                showLog(TAG, "CAMERA permission has already been granted. Displaying camera preview.");
            }
                new MediaPrepareTask().execute(null, null, null);
        }
    }

    /**
     * The capture button controls all user interaction. When recording, the button click
     * stops recording, releases {@link android.media.MediaRecorder} and {@link android.hardware.Camera}. When not recording,
     * it prepares the {@link android.media.MediaRecorder} and starts recording.
     *
     * @param view the view generating the event.
     */
    public void onCaptureClick(View view) {
        if (isRecording) {

            // stop recording and release camera
            mMediaRecorder.stop();  // stop the recording
            releaseMediaRecorder(); // release the MediaRecorder object
            mCamera.lock();         // take camera access back from MediaRecorder

            // inform the user that recording has stopped
            setCaptureButtonText("Capture");
            isRecording = false;
            releaseCamera();
            mStartTime = 0;
            mHandler.removeCallbacks(mTickExecutor);
            if (mVideoUri != null) {
                Log.i("VideoUri", mVideoUri.getPath());
                setIntentData();
            }

        } else {
            new MediaPrepareTask().execute(null, null, null);
        }
    }

    private void setIntentData() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(VIDEO_FILE_URI, mVideoUri.getPath());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void tick() {
        long time = (mStartTime < 0) ? 0 : (SystemClock.elapsedRealtime() - mStartTime);
        int minutes = (int) (time / 60000);
        int seconds = (int) (time / 1000) % 60;
        int milliseconds = (int) (time / 100) % 10;
        mTimerTextView.setText(minutes + ":" + (seconds < 10 ? "0" + seconds : seconds));
        if (mMediaRecorder != null) {
            amplitudes[i] = mMediaRecorder.getMaxAmplitude();
            //Log.d("Voice Recorder","amplitude: "+(amplitudes[i] * 100 / 32767));
            if (i >= amplitudes.length - 1) {
                i = 0;
            } else {
                ++i;
            }
        }
    }

    private void setCaptureButtonText(String title) {
        captureButton.setText(title);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // if we are using MediaRecorder, release it first
        releaseMediaRecorder();
        // release the camera immediately on pause event
        releaseCamera();
    }

    private void releaseMediaRecorder(){
        if (mMediaRecorder != null) {
            // clear recorder configuration
            mMediaRecorder.reset();
            // release the recorder object
            mMediaRecorder.release();
            mMediaRecorder = null;
            // Lock camera for later use i.e taking it back from MediaRecorder.
            // MediaRecorder doesn't need it anymore and we will release it if the activity pauses.
            mCamera.lock();
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            // release the camera for other applications
            mCamera.release();
            mCamera = null;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private boolean prepareVideoRecorder(){

        // BEGIN_INCLUDE (configure_preview)
        mCamera = CameraHelper.getDefaultCameraInstance();

        //Rotate the preview display to match portrait
        mCamera.setDisplayOrientation(90);


        try {
            // Requires API level 11+, For backward compatibility use {@link setPreviewDisplay}
            // with {@link SurfaceView}
            mCamera.setPreviewTexture(mPreview.getSurfaceTexture());
        } catch (IOException e) {
            Log.e(TAG, "Surface texture is unavailable or unsuitable" + e.getMessage());
            return false;
        }

        mMediaRecorder = new MediaRecorder();

        /*android.hardware.Camera.Parameters parameters = mCamera.getParameters();
        android.hardware.Camera.Size size = parameters.getPictureSize();*/

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);

        /*int height = size.height;
        int width = size.width;*/

        //double mg = height * width / 1024000;

        //Log.i("Mega Pixals", "MegaPixals " + mg);


        // Step 2: Set sources
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        //set output format
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

        mMediaRecorder.setVideoSize(320,240);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        //mMediaRecorder.setProfile(profile);
        mMediaRecorder.setVideoEncodingBitRate(1700000);

        //Set the video frame rate
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setOrientationHint(90);

        mVideoUri = CameraHelper.getOutputMediaFile(CameraHelper.MEDIA_TYPE_VIDEO);

        if (mVideoUri.exists()) {
            mVideoUri.delete();
        }

        // Step 4: Set output file
        mMediaRecorder.setOutputFile(mVideoUri.toString());

        mMediaRecorder.setMaxDuration(VIDEO_MAX_TIME_DURATION);
        mMediaRecorder.setMaxFileSize(Audio_MAX_FILE_SIZE);
        mMediaRecorder.setOnInfoListener(this);
        //set audio encoder format
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        //set video encoder format
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);

        // Step 5: Prepare configured MediaRecorder
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED ||
                what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
            // stop recording and release camera
            mMediaRecorder.stop();  // stop the recording
            releaseMediaRecorder(); // release the MediaRecorder object
            mCamera.lock();         // take camera access back from MediaRecorder
            setIntentData();
            // inform the user that recording has stopped
            setCaptureButtonText("Capture");
            isRecording = false;
            releaseCamera();
            Toast.makeText(CustomVideoActivity.this, "File size/duration limit reached(30mb/3mins)", Toast.LENGTH_SHORT).show();

        }
    }


    /**
     * Asynchronous task for preparing the {@link android.media.MediaRecorder} since it's a long blocking
     * operation.
     */
    class MediaPrepareTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            // initialize video camera
            if (prepareVideoRecorder()) {
                // Camera is available and unlocked, MediaRecorder is prepared,
                // now you can start recording
                mMediaRecorder.start();
                mStartTime = SystemClock.elapsedRealtime();
                mHandler.postDelayed(mTickExecutor, 100);

                isRecording = true;
            } else {
                // prepare didn't work, release the camera
                releaseMediaRecorder();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!result) {
                CustomVideoActivity.this.finish();
            }
            // inform the user that recording has started
            setCaptureButtonText("Stop");

        }
    }

    private void requestCameraPermission() {
        showLog(TAG, "CAMERA permission has NOT been granted. Requesting permission.");

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            showLog(TAG, "Displaying camera permission rationale to provide additional context.");
            Snackbar.make(mRootView, R.string.permission_camera_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(CustomVideoActivity.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    REQUEST_CAMERA);
                        }
                    })
                    .show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
    }

    /**
     * Requests the Write external storage permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */

    public void requestWriteExternalStoragePermission() {
        showLog(TAG, "WRITE_EXTERNAL permission has NOT been granted. Requesting permission.");

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            showLog(TAG, "Displaying write external storage permission rationale to provide additional context.");
            Snackbar.make(mRootView, R.string.permission_write_external_storage_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(CustomVideoActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    })
                    .show();
        } else {

            // Audio permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CAMERA) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            showLog(TAG, "Received response for Camera permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed
                showLog(TAG, "CAMERA permission has now been granted. Showing preview.");
                Snackbar.make(mRootView, R.string.permision_available_camera,
                        Snackbar.LENGTH_SHORT).show();
            } else {
                showLog(TAG, "CAMERA permission was NOT granted.");
                Snackbar.make(mRootView, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();

            }

        }  else if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE) {
            showLog(TAG, "Received response for Write external storage permissions request.");

            // Check if the only required permission has been  granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Write external storage permission has been granted, Record can be done
                showLog(TAG, "WRITE EXTERNAL STORAGE permission has now been granted. Recording.");
                Snackbar.make(mRootView, R.string.permision_available_write_external_storage,
                        Snackbar.LENGTH_SHORT).show();
            } else {
                showLog(TAG, "WRITE EXTERNAL STORAGE permission was NOT granted.");
                Snackbar.make(mRootView, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();
            }

        }  else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showLog(String tag, String message) {
        Log.i(tag, message);
    }
}
