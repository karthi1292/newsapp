package com.tamilflashnews.home;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.share.widget.LikeView;
import com.facebook.share.widget.ShareButton;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.squareup.picasso.Picasso;
import com.tamilflashnews.NewsApp;
import com.tamilflashnews.R;
import com.tamilflashnews.category.CategoryFragment;
import com.tamilflashnews.dbhelpers.Repo;
import com.tamilflashnews.model.NewsAppConstants;
import com.tamilflashnews.model.UpdatedArticleTable;
import com.tamilflashnews.model.UpdatedLikeTable;
import com.tamilflashnews.util.NewsAppUtil;
import com.tamilflashnews.util.adapters.NewsAdapter;
import com.tamilflashnews.util.ui.ViewClip;
import com.tamilflashnews.util.ui.WebViewActivity;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.enums.Quality;
import com.thefinestartist.ytpa.utils.YouTubeThumbnail;
import com.thefinestartist.ytpa.utils.YoutubeUrlParser;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

//import com.tamilflashnews.model.Article;

public class HomeContentFragment extends android.support.v4.app.Fragment implements PageCountInterface, YouTubeThumbnailView.OnInitializedListener {

    public static NewsAdapter adapterViewPager;
    public static ProgressBar progressBarRefreshOldArticle;
    private static String VIDEO_ID = null;
    public Typeface font;
    public Boolean toggle;
    public ImageView slanting_line, imageViewReadMoreIcon;
    public LinearLayout linear_layout, linear_layout_footer, rootLinearLayout;
    public FrameLayout frame_layout, fragment_layout, frame_layout_more;
    public ImageView more_icon, category_image, playImg;
    public ScrollView verticalScroller;
    public Tracker mTracker;
    public TextView moreTextView;
    public ImageView audioImageView;
    public WebView soundCloudWebview;
    ImageView newsImg, likeImg, more, shareImg;
    View playBtnImg;
    TextView newsContent, headline, categoryName, cdateText, more_content, more_title;
    View root, line, line1;
    ShareButton fbShare;
    LikeView like;
    Context context;
    ViewClip clip;
    UpdatedArticleTable updatedArticleTable;
    String cdate;
    String[] monthArray = {"Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep",
            "Oct", "Nov", "Dec"};
    YouTubePlayer.PlayerStyle playerStyle;
    Orientation orientation;
    boolean showAudioUi;
    boolean showFadeAnim;
    File picFile;
    private int page;
    // public View videoClickImage;
    private boolean UpdatedArticlesAPIAlarm = false;
    private Animation scaleAnimation;
    private String DEVICE_ID;
    private YouTubeThumbnailView youTubeThumbnailView;
    private YouTubeThumbnailLoader youTubeThumbnailLoader;
    private String googleApiKey;
    private SharedPreferences sharedpreferencesMultiCategory;
    private SharedPreferences.Editor editor;
    boolean multiCategoryHelp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedpreferencesMultiCategory = getActivity().getSharedPreferences("MultiCategoryArticles", Context.MODE_PRIVATE);
         multiCategoryHelp = sharedpreferencesMultiCategory.getBoolean("MultiCategoryHelp", false);

        DEVICE_ID = Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("UpdatedArticlesAlarm", Context.MODE_PRIVATE);
        UpdatedArticlesAPIAlarm = sharedPreferences.getBoolean("UpdatedArticlesAPIAlarm", false);

        NewsAppUtil.logMessage("HomeContentFragment", "onCREATE");
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        //page = getArguments().getInt("someInt", 0);
        toggle = getArguments().getBoolean("toggle");
        NewsAppUtil.logMessage("testingggg", "testing dhan");

        //screenDensity();

    }



    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private String capitalize(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = context;
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        NewsAppUtil.logMessage("likeclick", "check creation frequency");
        NewsAppUtil.logMessage("likeclick", "-----------------------------");

        View view = inflater.inflate(R.layout.home_content_fragment, container, false);
        updatedArticleTable = (UpdatedArticleTable) getArguments().getSerializable(UpdatedArticleTable.class.getName());

        linear_layout = (LinearLayout) view.findViewById(R.id.linear_layout_headline);
        rootLinearLayout = (LinearLayout) view.findViewById(R.id.main_layout);

//        frame_layout = (FrameLayout) view.findViewById(R.id.frame_layout_content);
//        frame_layout_more = (FrameLayout) view.findViewById(R.id.frame_layout_more);

        // fragment_layout = (FrameLayout) view.findViewById(R.id.fragment_layout);

        category_image = (ImageView) view.findViewById(R.id.categories_image);
        slanting_line = (ImageView) view.findViewById(R.id.slantingLine);
        newsImg = (ImageView) view.findViewById(R.id.image_view_news_area);
        shareImg = (ImageView) view.findViewById(R.id.share_icon);
        likeImg = (ImageView) view.findViewById(R.id.like_icon);
        playImg = (ImageView) view.findViewById(R.id.play_icon);
        playBtnImg = (View) view.findViewById(R.id.videoClick);
        imageViewReadMoreIcon = (ImageView) view.findViewById(R.id.more);
        progressBarRefreshOldArticle = (ProgressBar) view.findViewById(R.id.progressbar_refresh_old_article);

        // audioImageView = (ImageView) view.findViewById(R.id.audio_icon);
//        moreTextView    =   (TextView) view.findViewById(R.id.share_content);
        cdateText = (TextView) view.findViewById(R.id.cdate);
        more_content = (TextView) view.findViewById(R.id.more_content);
        newsContent = (TextView) view.findViewById(R.id.content);
        headline = (TextView) view.findViewById(R.id.head_line);
        more = (ImageView) view.findViewById(R.id.more);
        //soundCloudWebview = (WebView) view.findViewById(R.id.sound_cloud_webview);
        // videoClickImage = view.findViewById(R.id.videoClick);
        line = view.findViewById(R.id.line);
        line1 = view.findViewById(R.id.line1);
        // root = view.findViewById(R.id.fragment_layout);
       /* verticalScroller = (ScrollView) view.findViewById(R.id.vpscroll);

        verticalScroller.setSmoothScrollingEnabled(false);*/

//        line1.setVisibility(View.INVISIBLE);

        scaleAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);

       /* try {
            ApplicationInfo ai = getActivity().getPackageManager().getApplicationInfo(getActivity().getPackageName(),
                    PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            googleApiKey = bundle.getString("com.thefinestartist.ytpa.YouTubePlayerActivity.ApiKey");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/

        //  youTubeThumbnailView = (YouTubeThumbnailView) view.findViewById(R.id.youtubethumbnailview);



        registerCategoryClick();
        setThemeBasedColors();
        checkArticleAlreadyLiked();
        setNewsContent();
        setHeadlineContent();
        setMoreContent();
        setArticleTimeDifferenceContent();
        // registerShareClick();


        /*audioImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newsImg.setVisibility(View.GONE);
                audioImageView.setVisibility(View.GONE);
                soundCloudWebview.setVisibility(View.VISIBLE);
                soundCloudWebview.getSettings().setJavaScriptEnabled(true);
                soundCloudWebview.setWebViewClient(new WebViewClient());
               *//* soundCloudWebview.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl("https://soundcloud.com/vikatan/bill-gates");
                        return true;
                    }
                });*//*
                // soundCloudWebview.loadUrl("http://www.google.com");
                soundCloudWebview.loadUrl("https://soundcloud.com/vikatan/bill-gates");
            }
        });*/
        shareImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {


              /*  Handler shareHandler = new Handler();
                Runnable shareRunnable = new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Its taking ScreenShot.Please Wait.", Toast.LENGTH_LONG).show();
                        v.startAnimation(scaleAnimation);
                    }
                };

                shareHandler.post(shareRunnable);*/
                v.startAnimation(scaleAnimation);
                //  final UpdatedArticleTable updatedArticleTable = HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos);
                String articleId = updatedArticleTable.getArticleId() + "";
                sendShareEventToGA(articleId);

                picFile = shareit();

                // String play_app_id = getResources().getString(R.string.play_store_id);
                // String link = getResources().getString(R.string.playlink);
                //  link+= play_app_id;


                Intent sharingIntent = new Intent(Intent.ACTION_SEND);

                //sharingIntent.setType("text/plain");
                // sharingIntent.putExtra(Intent.EXTRA_TEXT, updatedArticleTable.getArticleTitle() + "\n" + "Get the app at" + link);
                sharingIntent.putExtra(Intent.EXTRA_TEXT, updatedArticleTable.getArticleTitle() + "\n" + "Get the app at " + NewsAppConstants.APP_LINK);
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(picFile));
                //sharingIntent.setType("text/plain");
                sharingIntent.setType("image/jpeg");
                startActivity(sharingIntent);

                category_image.setImageResource(R.drawable.category_menu_new);
                category_image.setVisibility(View.VISIBLE);
            }
        });

        progressBarRefreshOldArticle.getIndeterminateDrawable().setColorFilter(0xffFF3366, PorterDuff.Mode.SRC_IN);

        registerLikeClick();
        setNewsImage();
        // setDynamicContentHeight();

        //picFile=shareit();
        return view;
    }

    private int getDeviceWidth() {

        return getDisplayMetrics().widthPixels;
    }

    private int getDeviceHeight() {

        return getDisplayMetrics().heightPixels;
    }

    private DisplayMetrics getDisplayMetrics() {

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm;
    }

   /* private int getHeadlineHeight() {

        int headlineContentTopMargin = getResourceDimensionValue(R.dimen._55sdp);
        int fontSize = getResourceDimensionValue(R.dimen._20sdp);

        int hlTopPadding = 0;
        int hlRightPadding = getResourceDimensionValue(R.dimen._5sdp);
        int hlBottomPadding = 0;
        int hlLeftPadding = getResourceDimensionValue(R.dimen._15sdp);

        int headlineContentHeight = getHeight(getActivity(), updatedArticleTable.getArticleTitle(), fontSize, getDeviceWidth(), NewsAppUtil.getInstance(getActivity()).getTitleFontFace(), hlLeftPadding, hlTopPadding, hlRightPadding, hlBottomPadding);
        int totalHeadlineHeight = headlineContentTopMargin + headlineContentHeight;
        return totalHeadlineHeight;
    }*/

  /*  private int getNewsContentHeight() {

        int fontSize = getResourceDimensionValue(R.dimen._14sdp);
        int fontPadding = getResourceDimensionValue(R.dimen._10sdp);
        int fontPadding1 = getResourceDimensionValue(R.dimen._2sdp);
        int textViewHeight = 0;
        try {
            textViewHeight = getHeight(getActivity(), newsContent.getText(), fontSize, getDeviceWidth(), NewsAppUtil.getInstance(getActivity()).getContentFontTypeFace(), fontPadding, fontPadding, fontPadding, fontPadding1);
        } catch (Exception e) {
            NewsAppUtil.logMessage("exception", "exception is: " + e);
        }
        return textViewHeight;
    }*/

  /*  private void setDynamicContentHeight() {


        int totalDeviceHeight = getDeviceHeight();
        int headlineHeight = getHeadlineHeight();
        int contentHeight = getNewsContentHeight() - 13;
        int moreContentHeight = getResourceDimensionValue(R.dimen._30sdp);


        if (contentHeight < getResourceDimensionValue(R.dimen._165sdp)) {

            int calculatedTextViewHeight1 = totalDeviceHeight - (headlineHeight + getResourceDimensionValue(R.dimen._135sdp) + moreContentHeight);
            newsImg.getLayoutParams().height = calculatedTextViewHeight1;
            videoClickImage.getLayoutParams().height = calculatedTextViewHeight1 - getResourceDimensionValue(R.dimen._55sdp);
            newsContent.getLayoutParams().height = getResourceDimensionValue(R.dimen._180sdp);

        } else {
            int calculatedTextViewHeight = totalDeviceHeight - (headlineHeight + contentHeight + moreContentHeight);
            NewsAppUtil.logMessage("resize test", "calculated height is: " + calculatedTextViewHeight);
            newsImg.getLayoutParams().height = calculatedTextViewHeight;
            videoClickImage.getLayoutParams().height = calculatedTextViewHeight - getResourceDimensionValue(R.dimen._55sdp);
            newsContent.getLayoutParams().height = contentHeight + getResourceDimensionValue(R.dimen._45sdp);
        }


    }
*/

    private int getResourceDimensionValue(int id) {

        return Math.round(getResources().getDimension(id));
    }

    private void registerCategoryClick() {

        category_image.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

              /*  sharedPreferences = getActivity().getSharedPreferences("MultiCategoryHelp", Context.MODE_PRIVATE);
                editor = sharedPreferences.edit();
                boolean multiCategoryHelp = sharedPreferences.getBoolean("MultiCategoryHelp", false);

                if (multiCategoryHelp) {
                    NewsAppUtil.logMessage("categoryClick", "category clicked 2");
                    //TODO Integrate Category Fragment by retrieving the data from category table
                    // Toast.makeText(HomeActivity.this, "Integrate Categories by retrieving the data from category table ", Toast.LENGTH_SHORT).show();
                    CategoryFragment f = CategoryFragment.newInstance(((HomeActivity) getActivity()).currentPos);
                    f.show(getActivity().getSupportFragmentManager(), "Categories Fragment");
                } else {
                    multiCategoryHelpDialog();
                }*/
                NewsAppUtil.logMessage("categoryClick", "category clicked 2");
                //TODO Integrate Category Fragment by retrieving the data from category table
                // Toast.makeText(HomeActivity.this, "Integrate Categories by retrieving the data from category table ", Toast.LENGTH_SHORT).show();
                CategoryFragment f = CategoryFragment.newInstance(((HomeActivity) getActivity()).currentPos);
                f.show(getActivity().getSupportFragmentManager(), "Categories Fragment");
            }
        });
    }

    private void multiCategoryHelpDialog() {

        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.multicategory_help_dialog);
        TextView textviewDismiss = (TextView) dialog.findViewById(R.id.tapToDismiss);
        textviewDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("MultiCategoryHelp", true);
                editor.commit();
                dialog.dismiss();
                NewsAppUtil.logMessage("categoryClick", "category clicked 2");
                //TODO Integrate Category Fragment by retrieving the data from category table
                // Toast.makeText(HomeActivity.this, "Integrate Categories by retrieving the data from category table ", Toast.LENGTH_SHORT).show();
                CategoryFragment f = CategoryFragment.newInstance(((HomeActivity) getActivity()).currentPos);
                f.show(getActivity().getSupportFragmentManager(), "Categories Fragment");
            }
        });
        dialog.show();
    }

    private void setThemeBasedColors() {

        if (toggle) {

            slanting_line.setImageResource(R.drawable.slantlinegeywhite_new_edited_2);
            rootLinearLayout.setBackgroundColor(Color.WHITE);
            newsContent.setTextColor(Color.BLACK);
            headline.setTextColor(Color.BLACK);
            //frame_layout.setBackgroundColor(Color.WHITE);
            line.setBackgroundColor(Color.BLACK);
            // frame_layout_more.setBackgroundColor(Color.WHITE);
            shareImg.setImageResource(R.drawable.share);
            imageViewReadMoreIcon.setImageResource(R.drawable.ic_read_more_icon);
        } else {

            slanting_line.setImageResource(R.drawable.slantlinegeydark_new_edited_2);
            rootLinearLayout.setBackgroundColor(getResources().getColor(R.color.cod_gray));
            newsContent.setTextColor(Color.WHITE);
            headline.setTextColor(Color.WHITE);
            //frame_layout.setBackgroundColor(getResources().getColor(R.color.cod_gray));
            line.setBackgroundColor(Color.WHITE);
            // frame_layout_more.setBackgroundColor(getResources().getColor(R.color.cod_gray));
            shareImg.setImageResource(R.drawable.share_dark_icon);
            imageViewReadMoreIcon.setImageResource(R.drawable.ic_read_more_icon_white);
        }
    }

    private void checkArticleAlreadyLiked() {

        String article_id = updatedArticleTable.getArticleId() + "";
        String category_id = updatedArticleTable.getCategoryId();
        if (checkLike(category_id, article_id)) {
            likeImg.setImageResource(R.drawable.heart);
        } else {
            likeImg.setImageResource(R.drawable.heart_e);
        }
    }

    private void setNewsContent() {

        newsContent.setTypeface(NewsAppUtil.getInstance(getActivity()).getContentFontTypeFace());

        String content = updatedArticleTable.getArticleDesc();
        NewsAppUtil.logMessage("handle error", content);
        content = content.replace(" ", "  ");

        if (content.length() != 0) {
            String first_letter = String.valueOf(content.charAt(0));
            if (first_letter != null) {

                newsContent.setText(capitalize(first_letter));
            }
            newsContent.append(content.substring(1));
        }
        NewsAppUtil.logMessage("handle error", "text is: " + newsContent.getText());
    }

    private void setHeadlineContent() {

        NewsAppUtil.logMessage("handle error", "set headline content");
        headline.setTypeface(NewsAppUtil.getInstance(getActivity()).getTitleFontFace(), Typeface.BOLD);
        headline.setText(updatedArticleTable.getArticleTitle());
    }

    private void setMoreContent() {

        NewsAppUtil.logMessage("handle error", "set more content");

        if (!updatedArticleTable.getRef_source().equals("")) {

            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    more.setVisibility(View.VISIBLE);
                    UpdatedArticleTable updatedArticleTable = HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos);
                    sendReadEventToGA();
                    String more_link = updatedArticleTable.getRef_url();
                    more(more_link);
                }
            });
        } else {
            more.setVisibility(View.INVISIBLE);
        }

        if (!updatedArticleTable.getRef_source().equals("")) {
            more_content.setText("Read " + updatedArticleTable.getRef_source() + " @TamilFlashNews");
            more_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    more.setVisibility(View.VISIBLE);
                    UpdatedArticleTable updatedArticleTable = HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos);
                    sendReadEventToGA();
                    String more_link = updatedArticleTable.getRef_url();

                    more(more_link);
                }
            });
        } else {
            more.setVisibility(View.INVISIBLE);
        }
    }

    private void setArticleTimeDifferenceContent() {

        NewsAppUtil.logMessage("handle error", "set updatedArticleTable time difference content");

        String dtStart = updatedArticleTable.getCdate();

        Date todayDate = new Date();
        Date createdDate = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
        String todayDateString = dateFormat.format(todayDate);
        String differenceText = "";

        try {
            createdDate = dateFormat.parse(dtStart);
            Date currentDate = dateFormat.parse(todayDateString);
            differenceText = getcalculatedTimeDifference(createdDate, currentDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        cdateText.setTypeface(NewsAppUtil.getInstance(getActivity()).getRobotoRegularItalicTypeFace());
        cdateText.setText(differenceText);

    }

 /*   private void registerShareClick() {

        NewsAppUtil.logMessage("handle error", "registerShareClick");

        shareImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendShareEventToGA();
                shareit(updatedArticleTable.getArticleTitle());
                category_image.setImageResource(R.drawable.category_menu_new);
                category_image.setVisibility(View.VISIBLE);
                ((ImageView) v).startAnimation(scaleAnimation);
            }
        });
    }*/

    private String getcalculatedTimeDifference(Date startDate, Date endDate) {

        //calendar.setTimeZone(toTimeZone);
        long diff = endDate.getTime() - startDate.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;

        long hours = minutes / 60;
        long days = hours / 24;
        String differenceText = null;

        if (seconds < 60 && seconds > 0) {
            differenceText = (seconds != 0) ? ((seconds > 1) ? seconds + " Seconds ago" : seconds + " Second ago") : "";
        } else if (seconds >= 60 && seconds < 3600) {
            differenceText = (minutes != 0) ? ((minutes > 1) ? minutes + " Minutes ago" : minutes + " Minutes ago") : "";
        } else if (seconds >= 3600 && seconds < 86400) {
            long remainingMinutes = minutes % 60;
            if (remainingMinutes > 30) {
                differenceText = (hours != 0) ? ((hours >= 1) ? hours + 1 + " Hours ago" : hours + " Hours ago") : "";
            } else {
                differenceText = (hours != 0) ? ((hours > 1) ? hours + " Hours ago" : hours + " Hour ago") : "";
            }
        } else if (seconds >= 86400) {
            long remainingHours = hours % 24;
            if (remainingHours > 12) {
                differenceText = (days != 0) ? ((days >= 1) ? days + 1 + " Days ago" : days + " Days ago") : "";
            } else {
                differenceText = (days != 0) ? ((days > 1) ? days + " Days ago" : days + " Day ago") : "";
            }
        } else if (seconds < 0) {
            differenceText = "0 seconds ago";
        }

       /* long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        long remainingMinutes=162%60;
        String differenceText = (days != 0) ? ((days > 1) ? days + " Days ago" : days + " Day ago") : ((hours != 0) ? ((hours > 1) ? hours + " Hours ago" : hours + " Hour ago") : ((minutes != 0) ? ((minutes > 1) ? minutes + " Minutes ago" : minutes + " Minute ago") : ((seconds != 0) ? ((seconds > 1) ? seconds + " Seconds ago" : seconds + " Second ago") : "")));*/
        return differenceText;
    }

    private void registerShareClick() {

        NewsAppUtil.logMessage("handle error", "registerShareClick");

        shareImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                category_image.setImageResource(R.drawable.category_menu_new);
                category_image.setVisibility(View.VISIBLE);
                ((ImageView) v).startAnimation(scaleAnimation);
                // new ShareBitmapAsynctask().execute();

                /*getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        UpdatedArticleTable updatedArticleTable = HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos);
                        String articleId = updatedArticleTable.getArticleId() + "";
                        Log.d("GA", "UpdatedArticleTable id: " + articleId);
                        sendShareEventToGA(articleId);
                        shareit(updatedArticleTable.getArticleTitle());
                    }
                });*/
            }
        });
    }

   /* public static int getHeight(Context context, CharSequence text, int textSize, int deviceWidth, Typeface typeface, int leftPadding, int topPadding, int rightPadding, int bottomPadding) {

//        NewsAppUtil.logMessage("responsive","context is: "+context);
//        NewsAppUtil.logMessage("responsive","text is: "+text);
//        NewsAppUtil.logMessage("responsive","text size is: "+textSize);
//        NewsAppUtil.logMessage("responsive","device width is: "+deviceWidth);
//        NewsAppUtil.logMessage("responsive","typeface is: "+typeface);
//        NewsAppUtil.logMessage("responsive","padding is: "+padding);

        TextView textView = new TextView(context);
        textView.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);
        textView.setTypeface(typeface);
        textView.setText(text, TextView.BufferType.SPANNABLE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }*/

    private void registerLikeClick() {

        likeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewsAppUtil.logMessage("likeclick", "like clicked");
                ((ImageView) v).startAnimation(scaleAnimation);
                like();
            }
        });
    }

    private void setNewsImage() {

        SharedPreferences sharedpreferencesImagePath = getActivity().getSharedPreferences("downloadedAllArticles", Context.MODE_PRIVATE);
        String baseImageUrl = sharedpreferencesImagePath.getString("bpath", null);

        //  String articleImageURL = NewsAppConstants.BASE_IMAGE_URL + "/" + updatedArticleTable.getArticleImg();
        String articleImageURL = baseImageUrl + "/" + updatedArticleTable.getArticleImg();
        Picasso.with(getActivity()).load(articleImageURL).
                fit().into(newsImg);
        //playImg.setVisibility(View.INVISIBLE);`
        playBtnImg.setOnClickListener(null);
        // newsImg.setVisibility(View.VISIBLE);
        NewsAppUtil.logMessage("video test", "updatedArticleTable type is: " + updatedArticleTable.getArticleType());

        if (updatedArticleTable.getArticleType().equals("3")) {

            // youTubeThumbnailView.initialize(googleApiKey, this);

            Bitmap imageBitmap = BitmapFactory.decodeResource(getActivity().getResources(),
                    R.drawable.rsz_youtube);
            Drawable drawable = new BitmapDrawable(getActivity().getResources(), imageBitmap);
            playImg.setImageDrawable(drawable);

            String videoUrl = updatedArticleTable.getData();
            if (videoUrl != null) {

                final String videoId = YoutubeUrlParser.getVideoId(videoUrl);

                NewsAppUtil.logMessage("video test", "video id is: " + videoId);
                playImg.setVisibility(View.VISIBLE);
                playerStyle = YouTubePlayer.PlayerStyle.DEFAULT;
                orientation = Orientation.AUTO;
                showAudioUi = true;
                showFadeAnim = true;
                // newsImg.setVisibility(View.INVISIBLE);
                try {
                    Picasso.with(getActivity())
                            .load(YouTubeThumbnail.getUrlFromVideoId(videoId, Quality.HIGH)).
                            placeholder(R.drawable.news_app_logo_high_res)
                            .fit()
                            .centerCrop()
                            .into(newsImg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                playBtnImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        playVideo(videoId);
                    }
                });

                playImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        playVideo(videoId);
                    }
                });
            }
        } else if (updatedArticleTable.getArticleType().equals("5")) {

            Bitmap imageBitmap = BitmapFactory.decodeResource(getActivity().getResources(),
                    R.drawable.audio);
            Drawable drawable = new BitmapDrawable(getActivity().getResources(), imageBitmap);
            playImg.setImageDrawable(drawable);

            playImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //   UpdatedArticleTable updatedArticleTable = HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos);
                    String soundCloudUrl = updatedArticleTable.getScloud();
                    Intent soundCloudIntent = new Intent(getActivity(), SoundCloudWebviewActivity.class);
                    soundCloudIntent.putExtra("soundCloudUrl", soundCloudUrl);
                    startActivity(soundCloudIntent);
                }
            });

        } else if (updatedArticleTable.getArticleType().equals("4")) {
            Bitmap imageBitmap = BitmapFactory.decodeResource(getActivity().getResources(),
                    R.drawable.image_album);
            Drawable drawable = new BitmapDrawable(getActivity().getResources(), imageBitmap);
            playImg.setImageDrawable(drawable);

            playImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  UpdatedArticleTable updatedArticleTable = HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos);
                    String imageAlbumUrl = updatedArticleTable.getImage_album();
                    String articleTitle = updatedArticleTable.getArticleTitle();
                    Intent imageAlbumIntent = new Intent(getActivity(), ImageAlbumActivity.class);
                    imageAlbumIntent.putExtra("imageAlbumUrl", imageAlbumUrl);
                    imageAlbumIntent.putExtra("articleTitle", articleTitle);
                    startActivity(imageAlbumIntent);
                }
            });
        }
    }

    private void playVideo(String videoId) {

        String articleId = (HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos).getArticleId() == null) ? "0" : HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos).getArticleId() + "";

        Tracker t = ((NewsApp) getActivity().getApplicationContext()).tracker();
        t.send(new HitBuilders.EventBuilder()
                .setCategory("Android")
                .setAction("Video")
                .setLabel(articleId)
                .setValue(Integer.parseInt(articleId)).setCustomDimension(1, DEVICE_ID)
                .build());
        if (videoId != null) {
            Intent intent = new Intent(getActivity(), YouTubePlayerActivity.class);
            intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, videoId);
            intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, playerStyle);
            intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, orientation);
            intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, showAudioUi);
            intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);
            if (showFadeAnim) {
                intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_ENTER, R.anim.abc_fade_in);
                intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_EXIT, R.anim.abc_fade_out);
            } else {
                intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_ENTER, R.anim.abc_popup_enter);
                intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_EXIT, R.anim.abc_popup_exit);
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(getActivity().getApplicationContext(), "Video does not exist!", Toast.LENGTH_SHORT).show();

        }
    }

    public Boolean checkLike(String cid, String aid) {

        NewsAppUtil.logMessage("likeclick", "checking category id is: " + cid);
        NewsAppUtil.logMessage("likeclick", "checking updatedArticleTable id is: " + aid);

        List<UpdatedLikeTable> updatedLikeTableList = Repo.getInstance(context).repoLike.getArticleBycIDandaID(cid, aid);
        NewsAppUtil.logMessage("likeclick", "size is: " + updatedLikeTableList.size());

        if (updatedLikeTableList == null || updatedLikeTableList.size() == 0)
            return false;
        else
            return true;
    }

    public void like() {

        // UpdatedArticleTable updatedArticleTable = HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos);
        String article_id = updatedArticleTable.getArticleId() + "";
        String category_id = updatedArticleTable.getCategoryId();
        UpdatedLikeTable updatedLikeTable = new UpdatedLikeTable().fromArticle(updatedArticleTable);

        NewsAppUtil.logMessage("likeclick", "category id is: " + category_id);
        NewsAppUtil.logMessage("likeclick", "updatedArticleTable id is: " + article_id);


        if (checkLike(category_id, article_id)) {

            NewsAppUtil.logMessage("likeclick", "removing heart fill");
            Repo.getInstance(context).repoLike.deleteById(article_id);
            NewsAppUtil.logMessage("likeclick", "deleting updatedArticleTable " + category_id + ": " + article_id + "in the db");
            likeImg.setImageResource(R.drawable.heart_e);
        } else {
            NewsAppUtil.logMessage("likeclick", "refilling heart");
            Repo.getInstance(context).repoLike.insert(updatedLikeTable);
            NewsAppUtil.logMessage("likeclick", "inserting article" + category_id + ": " + article_id + " in the db");
            likeImg.setImageResource(R.drawable.heart);

            try {

                NewsAppUtil.logMessage("analytics", "send analytics like");
                Tracker t = ((NewsApp) getActivity().getApplicationContext()).tracker();

                //  String categoryId = (HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos).getCategoryId() == null) ? "0" : HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos).getCategoryId() + "";
                String articleId = (HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).vpPager.getCurrentItem()).getArticleId() == null) ? "0" : HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).vpPager.getCurrentItem()).getArticleId() + "";

                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Android")
                        .setAction("Like")
                        .setLabel(articleId)
                        .setValue(Integer.parseInt(articleId)).setCustomDimension(1, DEVICE_ID)
                        .build());

            } catch (Exception e) {
                NewsAppUtil.logMessage("analytics", "send analytics Exception");
            }
        }
    }

    /*  private void sendShareEventToGA() {
          Tracker t = ((NewsApp) getActivity().getApplication()).tracker();
          Article article = HomeActivity.adapterViewPager.articleList.get(((HomeActivity) getActivity()).currentPos);
          String articleId = article.getArticleId() + "";
          NewsAppUtil.logMessage("analytics", "share article id is: " + articleId);
          t.send(new HitBuilders.EventBuilder()
                  .setCategory("Android")
                  .setAction("Share")
                  .setLabel(articleId)
                  .setValue(Integer.parseInt(articleId))
                  .build());
      }*/
    private void sendShareEventToGA(String articleId) {

        Tracker t = ((NewsApp) getActivity().getApplication()).tracker();
        //Article article = HomeActivity.adapterViewPager.articleList.get(((HomeActivity) getActivity()).currentPos);
        //String articleId = article.getArticleId() + "";
        NewsAppUtil.logMessage("analytics", "share article id is: " + articleId);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("Android")
                .setAction("Share")
                .setLabel(articleId)
                .setValue(Integer.parseInt(articleId)).setCustomDimension(1, DEVICE_ID)
                .build());
    }

    private void sendReadEventToGA() {

        Tracker t = ((NewsApp) getActivity().getApplication()).tracker();
        UpdatedArticleTable article = HomeActivity.adapterViewPager.updatedArticleTableList.get(((HomeActivity) getActivity()).currentPos);
        String articleId = article.getArticleId() + "";
        String articleTitle = article.getArticleTitle();

        NewsAppUtil.logMessage("analytics", "send event article id is: " + articleId);
        t.setScreenName("Read - " + articleId);
        t.setTitle("Title - " + articleTitle);
        t.send(new HitBuilders.ScreenViewBuilder().setCustomDimension(1, DEVICE_ID)
                .build());
    }

    private String formatDate(String cdate) {

        String[] cdate1 = cdate.split(" ");
        String[] cd = cdate1[0].split("-");
        cdate = cd[2] + " " + getMonthName(cd[1]) + " " + cd[0];

        return cdate;

    }

    private String formatcDate(String adate_number, String adate_duration) {

        String[] adate_duration1 = adate_duration.split(" ");

        if (adate_number.equals("1"))
            adate_duration1[0] = adate_duration1[0].substring(0, adate_duration1[0].length() - 1);

        return adate_number + " " + adate_duration1[0] + " " + adate_duration1[1];
    }


   /* File picFile;


    public void shareit(String articleTitle,Bitmap bitmap) {

        View window = getActivity().getWindow().getDecorView();
           new  ShareBitmapAsynctask(articleTitle,bitmap).execute(new Integer[]{window.getWidth(),window.getHeight()} );


    }*/

    private String getMonthName(String monthNum) {
        String monthName = monthNum;
        try {
            monthNum = (monthNum.startsWith("0")) ? monthNum.substring(1) : monthNum;
            int monthIndex = Integer.parseInt(monthNum) - 1;
            monthName = monthArray[monthIndex];
        } catch (Exception e) {
            e.printStackTrace();
        }

        return monthName;
    }

    public File shareit() {

        category_image.setImageResource(R.drawable.news_app_logo_high_res);
        // rootLinearLayout.getRootView();
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File picDir = new File(Environment.getExternalStorageDirectory() + "/myPic");
            if (!picDir.exists()) {
                picDir.mkdir();
            }
           /* rootLinearLayout.setDrawingCacheEnabled(true);
            rootLinearLayout.buildDrawingCache(true);
            Bitmap bitmap = rootLinearLayout.getDrawingCache();*/

            String fileName = "news_share" + ".png";
            picFile = new File(picDir + "/" + fileName);
            try {
                /*picFile.createNewFile();
                FileOutputStream picOut = new FileOutputStream(picFile);
                View window = getActivity().getWindow().getDecorView();

                Canvas bitmapCanvas = new Canvas();
                bitmap = Bitmap.createBitmap(window.getWidth()*2, window.getHeight()*2, Bitmap.Config.ARGB_8888);

                bitmapCanvas.setBitmap(bitmap);
                bitmapCanvas.scale(2.0f, 2.0f);
                window.draw(bitmapCanvas);
                boolean saved = bitmap.compress(Bitmap.CompressFormat.PNG, 100, picOut);
                if (saved) {
                    //Toast.makeText(getActivity().getApplicationContext(), "Image saved to your device Pictures " + "directory!", Toast.LENGTH_SHORT).show();
                } else {
                    //Error
                }
                picOut.close();*/


                // create bitmap screen capture
                View v1 = getActivity().getWindow().getDecorView().getRootView();
                v1.setDrawingCacheEnabled(true);
                Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                v1.setDrawingCacheEnabled(false);

                // File imageFile = new File(picFile);

                FileOutputStream outputStream = new FileOutputStream(picFile);
                int quality = 100;
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                outputStream.flush();
                outputStream.close();

                // openScreenshot(imageFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // rootLinearLayout.destroyDrawingCache();
        } else {
            //Error

        }

       /* Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("image/jpeg");
        sharingIntent.putExtra(Intent.EXTRA_TEXT,atricleTitle+"\n"+"Get the app at" + NewsAppConstants.APP_LINK);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(picFile));
        startActivity(sharingIntent);*/
        return picFile;
    }


    private void more(String url) {

        if (url == null || url.equals(""))
            Toast.makeText(getActivity(), "Nothing more to read!", Toast.LENGTH_SHORT).show();
        else {
            Intent intent = new Intent(getActivity(), WebViewActivity.class);
            intent.putExtra("url", url);
            startActivity(intent);
        }

    }

    @Override
    public void pageCount() {

    }

    @Override
    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
        youTubeThumbnailLoader = youTubeThumbnailLoader;
        //    youTubeThumbnailLoader.setOnThumbnailLoadedListener(new ThumbnailLoadedListener());
        String videoUrl = updatedArticleTable.getData();
        final String videoId = YoutubeUrlParser.getVideoId(videoUrl);
        youTubeThumbnailLoader.setVideo(videoId);
    }

    @Override
    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

    }


    public void screenDensity() {
        int density = getResources().getDisplayMetrics().densityDpi;
        switch (density) {
            case DisplayMetrics.DENSITY_LOW:
                Toast.makeText(getActivity().getApplicationContext(), "LDPI", Toast.LENGTH_SHORT).show();
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                Toast.makeText(getActivity().getApplicationContext(), "MDPI", Toast.LENGTH_SHORT).show();
                break;
            case DisplayMetrics.DENSITY_HIGH:
                Toast.makeText(getActivity().getApplicationContext(), "HDPI", Toast.LENGTH_SHORT).show();
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                Toast.makeText(getActivity().getApplicationContext(), "XHDPI", Toast.LENGTH_SHORT).show();
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                Toast.makeText(getActivity().getApplicationContext(), "XXHDPI", Toast.LENGTH_SHORT).show();
                break;
        }
    }
   /* private class ShareBitmapAsynctask extends AsyncTask<Integer, Void, Void> {
        private Dialog dialog;
        String articleTitle;
        Bitmap bitmap;
        public ShareBitmapAsynctask(String articleTitle, Bitmap bitmap) {
            this.articleTitle=articleTitle;
            this.bitmap=bitmap;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            *//*dialog = new Dialog(getActivity());
            dialog.show();*//*
        }

        @Override
        protected Void doInBackground(Integer... params) {

            int width = params[0];
            int height = params[1];

            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                File picDir = new File(Environment.getExternalStorageDirectory() + "/myPic");
                if (!picDir.exists()) {
                    picDir.mkdir();
                }


                String fileName = "news_share" + ".png";
                picFile = new File(picDir + "/" + fileName);
                try {
                    picFile.createNewFile();
                    FileOutputStream picOut = new FileOutputStream(picFile);
                    final View window = getActivity().getWindow().getDecorView();

                    final Canvas bitmapCanvas = new Canvas();
                    bitmap = Bitmap.createBitmap(width * 2, height * 2, Bitmap.Config.ARGB_8888);

                    bitmapCanvas.setBitmap(bitmap);
                    bitmapCanvas.scale(2.0f, 2.0f);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            window.draw(bitmapCanvas);
                        }
                    });


                    boolean saved = bitmap.compress(Bitmap.CompressFormat.PNG, 100, picOut);
                    if (saved) {
                        //Toast.makeText(getActivity().getApplicationContext(), "Image saved to your device Pictures " + "directory!", Toast.LENGTH_SHORT).show();
                    } else {
                        //Error
                    }
                    picOut.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //root.destroyDrawingCache();
            } else {
                //Error

            }

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("image/jpeg");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, articleTitle + "\n" + "Get the app at " + NewsAppConstants.APP_LINK);
            sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(picFile));
            startActivity(sharingIntent);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            rootLinearLayout.destroyDrawingCache();
            //category_image.setImageResource(R.drawable.category_menu_new);

        }
    }*/

}
