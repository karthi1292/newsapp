package com.tamilflashnews.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tamilflashnews.R;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by CIPL307 on 9/22/2015.
 */
public class AppIntroductionAdapter extends PagerAdapter {

    private String mArticleId;
    private SharedPreferences mAppIntroPreference;
    private Context mContext;
    private AdapterListener mAdapterListener;

    public interface AdapterListener{
        void onClick();
    }

    public void setOnAdapterLister(AdapterListener adapterLister) {
        mAdapterListener = adapterLister;
    }

    private LayoutInflater inflater;
    private Integer imgArray[] = {R.drawable.intro_1,R.drawable.intro_2,R.drawable.intro_3};
    public AppIntroductionAdapter(LayoutInflater inflater,
                                  SharedPreferences appIntroPreference,
                                  String articleId,
                                  AppIntroduction appIntroduction){
        mArticleId = articleId;
        mAppIntroPreference = appIntroPreference;
        mContext = appIntroduction;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return imgArray.length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View rootView = inflater.inflate(R.layout.item_intro_screen_images,container,false);
        TextView tapToStart = (TextView) rootView.findViewById(R.id.tapToStart);
        RelativeLayout relativeLayout = (RelativeLayout) rootView.findViewById(R.id.appIntroRelativeLayout);
        if (getCount() - 1 == position) {
            tapToStart.setVisibility(View.VISIBLE);
            relativeLayout.setBackgroundColor(Color.BLACK);
        } else {
            tapToStart.setVisibility(View.GONE);
            relativeLayout.setBackgroundColor(Color.WHITE);
        }

        tapToStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mAdapterListener != null) {
                    mAdapterListener.onClick();
                }
            }
        });
        ImageView img = (ImageView) rootView.findViewById(R.id.intro_screen_image);
        Bitmap bitmap = BitmapFactory.decodeResource(inflater.getContext().getResources(), imgArray[position]);
        img.setImageBitmap(bitmap);

        container.addView(rootView);
        return rootView;
    }
    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename + ".png")));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View)object);
    }

    public interface AdapterListene {
    }
}
