package com.tamilflashnews.rate;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.tamilflashnews.R;
import com.tamilflashnews.model.NewsAppConstants;

/**
 * Created by CIPL0233 on 12/29/2015.
 */
public class AppRater {
    private final static String APP_TITLE = "TamilFlashnews";// App Name
    private final static String APP_NAME = "com.tamilflashnews";// Package Name
    private final static int DAYS_UNTIL_PROMPT = 0;//Min number of days
    private final static int LAUNCHES_UNTIL_PROMPT = 3;//Min number of launches
    private final static int REMIND_LATER = 2;
    private static boolean DAYS_REMIND_LATER = false;//Min number of days
    private static String DIALOG_SELECTION = null;
    private static SharedPreferences prefs;
    private static Long date_remind_later;
    private static boolean rateSelection = false;
    static Button btnRate, btnNoThanks, btnLater;

    public static void app_launched(Context mContext) {
        prefs = mContext.getSharedPreferences("apprater", 0);

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);

        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
            if (System.currentTimeMillis() >= date_firstLaunch +
                    (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                showRateDialog(mContext, editor);
            }
        }
        editor.commit();
    }

    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {


        final Dialog dialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        DIALOG_SELECTION = prefs.getString("DIALOG_SELECTION", null);
        boolean rateSelected = prefs.getBoolean("RATE_SELECTED", false);
        boolean dontShowSelected = prefs.getBoolean("DONT_SHOW_AGAIN", false);
        DAYS_REMIND_LATER = prefs.getBoolean("REMAIN_LATER", false);
        date_remind_later = prefs.getLong("DATE_REMIND_LATER", 0);

        if (DIALOG_SELECTION == null) {

            rateSelection = true;
        } else if (DIALOG_SELECTION.equals("RATE")) {
            rateSelection = true;
        }
        if ((rateSelection) && (!rateSelected) && (!dontShowSelected)) {

            if (DAYS_REMIND_LATER) {
                if (System.currentTimeMillis() >= date_remind_later +
                        (REMIND_LATER * 24 * 60 * 60 * 1000)) {

                    editor.putBoolean("REMAIN_LATER", false);
                    editor.putLong("DATE_REMIND_LATER", 0);
                    editor.commit();
                    dialog.setContentView(R.layout.rate_dialog);
                    initWidgets(dialog, mContext, editor);
                    dialog.show();

                }
            } else {
                dialog.setContentView(R.layout.rate_dialog);
                initWidgets(dialog, mContext, editor);
                dialog.show();
            }
            rateSelection=false;
            editor.putString("DIALOG_SELECTION", "SHARE");
        } else {
            dialog.setContentView(R.layout.share_dialog);
            Button btnShare = (Button) dialog.findViewById(R.id.button_share);
            TextView textviewClose = (TextView) dialog.findViewById(R.id.textview_close);

            textviewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String market, link, play_app_id;

                    play_app_id = mContext.getResources().getString(R.string.play_store_id);
                    market = mContext.getResources().getString(R.string.market);
                    market += play_app_id;
                    link = mContext.getResources().getString(R.string.playlink);
                    link += play_app_id;


                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                            "Check out the app at: " + NewsAppConstants.APP_LINK);
                    sendIntent.setType("text/plain");
                    mContext.startActivity(sendIntent);
                    dialog.dismiss();
                }
            });
            editor.putString("DIALOG_SELECTION", "RATE");
            editor.commit();
            dialog.show();
        }


    }

    private static void initWidgets(final Dialog dialog,final Context mContext, final SharedPreferences.Editor editor) {
        btnRate = (Button) dialog.findViewById(R.id.button_rate_now);
        btnNoThanks = (Button) dialog.findViewById(R.id.button_no_thanks);
        btnLater = (Button) dialog.findViewById(R.id.button_later);

        btnRate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_NAME)));
                editor.putBoolean("RATE_SELECTED", true);
                editor.commit();
                dialog.dismiss();
            }
        });


        btnNoThanks.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean("DONT_SHOW_AGAIN", true);
                    editor.commit();
                }
                dialog.dismiss();
            }
        });

        btnLater.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();

                editor.putBoolean("REMAIN_LATER", true);
                //  editor.putLong("date_remind_later", 0);

                if (date_remind_later == 0) {
                    date_remind_later = System.currentTimeMillis();
                    editor.putLong("DATE_REMIND_LATER", date_remind_later);
                }
                editor.commit();
            }
        });
    }
}
