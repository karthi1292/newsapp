package com.tamilflashnews.model;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by developgopi on 30/7/15.
 */
public interface VolleyJsonObjectResponder {
    void onJsonObjectResponseSuccess(JSONObject response);
    void onJsonObjectResponseFailure(VolleyError error);
}


