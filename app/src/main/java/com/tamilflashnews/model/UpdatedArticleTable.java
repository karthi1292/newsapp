package com.tamilflashnews.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

@DatabaseTable(tableName = "updatedArticle")

public class UpdatedArticleTable implements Serializable {
    @DatabaseField
    private String categoryId;

    @DatabaseField
    private String categoryName;

    @DatabaseField
    private String articleTag;

    @DatabaseField
    private String cmtcount;

    @DatabaseField
    private String articleTitle;

    @DatabaseField
    private String articleDate;

    @DatabaseField
    private String data;

    @DatabaseField
    private String cdate;

    @DatabaseField
    private String articleType;

    @DatabaseField
    private String articleDesc;

    @DatabaseField
    private String articleUpdatedDate;

    @DatabaseField(id = true)
    private Integer articleId;

    @DatabaseField
    private String articleUrl;

    @DatabaseField
    private String articleImg;

    @DatabaseField
    private String ref_source;

    @DatabaseField
    private String ref_url;

    @DatabaseField
    private String scloud;

    @DatabaseField
    private String image_album;

    @DatabaseField
    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getScloud() {
        return scloud;
    }

    public void setScloud(String scloud) {
        this.scloud = scloud;
    }

    public String getImage_album() {
        return image_album;
    }

    public void setImage_album(String image_album) {
        this.image_album = image_album;
    }

    public String getRef_source() {
        return ref_source;
    }

    public void setRef_source(String ref_source) {
        this.ref_source = ref_source;
    }

    public String getRef_url() {
        return ref_url;
    }

    public void setRef_url(String ref_url) {
        this.ref_url = ref_url;
    }

    public String getArticleTag() {
        return articleTag;
    }

    public void setArticleTag(String articleTag) {
        this.articleTag = articleTag;
    }

    public String getCmtcount() {
        return cmtcount;
    }

    public void setCmtcount(String cmtcount) {
        this.cmtcount = cmtcount;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(String articleDate) {
        this.articleDate = articleDate;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getArticleType() {
        return articleType;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }

    public String getArticleDesc() {
        return articleDesc;
    }

    public void setArticleDesc(String articleDesc) {
        this.articleDesc = articleDesc;
    }

    public String getArticleUpdatedDate() {
        return articleUpdatedDate;
    }

    public void setArticleUpdatedDate(String articleUpdatedDate) {
        this.articleUpdatedDate = articleUpdatedDate;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    public String getArticleImg() {
        return articleImg;
    }

    public void setArticleImg(String articleImg) {
        this.articleImg = articleImg;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("ref_source", ref_source);
        jo.put("ref_url", ref_url);
        jo.put("cid", categoryId);
        jo.put("ctitle", categoryName);
        jo.put("aid", articleId);
        jo.put("adate", articleDate);
        jo.put("cdate", cdate);
        jo.put("udate", articleUpdatedDate);
        jo.put("atitle", articleTitle);
        jo.put("atag", articleTag);
        jo.put("adesc", articleDesc);
        jo.put("aimg", articleImg);
        jo.put("aurl", articleUrl);
        jo.put("atype", articleType);
        jo.put("data", data);
        jo.put("cmtcount", cmtcount);
        jo.put("scloud", scloud);
        jo.put("album", image_album);
        jo.put("lang", language);
        return jo;
    }

    public UpdatedArticleTable fromJSON(JSONObject jo) throws JSONException {

        this.ref_source = jo.optString("ref_source");
        this.ref_url = jo.optString("ref_url");
        this.articleId = jo.optInt("aid");
        this.articleDate = jo.optString("adate");
        this.cdate = jo.optString("cdate");
        this.articleUpdatedDate = jo.optString("udate");
        this.articleTitle = jo.optString("atitle");
        this.articleTag = jo.optString("atag");
        this.articleDesc = jo.optString("adesc");
        this.articleImg = jo.optString("aimg");
        this.articleUrl = jo.optString("aurl");
        this.articleType = jo.optString("atype");
        this.data = jo.optString("data");
        this.cmtcount = jo.optString("cmtcount");
        this.scloud = jo.optString("scloud");
        this.image_album = jo.optString("album");
        this.language=jo.optString("lang");
        return this;
    }

    public UpdatedArticleTable fromBookmark(Bookmark bookmark) {

        this.ref_source = bookmark.getRef_source();
        this.ref_url = bookmark.getRef_url();
        this.categoryId = bookmark.getCategoryId();
        this.categoryName = bookmark.getCategoryName();
        this.articleId = bookmark.getArticleId();
        this.articleDate = bookmark.getArticleDate();
        this.cdate = bookmark.getCdate();
        this.articleUpdatedDate = bookmark.getArticleUpdatedDate();
        this.articleTitle = bookmark.getArticleTitle();
        this.articleTag = bookmark.getArticleTag();
        this.articleDesc = bookmark.getArticleDesc();
        this.articleImg = bookmark.getArticleImg();
        this.articleUrl = bookmark.getArticleUrl();
        this.articleType = bookmark.getArticleType();
        this.data = bookmark.getData();
        this.cmtcount = bookmark.getCmtcount();
        this.scloud = bookmark.getScloud();
        this.image_album = bookmark.getImage_album();
        this.language=bookmark.getLanguage();
        return this;
    }

    public UpdatedArticleTable fromLike(UpdatedLikeTable like) {

        this.ref_source = like.getRef_source();
        this.ref_url = like.getRef_url();
        this.categoryId = like.getCategoryId();
        this.categoryName = like.getCategoryName();
        this.articleId = like.getArticleId();
        this.articleDate = like.getArticleDate();
        this.cdate = like.getCdate();
        this.articleUpdatedDate = like.getArticleUpdatedDate();
        this.articleTitle = like.getArticleTitle();
        this.articleTag = like.getArticleTag();
        this.articleDesc = like.getArticleDesc();
        this.articleImg = like.getArticleImg();
        this.articleUrl = like.getArticleUrl();
        this.articleType = like.getArticleType();
        this.data = like.getData();
        this.cmtcount = like.getCmtcount();
        this.scloud = like.getScloud();
        this.image_album = like.getImage_album();
        this.language=like.getLanguage();
        return this;
    }

    public UpdatedArticleTable fromArticle(Article articles) {

        this.ref_source = articles.getRef_source();
        this.ref_url = articles.getRef_url();
        this.categoryId = articles.getCategoryId();
        this.categoryName = articles.getCategoryName();
        this.articleId = articles.getArticleId();
        this.articleDate = articles.getArticleDate();
        this.cdate = articles.getCdate();
        this.articleUpdatedDate = articles.getArticleUpdatedDate();
        this.articleTitle = articles.getArticleTitle();
        this.articleTag = articles.getArticleTag();
        this.articleDesc = articles.getArticleDesc();
        this.articleImg = articles.getArticleImg();
        this.articleUrl = articles.getArticleUrl();
        this.articleType = articles.getArticleType();
        this.data = articles.getData();
        this.cmtcount = articles.getCmtcount();
        this.scloud = articles.getScloud();
        this.image_album = articles.getImage_album();
        this.language=articles.getLanguage();
        return this;
    }
}