package com.tamilflashnews.model;

/**
 * Created by developgopi on 30/7/15.
 */
public interface NewsAppConstants {

    String DB_NAME = "grocery.db";
    int DB_VERSION = 4;
    String APP_VERSION = "2.0.3";

    String DEV_URL = "http://www.tamilflashnews.com/api/index.php?version=" + APP_VERSION + "&platform=android";
    // String DEV_URL = "http://www.tamilflashnews.com/api/index.php";
    String BASE_URL = DEV_URL;

    String DEV_IMAGE_URL = "http://www.tamilflashnews.com";
    String BASE_IMAGE_URL = DEV_IMAGE_URL;

    // Request Tags
    String CATEGORY_REQUEST_TAG = "Category Request";
    String ALL_ARTICLES_REQUEST_TAG = "All Articles Request";

    //Module Names
    String MODULE_ARTICLES = "article";

    //View Names
    String VIEW_ARCHIVE = "archive";

    //Push Token Key
    String PUSH_TOKEN_KEY = "push_news_key";
    String REGISTRATION_COMPLETE = "registrationComplete";

    //View Names
    String VIEW_CATEGORY_LIST = "category_list";
    //String VIEW_CATEGORY = "category";

    //UpdatedArticleTable CID
    String CID_ALL = "all";
    //String APP_LINK ="https://play.google.com/store/apps/details?id=com.tamilflashnews&hl=en";
    String APP_LINK = " http://bit.ly/TFNewsapp";

    String CONTENT_TYPEFACE = "SECRobotoLight-Regular.ttf";
    String ROBOTO_TYPEFACE = "Roboto-Light.ttf";
    String ROBOTO_REGULAR = "Roboto-Regular.ttf";
    String ROBOTO_REGULAR_ITALIC = "Roboto-Italic.ttf";
    String HEADER_TYPEFACE = "vijayab.ttf";
}
