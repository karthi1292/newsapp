package com.tamilflashnews.model;

import java.util.ArrayList;

/**
 * Created by ${zakirhussain} on 12/28/2015.
 */
public class UserContentData {

    public static final String USER_NAME = "user_name";
    public static final String USER_MAIL = "user_mail";
    public static final String USER_PHONE = "user_phone";
    public static final String ARTICLE_TITLE = "article_title";
    public static final String DESCRIPTION = "description";
    public static final String IMAGE_FILE = "image_file";
    public static final String MEDIA_TYPE = "media_type";
    public static final String MEDIA_FILE = "media_file";
    public static final String GEO_TAG = "geo_tag";
    public static final String DEVICE_ID = "deviceid";
    public static final String APP_VERSION = "version";
    public static final String PLATFORM = "android";
    public static final String YOUTUBE_URL = "Youtube_Url";

    private String mUsername;
    private String mUserMail;
    private String mUserPhone;
    private String mArticleTitle;
    private String mDescription;
    private ArrayList<String> mImageFile;
    private String mMediaType;
    private String mMediaFile;
    private String mLongitude;
    private String mLatitude;
    private String mDeviceId;
    private String mAppVersion;
    private String mPlatform;
    private String mYoutubeUrl;

    public String getYoutubeUrl() {
        return mYoutubeUrl;
    }

    public void setYoutubeUrl(String mYoutubeUrl) {
        this.mYoutubeUrl = mYoutubeUrl;
    }

    public String getMediaFile() {
        return mMediaFile;
    }

    public void setMediaFile(String mMediaFile) {
        this.mMediaFile = mMediaFile;
    }

    public String getUserMail() {
        return mUserMail;
    }

    public void setUserMail(String mUserMail) {
        this.mUserMail = mUserMail;
    }

    public String getUserPhone() {
        return mUserPhone;
    }

    public void setUserPhone(String mUserPhone) {
        this.mUserPhone = mUserPhone;
    }

    public String getArticleTitle() {
        return mArticleTitle;
    }

    public void setArticleTitle(String mArticleTitle) {
        this.mArticleTitle = mArticleTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public ArrayList<String> getImageFile() {
        return mImageFile;
    }

    public void setImageFile(ArrayList<String> mImageFile) {
        this.mImageFile = mImageFile;
    }

    public String getMediaType() {
        return mMediaType;
    }

    public void setMediaType(String mMediaType) {
        this.mMediaType = mMediaType;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String mDeviceId) {
        this.mDeviceId = mDeviceId;
    }

    public String getAppVersion() {
        return mAppVersion;
    }

    public void setAppVersion(String mAppVersion) {
        this.mAppVersion = mAppVersion;
    }

    public String getPlatform() {
        return mPlatform;
    }

    public void setPlatform(String mPlatform) {
        this.mPlatform = mPlatform;
    }
}
