package com.tamilflashnews.model;

import com.j256.ormlite.field.DatabaseField;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by CIPL319 on 11/12/2015.
 */
public class Like implements Serializable {

    @DatabaseField
    private Integer likeId;

    @DatabaseField
    private String categoryId;

    @DatabaseField
    private String categoryName;

    @DatabaseField
    private String articleTag;

    @DatabaseField
    private String cmtcount;

    @DatabaseField
    private String articleTitle;

    @DatabaseField
    private String articleDate;

    @DatabaseField
    private String data;

    @DatabaseField
    private String cdate;

    @DatabaseField
    private String articleType;

    @DatabaseField
    private String articleDesc;

    @DatabaseField
    private String articleUpdatedDate;

    @DatabaseField
    private Integer articleId;

    @DatabaseField
    private String articleUrl;

    @DatabaseField
    private String articleImg;

    @DatabaseField
    private String ref_source;

    @DatabaseField
    private String ref_url;

    @DatabaseField
    private String scloud;

    @DatabaseField
    private String image_album;

    @DatabaseField
    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getScloud() {
        return scloud;
    }

    public void setScloud(String scloud) {
        this.scloud = scloud;
    }

    public String getImage_album() {
        return image_album;
    }

    public void setImage_album(String image_album) {
        this.image_album = image_album;
    }

    public String getRef_source() {
        return ref_source;
    }

    public void setRef_source(String ref_source) {
        this.ref_source = ref_source;
    }

    public String getRef_url() {
        return ref_url;
    }

    public void setRef_url(String ref_url) {
        this.ref_url = ref_url;
    }

    public String getArticleTag() {
        return articleTag;
    }

    public void setArticleTag(String articleTag) {
        this.articleTag = articleTag;
    }

    public String getCmtcount() {
        return cmtcount;
    }

    public void setCmtcount(String cmtcount) {
        this.cmtcount = cmtcount;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(String articleDate) {
        this.articleDate = articleDate;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getArticleType() {
        return articleType;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }

    public String getArticleDesc() {
        return articleDesc;
    }

    public void setArticleDesc(String articleDesc) {
        this.articleDesc = articleDesc;
    }

    public String getArticleUpdatedDate() {
        return articleUpdatedDate;
    }

    public void setArticleUpdatedDate(String articleUpdatedDate) {
        this.articleUpdatedDate = articleUpdatedDate;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    public String getArticleImg() {
        return articleImg;
    }

    public void setArticleImg(String articleImg) {
        this.articleImg = articleImg;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("ref_source", ref_source);
        jo.put("ref_url", ref_url);
        jo.put("aid", articleId);
        jo.put("adate", articleDate);
        jo.put("cdate", cdate);
        jo.put("udate", articleUpdatedDate);
        jo.put("atitle", articleTitle);
        jo.put("atag", articleTag);
        jo.put("adesc", articleDesc);
        jo.put("aimg", articleImg);
        jo.put("aurl", articleUrl);
        jo.put("atype", articleType);
        jo.put("data", data);
        jo.put("cmtcount", cmtcount);
        jo.put("scloud", scloud);
        jo.put("album", image_album);
        return jo;
    }

    public Like fromJSON(JSONObject jo) throws JSONException {

        this.ref_source = jo.optString("ref_source");
        this.ref_url = jo.optString("ref_url");
        this.articleId = jo.optInt("aid");
        this.articleDate = jo.optString("adate");
        this.cdate = jo.optString("cdate");
        this.articleUpdatedDate = jo.optString("udate");
        this.articleTitle = jo.optString("atitle");
        this.articleTag = jo.optString("atag");
        this.articleDesc = jo.optString("adesc");
        this.articleImg = jo.optString("aimg");
        this.articleUrl = jo.optString("aurl");
        this.articleType = jo.optString("atype");
        this.data = jo.optString("data");
        this.cmtcount = jo.optString("cmtcount");
        this.scloud = jo.optString("scloud");
        this.image_album = jo.optString("album");
        return this;
    }

    public Like fromArticle(Like updatedArticleTable) {

        this.ref_source = updatedArticleTable.getRef_source();
        this.ref_url = updatedArticleTable.getRef_url();
        this.categoryId = updatedArticleTable.getCategoryId();
        this.categoryName = updatedArticleTable.getCategoryName();
        this.articleId = updatedArticleTable.getArticleId();
        this.articleDate = updatedArticleTable.getArticleDate();
        this.cdate = updatedArticleTable.getCdate();
        this.articleUpdatedDate = updatedArticleTable.getArticleUpdatedDate();
        this.articleTitle = updatedArticleTable.getArticleTitle();
        this.articleTag = updatedArticleTable.getArticleTag();
        this.articleDesc = updatedArticleTable.getArticleDesc();
        this.articleImg = updatedArticleTable.getArticleImg();
        this.articleUrl = updatedArticleTable.getArticleUrl();
        this.articleType = updatedArticleTable.getArticleType();
        this.data = updatedArticleTable.getData();
        this.cmtcount = updatedArticleTable.getCmtcount();
        this.scloud = updatedArticleTable.getScloud();
        this.image_album = updatedArticleTable.getImage_album();
        return this;
    }


    public Integer getLikeId() {
        return likeId;
    }

    public void setLikeId(Integer likeId) {
        this.likeId = likeId;
    }
}