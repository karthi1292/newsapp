package com.tamilflashnews.model;

import com.android.volley.VolleyError;

import org.json.JSONArray;

/**
 * Created by developgopi on 30/7/15.
 */
public interface VolleyJsonArrayResponder {
    void onJsonArrayResponseSuccess(JSONArray response);
    void onJsonArrayResponseFailure(VolleyError error);
}
