package com.tamilflashnews.model;

import com.android.volley.VolleyError;

/**
 * Created by developgopi on 30/7/15.
 */
public interface VolleyStringResponder {
    void onStringResponseSuccess(String response);
    void onStringResponseFailure(VolleyError error);
}
