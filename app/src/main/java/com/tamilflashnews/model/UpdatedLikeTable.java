package com.tamilflashnews.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

@DatabaseTable(tableName = "updatedLike")
public class UpdatedLikeTable implements Serializable {

    @DatabaseField(generatedId = true)    // For Autoincrement
    private Integer likeId;

    @DatabaseField
    private String categoryId;

    @DatabaseField
    private String categoryName;

    @DatabaseField
    private String articleTag;

    @DatabaseField
    private String cmtcount;

    @DatabaseField
    private String articleTitle;

    @DatabaseField
    private String articleDate;

    @DatabaseField
    private String data;

    @DatabaseField
    private String cdate;

    @DatabaseField
    private String articleType;

    @DatabaseField
    private String articleDesc;

    @DatabaseField
    private String articleUpdatedDate;

    @DatabaseField
    private Integer articleId;

    @DatabaseField
    private String articleUrl;

    @DatabaseField
    private String articleImg;

    @DatabaseField
    private String ref_source;

    @DatabaseField
    private String ref_url;

    @DatabaseField
    private String scloud;

    @DatabaseField
    private String image_album;

    @DatabaseField
    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getLikeId() {
        return likeId;
    }
    public void setLikeId(Integer likeId) {
        this.likeId = likeId;
    }

    public String getScloud() {
        return scloud;
    }
    public void setScloud(String scloud) {
        this.scloud = scloud;
    }

    public String getImage_album() {
        return image_album;
    }

    public void setImage_album(String image_album) {
        this.image_album = image_album;
    }

    public String getRef_source() {
        return ref_source;
    }

    public void setRef_source(String ref_source) {
        this.ref_source = ref_source;
    }

    public String getRef_url() {
        return ref_url;
    }

    public void setRef_url(String ref_url) {
        this.ref_url = ref_url;
    }

    public String getArticleTag() {
        return articleTag;
    }

    public void setArticleTag(String articleTag) {
        this.articleTag = articleTag;
    }

    public String getCmtcount() {
        return cmtcount;
    }

    public void setCmtcount(String cmtcount) {
        this.cmtcount = cmtcount;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(String articleDate) {
        this.articleDate = articleDate;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getArticleType() {
        return articleType;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }

    public String getArticleDesc() {
        return articleDesc;
    }

    public void setArticleDesc(String articleDesc) {
        this.articleDesc = articleDesc;
    }

    public String getArticleUpdatedDate() {
        return articleUpdatedDate;
    }

    public void setArticleUpdatedDate(String articleUpdatedDate) {
        this.articleUpdatedDate = articleUpdatedDate;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    public String getArticleImg() {
        return articleImg;
    }

    public void setArticleImg(String articleImg) {
        this.articleImg = articleImg;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("ref_source", ref_source);
        jo.put("ref_url", ref_url);
        jo.put("aid", articleId);
        jo.put("adate", articleDate);
        jo.put("cdate", cdate);
        jo.put("udate", articleUpdatedDate);
        jo.put("atitle", articleTitle);
        jo.put("atag", articleTag);
        jo.put("adesc", articleDesc);
        jo.put("aimg", articleImg);
        jo.put("aurl", articleUrl);
        jo.put("atype", articleType);
        jo.put("data", data);
        jo.put("cmtcount", cmtcount);
        jo.put("scloud", scloud);
        jo.put("album", image_album);
        jo.put("lang", language);
        return jo;
    }

    public UpdatedLikeTable fromJSON(JSONObject jo) throws JSONException {

        this.ref_source = jo.optString("ref_source");
        this.ref_url = jo.optString("ref_url");
        this.articleId = jo.optInt("aid");
        this.articleDate = jo.optString("adate");
        this.cdate = jo.optString("cdate");
        this.articleUpdatedDate = jo.optString("udate");
        this.articleTitle = jo.optString("atitle");
        this.articleTag = jo.optString("atag");
        this.articleDesc = jo.optString("adesc");
        this.articleImg = jo.optString("aimg");
        this.articleUrl = jo.optString("aurl");
        this.articleType = jo.optString("atype");
        this.data = jo.optString("data");
        this.cmtcount = jo.optString("cmtcount");
        this.scloud = jo.optString("scloud");
        this.image_album = jo.optString("album");
        this.language=jo.optString("lang");
        return this;
    }

    public UpdatedLikeTable fromArticle(UpdatedArticleTable article) {

        this.ref_source = article.getRef_source();
        this.ref_url = article.getRef_url();
        this.categoryId = article.getCategoryId();
        this.categoryName = article.getCategoryName();
        this.articleId = article.getArticleId();
        this.articleDate = article.getArticleDate();
        this.cdate = article.getCdate();
        this.articleUpdatedDate = article.getArticleUpdatedDate();
        this.articleTitle = article.getArticleTitle();
        this.articleTag = article.getArticleTag();
        this.articleDesc = article.getArticleDesc();
        this.articleImg = article.getArticleImg();
        this.articleUrl = article.getArticleUrl();
        this.articleType = article.getArticleType();
        this.data = article.getData();
        this.cmtcount = article.getCmtcount();
        this.scloud = article.getScloud();
        this.image_album = article.getImage_album();
        this.language=article.getLanguage();
        return this;
    }

    public UpdatedLikeTable fromLike(Like like) {

        this.ref_source = like.getRef_source();
        this.ref_url = like.getRef_url();
        this.categoryId = like.getCategoryId();
        this.categoryName = like.getCategoryName();
        this.articleId = like.getArticleId();
        this.articleDate = like.getArticleDate();
        this.cdate = like.getCdate();
        this.articleUpdatedDate = like.getArticleUpdatedDate();
        this.articleTitle = like.getArticleTitle();
        this.articleTag = like.getArticleTag();
        this.articleDesc = like.getArticleDesc();
        this.articleImg = like.getArticleImg();
        this.articleUrl = like.getArticleUrl();
        this.articleType = like.getArticleType();
        this.data = like.getData();
        this.cmtcount = like.getCmtcount();
        this.scloud = like.getScloud();
        this.image_album = like.getImage_album();
        this.language=like.getLanguage();
        return this;
    }



}