package com.tamilflashnews.model;

import com.j256.ormlite.field.DatabaseField;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Category implements Serializable
{
    @DatabaseField
    private String icon;

    @DatabaseField
    private String pid;

    @DatabaseField
    private String categoryId;

    @DatabaseField
    private String categoryName;

    public String getIcon ()
    {
        return icon;
    }

    public void setIcon (String icon)
    {
        this.icon = icon;
    }

    public String getPid ()
    {
        return pid;
    }

    public void setPid (String pid)
    {
        this.pid = pid;
    }

    public String getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(String categoryId)
    {
        this.categoryId = categoryId;
    }

    public String getCategoryName()
    {
        return categoryName;
    }

    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("cid", categoryId);
        jo.put("cname", categoryName);
        jo.put("pid", pid);
        jo.put("icon", icon);
        return jo;
    }

    public Category fromJSON(JSONObject jo) throws JSONException {
        this.categoryId = jo.optString("cid");
        this.categoryName = jo.optString("cname");
        this.pid = jo.optString("pid");
        this.icon = jo.optString("icon");
        return this;
    }
}
